# Grading Service

This service is responsible for managing the grading of assignments or assignment's sets
within digital lab.

Teachers can create rubrics, which are used as the basis for grading assignments.
see [Design](#Design) for more information.



> This file is best viewed as a rendered MD file.

## Design

### Rubrics

Rubrics within the grading service, consist of the following entities. Currently, a rubric is associated with an
assignment, but see [#4](https://gitlab.com/rug-digitallab/products/grading/grading-core/-/issues/4).

```mermaid  
classDiagram
    direction BT
    class Assessment {
        Instant created
        UUID id
        BigDecimal? grade
        Rubric templateRubric
        Instant updated
        List~Score~ scores
    }
    class Bucket {
        String name
        String description
        BigDecimal points
        Instant created
        Criterion criterion
        UUID id
        Instant updated
    }
    class Category {
        Set~Criterion~ criteria
        String name
        String description
        Instant created
        Rubric rubric
        BigDecimal scalePointsTo
        UUID id
        Instant updated
    }
    class Criterion {
        String name
        String description
        BigDecimal? points
        Instant created
        Category category
        PointsSource? externalPoints
        Set~Bucket~? buckets
        UUID id
        Instant updated
        CriterionSource source
        Boolean allowOverride
    }
    class CriterionSource {
        <<enumeration>>
        EnumEntries~CriterionSource~ entries
        Int code
    }
    class Rubric {
        String name
        String description
        Instant created
        Set~Category~ categories
        UUID assignmentId
        BigDecimal scalePointsTo
        UUID id
        Instant updated
    }
    class Score {
        Instant created
        Criterion criterion
        Int revisionCount
        Assessment assessment
        UUID id
        ScoreRevision latestRevision
        List~ScoreRevision~ revisions
        Instant updated
    }
    class ScoreRevision {
        UUID grader
        BigDecimal? points
        Instant created
        UUID id
        Score score
        Bucket? bucket
    }

    Assessment "1" *--> "templateRubric 1" Rubric
    Assessment "1" *--> "scores *" Score
    Bucket "1" *--> "criterion 1" Criterion
    Category "1" *--> "criteria *" Criterion
    Category "1" *--> "rubric 1" Rubric
    Criterion "1" *--> "buckets *" Bucket
    Criterion "1" *--> "category 1" Category
    Criterion  ..>  Criterion
    Criterion "1" *--> "source 1" CriterionSource
    Rubric "1" *--> "categories *" Category
    Score "1" *--> "assessment 1" Assessment
    Score "1" *--> "criterion 1" Criterion
    Score "1" *--> "revisions *" ScoreRevision
    ScoreRevision "1" *--> "bucket 1" Bucket
    ScoreRevision  ..>  Category
    ScoreRevision  ..>  Criterion
    ScoreRevision  ..>  CriterionSource
    ScoreRevision "1" *--> "score 1" Score
    ScoreRevision  ..>  ScoreRevision


```
