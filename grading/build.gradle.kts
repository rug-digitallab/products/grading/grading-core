plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.project")
}

dependencies {
    val commonQuarkusVersion: String by project

    // Flyway DB Migrations
    implementation("io.quarkus:quarkus-flyway")
    implementation("io.quarkus:quarkus-jdbc-mariadb")
    implementation("org.flywaydb:flyway-mysql")
    // Panache/Hibernate Reactive
    implementation("io.quarkus:quarkus-hibernate-reactive-panache-kotlin")
    implementation("io.quarkus:quarkus-hibernate-validator")
    implementation("io.quarkus:quarkus-reactive-mysql-client")
    implementation("nl.rug.digitallab.common.quarkus:hibernate-reactive:$commonQuarkusVersion")
    // RESTEasy Reactive
    implementation("io.quarkus:quarkus-rest")
    implementation("io.quarkus:quarkus-rest-jackson")
    implementation("nl.rug.digitallab.common.quarkus:exception-mapper-hibernate:$commonQuarkusVersion")
    implementation("io.quarkus:quarkus-smallrye-openapi")

    testImplementation("io.quarkus:quarkus-test-hibernate-reactive-panache")
    testImplementation("io.quarkus:quarkus-test-vertx")
    testImplementation("io.rest-assured:kotlin-extensions")
    testImplementation("io.rest-assured:rest-assured")
}
