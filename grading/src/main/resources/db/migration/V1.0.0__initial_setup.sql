
create table assessment (
                            id binary(16) not null,
                            created datetime(6),
                            updated datetime(6),
                            template_rubric_id binary(16),
                            primary key (id)
) engine=InnoDB;

create table bucket (
                        id binary(16) not null,
                        created datetime(6),
                        description varchar(255) not null,
                        name varchar(255) not null,
                        points decimal(38,2),
                        updated datetime(6),
                        criterion_id binary(16),
                        primary key (id)
) engine=InnoDB;

create table category (
                          id binary(16) not null,
                          created datetime(6),
                          description varchar(255) not null,
                          name varchar(255) not null,
                          scale_points_to decimal(38,2),
                          updated datetime(6),
                          rubric_id binary(16),
                          primary key (id)
) engine=InnoDB;

create table criterion (
                           id binary(16) not null,
                           allow_override bit not null,
                           created datetime(6),
                           description varchar(255) not null,
                           external_source varbinary(255),
                           scale_points_to decimal(38,2),
                           name varchar(255) not null,
                           points decimal(38,2),
                           source tinyint not null check (source between 0 and 3),
                           updated datetime(6),
                           category_id binary(16),
                           primary key (id)
) engine=InnoDB;

create table rubric (
                        id binary(16) not null,
                        assignment_id binary(16),
                        created datetime(6),
                        description varchar(10000) not null,
                        name varchar(255) not null,
                        scale_points_to decimal(38,2),
                        updated datetime(6),
                        primary key (id)
) engine=InnoDB;

create table score (
                       id binary(16) not null,
                       created datetime(6),
                       revision_count integer not null,
                       updated datetime(6),
                       assessment_id binary(16) not null,
                       criterion_id binary(16) not null,
                       primary key (id)
) engine=InnoDB;

create table score_revision (
                                id binary(16) not null,
                                created datetime(6),
                                grader binary(16) not null,
                                points decimal(38,2),
                                bucket_id binary(16),
                                score_id binary(16),
                                primary key (id)
) engine=InnoDB;

alter table if exists assessment
    add constraint UKpgxm0ie4js9j99u6y22mkhv6b unique (template_rubric_id);

alter table if exists score
    add constraint UK5xkkteu88luxtlmpk7s38myy7 unique (criterion_id, assessment_id);

alter table if exists assessment
    add constraint FKdjaxvlk4g5rekhdd9x8jntnyc
    foreign key (template_rubric_id)
    references rubric (id);

alter table if exists bucket
    add constraint FK4ce9mecxhorsp64gxhbisgd5j
    foreign key (criterion_id)
    references criterion (id);

alter table if exists category
    add constraint FK24hyl565pbvded5xe5ltrkli0
    foreign key (rubric_id)
    references rubric (id);

alter table if exists criterion
    add constraint FK7k3xmpmqk719xvcohmwa74or5
    foreign key (category_id)
    references category (id);

alter table if exists score
    add constraint FKj2t996tvl0ocak4p4k7n7lfi3
    foreign key (assessment_id)
    references assessment (id);

alter table if exists score
    add constraint FK3woom0qv7joeos3uu60ikrlw1
    foreign key (criterion_id)
    references criterion (id);

alter table if exists score_revision
    add constraint FKk0br2w336ajqrv78onsg62198
    foreign key (bucket_id)
    references bucket (id);

alter table if exists score_revision
    add constraint FKal4igrg7obasm04aagmvaf7ct
    foreign key (score_id)
    references score (id);
