package nl.rug.digitallab.grading.persistence.entities

import jakarta.persistence.*
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull
import nl.rug.digitallab.grading.CriterionId
import nl.rug.digitallab.grading.enums.CriterionSource
import nl.rug.digitallab.grading.persistence.entities.external.PointsSource
import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.math.BigDecimal
import java.time.Instant

/**
 * Represents an individual criterion within a category of a rubric, detailing specific assessment metrics or goals.
 * Each criterion contributes to the category's total points based on its maximum points.
 *
 * A criterion's points may be derived **one** from:
 * - A predefined bucket
 * - An external source
 * - Manually assigned by a grader
 * - A constant value
 *
 * @property id The id of the criterion, automatically generated.
 * @property name The name of the criterion, must not be null or blank.
 * @property description Description of the criterion, offering further details about what is
 *                       specifically assessed.
 * @property category The category this criterion belongs to.
 * @property buckets A list of scoring buckets that define ranges of scores for the criterion.
 * @property allowOverride Whether to allow manually overriding the score for this criterion.
 * @property externalPoints An external points source where this criterion needs to get the points from.
 * @property source The point source type of this criterion
 * @property points The maximum number of points this criterion may provide, unless allowOverride is set.
 * @property created The timestamp when the criterion was created, automatically set.
 * @property updated The timestamp of the last update made to the criterion, automatically set.
 */

@Entity
class Criterion(
    @field:ManyToOne(fetch = FetchType.LAZY)
    var category: Category,

    @field:NotBlank(message = "Criterion name cannot be null or blank")
    var name: String,

    @field:NotNull(message = "Bucket description cannot be null")
    var description: String,

    @field:OneToMany(
        mappedBy = "criterion",
        cascade = [CascadeType.ALL],
        orphanRemoval = true,
        fetch = FetchType.EAGER,
    )
    var buckets: MutableSet<Bucket>? = null,

    var allowOverride: Boolean,

    @field:NotNull(message = "Criterion source cannot be null")
    @field:Enumerated(EnumType.ORDINAL)
    var source: CriterionSource,

    @field:Embedded
    var externalPoints: PointsSource? = null,

    var points: BigDecimal? = null,
) {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    lateinit var id: CriterionId

    @CreationTimestamp
    lateinit var created: Instant

    @UpdateTimestamp
    lateinit var updated: Instant

    @PrePersist
    @PreUpdate
    private fun validate() {
        when(source) {
            CriterionSource.BUCKET -> bucketConfigChecks()
            CriterionSource.CONSTANT -> constantConfigChecks()
            CriterionSource.MANUAL -> manualConfigChecks()
            CriterionSource.EXTERNAL -> externalConfigChecks()
        }
    }

    private fun bucketConfigChecks() {
        check(externalPoints == null) { "Source cannot be bucket and external at the same time" }
    }

    private fun constantConfigChecks() {
        checkNotNull(points) { "Points must be set for constant criteria" }
        check(externalPoints == null) { "Source cannot be constant and external at the same time" }
    }

    private fun manualConfigChecks() {
        check(externalPoints == null) { "Source cannot be manual and external at the same time" }
    }

    private fun externalConfigChecks() {
        check(points == null) { "Points must not be set for external criteria" }
    }
}
