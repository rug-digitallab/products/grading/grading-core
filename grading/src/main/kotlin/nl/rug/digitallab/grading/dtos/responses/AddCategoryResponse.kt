package nl.rug.digitallab.grading.dtos.responses

import nl.rug.digitallab.grading.BucketId
import java.math.BigDecimal
import java.time.Instant

/**
 * The [AddCategoryResponse] class is a data transfer object that represents a category in the grading service.
 *
 * @property id The id of the category.
 * @property name The name of the category.
 * @property description The description of the category.
 * @property criteria The criteria of the category.
 * @property scalePointsTo The scale to of the category.
 * @property created The creation date of the category.
 * @property updated The last update date of the category.
 */
data class AddCategoryResponse(
    val id: BucketId,
    val name: String,
    val description: String,
    val criteria: Set<AddCriterionResponse>,
    val scalePointsTo: BigDecimal,
    val created: Instant,
    val updated: Instant,
)
