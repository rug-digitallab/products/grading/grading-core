package nl.rug.digitallab.grading.persistence.entities

import jakarta.persistence.*
import nl.rug.digitallab.grading.AssessmentId
import nl.rug.digitallab.grading.CriterionId
import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.math.BigDecimal
import java.time.Instant

/**
 * Assessment class that represents an "initialized rubric".
 * An assessment is linked to a rubric and has list of scores.
 *
 * @property id The unique id of the assessment
 * @property templateRubric The template rubric used to assess a submission.
 * @property scores The criteria map, where a [CriterionId] is mapped to a [Score].
 * @property grade The grade of the whole assessment.
 * @property created When this assessment was created
 * @property updated Last time when this assessment was updated
 */
@Entity
class Assessment(
    @field:OneToOne(fetch = FetchType.EAGER)
    val templateRubric: Rubric,
    // https://gitlab.com/rug-digitallab/products/grading/grading-core/-/issues/25
) {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    lateinit var id: AssessmentId

    @OneToMany(
        fetch = FetchType.EAGER,
        cascade = [CascadeType.ALL],
        mappedBy = "assessment",
    )
    var scores: MutableList<Score> = mutableListOf()

    @get:Transient
    val grade: BigDecimal
        get() = calculateGrade()

    @CreationTimestamp
    lateinit var created: Instant

    @UpdateTimestamp
    lateinit var updated: Instant

    /**
     * Calculate the grade of the assessment once the entity has been fully loaded and initialized.
     *
     * @PostLoad is called before the scores list is initialized, sadly. So we have to leave this for whoever is fetching
     * the assessment. See [AssessmentRepository]
     */
    fun calculateGrade() : BigDecimal {
        return if (scores.isEmpty()) {
            BigDecimal.ZERO
        } else {
            scores.sumOf { it.latestRevision.points!! }
        }
    }
}
