package nl.rug.digitallab.grading.persistence.entities

import jakarta.persistence.*
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull
import nl.rug.digitallab.grading.BucketId
import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.math.BigDecimal
import java.time.Instant

/**
 * Represents a scoring bucket within a criterion for a rubric, which defines specific score ranges or qualitative
 * evaluations.
 * Buckets can be seen as a column in a rubric, where each bucket represents a certain state or (predefined) score.
 *
 * @property id The id of the bucket
 * @property criterion The criterion this bucket belongs to.
 * @property name The name of the bucket, must not be null or blank.
 * @property description Description of the bucket, providing additional details.
 * @property points The specific number of points associated with this bucket, indicating its weight or value.
 * @property created The timestamp when the bucket was created, automatically set.
 * @property updated The timestamp of the last update made to the bucket, automatically set.
 */
@Entity
class Bucket(
    @field:ManyToOne(fetch = FetchType.LAZY)
    var criterion: Criterion,

    @field:NotBlank(message = "Bucket name cannot be null or blank")
    var name: String,

    @field:NotNull(message = "Bucket description cannot be null")
    var description: String,

    var points: BigDecimal,
) {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    lateinit var id: BucketId

    @CreationTimestamp
    lateinit var created: Instant

    @UpdateTimestamp
    lateinit var updated: Instant
}
