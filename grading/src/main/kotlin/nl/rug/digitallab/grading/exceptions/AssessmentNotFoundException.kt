package nl.rug.digitallab.grading.exceptions

import nl.rug.digitallab.common.quarkus.exception.mapper.rest.MappedException
import nl.rug.digitallab.grading.AssessmentId
import org.jboss.resteasy.reactive.RestResponse.Status.NOT_FOUND

/**
 * Exception to throw when an assessment is not found.
 */
class AssessmentNotFoundException(message: String) : MappedException(message, NOT_FOUND) {
    constructor(assessmentId: AssessmentId) : this("Assessment with id $assessmentId not found")
}
