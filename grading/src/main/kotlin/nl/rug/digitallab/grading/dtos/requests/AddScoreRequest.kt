package nl.rug.digitallab.grading.dtos.requests

import nl.rug.digitallab.grading.BucketId
import nl.rug.digitallab.grading.CriterionId
import java.math.BigDecimal

/**
 * The request class for the score of a criterion
 *
 * @property criterionId The id of the criterion this score is linked to
 * @property points The grade being assigned to the criterion.
 * @property bucketId The id of the bucket this score originates from.
 *              **Used when the grade is from a bucket**
 */
data class AddScoreRequest(
    val criterionId: CriterionId,
    val points: BigDecimal? = null,
    val bucketId: BucketId? = null,
)
