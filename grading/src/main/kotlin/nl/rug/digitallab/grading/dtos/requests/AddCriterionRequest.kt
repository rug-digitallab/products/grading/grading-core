package nl.rug.digitallab.grading.dtos.requests

import nl.rug.digitallab.grading.enums.CriterionSource
import nl.rug.digitallab.grading.persistence.entities.external.PointsSource
import java.math.BigDecimal

/**
 * The [AddCriterionRequest] class is a data transfer object that is used to create a new [Criterion].
 *
 * @property name The name of the criterion.
 * @property description The description of the criterion.
 * @property allowOverride A boolean that indicates if the criterion is graded with a custom grade.
 * @property externalPoints An optional external points source to fetch the grade from for this criterion.
 * @property points The points to include for the criterion.
 * @property source The criterion source for the points.
 */
data class AddCriterionRequest(
    val name: String,
    val description: String,
    val allowOverride: Boolean,
    val source: CriterionSource,
    val externalPoints: PointsSource? = null,
    val points: BigDecimal? = null,
)
