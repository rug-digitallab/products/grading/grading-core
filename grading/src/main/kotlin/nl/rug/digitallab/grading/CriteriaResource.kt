package nl.rug.digitallab.grading

import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.RequestScoped
import jakarta.inject.Inject
import jakarta.validation.Valid
import jakarta.validation.constraints.NotNull
import jakarta.ws.rs.*
import jakarta.ws.rs.core.MediaType
import nl.rug.digitallab.grading.dtos.requests.AddBucketRequest
import nl.rug.digitallab.grading.dtos.requests.AddCriterionRequest
import nl.rug.digitallab.grading.dtos.responses.AddBucketResponse
import nl.rug.digitallab.grading.dtos.responses.AddCriterionResponse
import nl.rug.digitallab.grading.managers.BucketsManager
import nl.rug.digitallab.grading.managers.CriteriaManager
import org.jboss.resteasy.reactive.ResponseStatus
import org.jboss.resteasy.reactive.RestPath
import org.jboss.resteasy.reactive.RestResponse.StatusCode

/**
 * The [CriteriaResource]. Handles all incoming requests related to criteria.
 */
@Path("/api/v1/grading/criteria")
class CriteriaResource {
    @Inject
    private lateinit var criteriaManager: CriteriaManager

    /**
     * Get a criterion by its id.
     *
     * @param criterionId The criterion id to get.
     *
     * @return The [AddCriterionResponse] of the found Criterion, if found
     *
     * @throws [CriterionNotFoundException] if the criterion was not found
     */
    @GET
    @Path("{criterionId}")
    @Produces(MediaType.APPLICATION_JSON)
    fun getCriterion(
        @RestPath criterionId: CriterionId,
    ): Uni<AddCriterionResponse> = criteriaManager.getCriterion(criterionId)

    /**
     * Update a criterion by its id.
     *
     * @param criterionId The criterion id to update.
     * @param criterionRequest The representation of the new criterion.
     *
     * @return The [AddCriterionResponse] of the updated Criterion, if found
     *
     * @throws [CriterionNotFoundException] if the criterion was not found
     */
    @PUT
    @Path("{criterionId}")
    @ResponseStatus(StatusCode.OK)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    fun updateCriterion(
        @RestPath criterionId: CriterionId,
        @Valid @NotNull criterionRequest: AddCriterionRequest,
    ): Uni<AddCriterionResponse> = criteriaManager.updateCriterion(criterionId, criterionRequest)

    /**
     * Delete a criterion by its id.
     *
     * @param criterionId The criterion id to delete.
     *
     * @return Noting, Unit
     *
     * @throws [CriterionNotFoundException] if the criterion was not found
     */
    @DELETE
    @Path("{criterionId}")
    @ResponseStatus(StatusCode.NO_CONTENT)
    fun deleteCriterion(
        @RestPath criterionId: CriterionId,
    ): Uni<Unit> = criteriaManager.deleteCriterion(criterionId)

    /**
     * See [BucketsSubResource]
     */
    @Path("{criterionId}/buckets")
    fun bucketsSubResource(): Class<BucketsSubResource> = BucketsSubResource::class.java

    /**
     * The buckets sub-resource for the criterion. Which is responsible for handling all bucket related
     * to a specific criterion in combination with a bucket.
     */
    @RequestScoped
    class BucketsSubResource {
        @Inject
        private lateinit var bucketsManager: BucketsManager

        /**
         * Create a new bucket and add it to the criterion, subsequently to a rubric.
         *
         * @param criterionId The criterion id to add the bucket to.
         * @param bucketRequest The bucket as a DTO, using the [AddBucketRequest] as the API representation.
         *
         * @return The [AddBucketResponse] of the created bucket, if created
         *
         * @throws [BucketNotFoundException] if the bucket was not found
         */
        @POST
        @Path("")
        @ResponseStatus(StatusCode.CREATED)
        @Produces(MediaType.APPLICATION_JSON)
        @Consumes(MediaType.APPLICATION_JSON)
        fun createBucket(
            @RestPath criterionId: CriterionId,
            @Valid @NotNull bucketRequest: AddBucketRequest,
        ): Uni<AddBucketResponse> = bucketsManager.addBucket(criterionId, bucketRequest)
    }
}
