package nl.rug.digitallab.grading.dtos.responses

import nl.rug.digitallab.grading.AssignmentId
import nl.rug.digitallab.grading.RubricId
import java.math.BigDecimal
import java.time.Instant

/**
 * The [AddRubricResponse] class is a data transfer object that represents a rubric in the grading service.
 *
 * @property id The id of the rubric.
 * @property assignmentId The id of the assignment linked to this rubric.
 * @property name The name of the rubric.
 * @property description The description of the rubric.
 * @property categories The categories of the rubric.
 * @property scalePointsTo The scale to of the rubric.
 * @property created The creation date of the rubric.
 * @property updated The last update date of the rubric.
 */
class AddRubricResponse(
    val id: RubricId,
    val assignmentId: AssignmentId,
    val name: String,
    val description: String,
    val categories: Set<AddCategoryResponse>,
    val scalePointsTo: BigDecimal,
    val created: Instant,
    val updated: Instant,
)
