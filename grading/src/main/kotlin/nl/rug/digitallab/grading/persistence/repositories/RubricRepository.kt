package nl.rug.digitallab.grading.persistence.repositories

import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import nl.rug.digitallab.common.quarkus.hibernate.reactive.panache.repositories.BaseRepository
import nl.rug.digitallab.grading.AssignmentId
import nl.rug.digitallab.grading.RubricId
import nl.rug.digitallab.grading.exceptions.AssignmentNotFoundException
import nl.rug.digitallab.grading.exceptions.RubricNotFoundException
import nl.rug.digitallab.grading.persistence.entities.Rubric

/**
 * Rubric Repository class for handling operations related to Rubric entities.
 */
@ApplicationScoped
class RubricRepository : BaseRepository<Rubric, RubricId>() {
    /**
     * Find a rubric by ID or throw [RubricNotFoundException] if not found
     *
     * @param rubricId The ID of the rubric to search for
     *
     * @return The found [Rubric]
     *
     * @throws [RubricNotFoundException]
     */
    fun findByIdOrThrow(rubricId: RubricId): Uni<Rubric> =
        findByIdOrThrow(rubricId) { RubricNotFoundException(rubricId) }

    /**
     * Update a rubric by its ID and throw a [RubricNotFoundException] if not found
     *
     * @param rubricId The ID of the rubric to update
     * @param updateRubric The method to take the old entity and should return [Rubric]
     *
     * @return [Rubric] The updated rubric
     *
     * @throws [RubricNotFoundException]
     */
    fun updateWithOrThrow(
        rubricId: RubricId,
        updateRubric: (Rubric) -> Rubric
    ): Uni<Rubric> = updateWithOrThrow(rubricId, { RubricNotFoundException(rubricId) }, updateRubric)

    /**
     * Delete a Rubric by its ID and throw a [RubricNotFoundException] if not found
     *
     * @param rubricId The ID of the rubric
     *
     * @return [Unit]
     */
    fun deleteByIdOrThrow(rubricId: RubricId): Uni<Unit> =
        deleteByIdOrThrow(rubricId) { RubricNotFoundException(rubricId) }

    /**
     * Finds a Rubric by its associated AssignmentId.
     * If no Rubric is found for the given AssignmentId, an AssignmentNotFoundException is thrown.
     *
     * @param assignmentId The ID of the assignment to find the Rubric for.
     * @return A Uni emitting the found Rubric, or failing with AssignmentNotFoundException if not found.
     */
    fun findByAssignmentIdOrThrow(assignmentId: AssignmentId): Uni<Rubric> {
        return find("assignmentId", assignmentId)
            .firstResult()
            .onItem().ifNull().failWith { AssignmentNotFoundException(assignmentId) }
            .onItem().transform { return@transform it }
    }
}
