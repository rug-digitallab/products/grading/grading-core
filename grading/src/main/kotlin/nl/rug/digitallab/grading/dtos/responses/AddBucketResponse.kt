package nl.rug.digitallab.grading.dtos.responses

import nl.rug.digitallab.grading.BucketId
import java.math.BigDecimal
import java.time.Instant

/**
 * The [AddBucketResponse] class is a data transfer object that represents a bucket in the grading service.
 *
 * @property id The id of the bucket.
 * @property name The name of the bucket.
 * @property description The description of the bucket.
 * @property points The points of the bucket.
 * @property created The creation date of the bucket.
 * @property updated The last update date of the bucket.
 */
data class AddBucketResponse(
    val id: BucketId,
    val name: String,
    val description: String,
    val points: BigDecimal,
    val created: Instant,
    val updated: Instant,
)
