package nl.rug.digitallab.grading.enums

/**
 * Represents the different sources from which a criterion's points can be derived in the grading system.
 *
 * @property code The numeric representation associated with the source type.
 *
 * Types:
 * - BUCKET: Points are derived from a predefined bucket.
 * - EXTERNAL: Points are fetched from an external source.
 * - MANUAL: Points are manually assigned by a grader.
 * - CONSTANT: Points are set as a constant value.
 */
enum class CriterionSource(val code: Int) {
    /**
     * Indicates that a criterion's points are derived from a predefined bucket in the grading system.
     * Represents a source type within the [CriterionSourceRequest] enumeration.
     */
    BUCKET(1),

    /**
     * Indicates that a criterion's points are fetched from an external source.
     * Represents a source type within the [CriterionSourceRequest] enumeration.
     */
    EXTERNAL(2),

    /**
     * Represents a source type in the grading system where points are manually assigned by a grader.
     */
    MANUAL(3),

    /**
     * Represents a source type in the grading system where the points for a criterion are set as a constant value.
     * This value is predefined and does not depend on external input, manual grading, or buckets.
     */
    CONSTANT(4),
}
