package nl.rug.digitallab.grading.exceptions
import nl.rug.digitallab.common.quarkus.exception.mapper.rest.MappedException
import nl.rug.digitallab.grading.BucketId
import org.jboss.resteasy.reactive.RestResponse.Status.NOT_FOUND

/**
 * Exception to throw when a bucket is not found.
 */
class BucketNotFoundException(message: String) : MappedException(message, NOT_FOUND) {
    constructor(bucketId: BucketId) :
            this("Bucket with id $bucketId doest not exist.")
}
