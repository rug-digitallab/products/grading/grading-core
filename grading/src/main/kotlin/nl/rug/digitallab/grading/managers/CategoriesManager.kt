package nl.rug.digitallab.grading.managers

import io.quarkus.hibernate.reactive.panache.common.WithSession
import io.quarkus.hibernate.reactive.panache.common.WithTransaction
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.grading.CategoryId
import nl.rug.digitallab.grading.RubricId
import nl.rug.digitallab.grading.dtos.requests.AddCategoryRequest
import nl.rug.digitallab.grading.dtos.responses.AddCategoryResponse
import nl.rug.digitallab.grading.persistence.entities.Category
import nl.rug.digitallab.grading.persistence.entities.Rubric
import nl.rug.digitallab.grading.persistence.repositories.CategoryRepository
import nl.rug.digitallab.grading.persistence.repositories.RubricRepository

/**
 * The [CategoriesManager] is the component that manages all of the [Rubric]s. It contains all business logic needed
 */
@ApplicationScoped
class CategoriesManager {
    @Inject
    private lateinit var categoryRepository: CategoryRepository

    @Inject
    private lateinit var rubricRepository: RubricRepository

    /**
     * Get the category from the [CategoryRepository]
     *
     * @param categoryId The id of the category needed
     *
     * @return The category if found
     *
     * @throws [CategoryNotFoundException] if the category was not found
     */
    @WithSession
    fun getCategory(categoryId: CategoryId): Uni<AddCategoryResponse> {
        return categoryRepository.findByIdOrThrow(categoryId)
            .map(Category::toDTO)
    }

    /**
     * Add a category to [Rubric].
     *
     * @param rubricId The id of the rubric to add the category to
     * @param request The DTO representing the category to add
     *
     * @return The new category if the rubric was found
     *
     * @throws [RubricNotFoundException] if the rubric was not found
     */
    @WithTransaction
    fun addCategory(
        rubricId: RubricId,
        request: AddCategoryRequest,
    ): Uni<AddCategoryResponse> {
        return rubricRepository.findByIdOrThrow(rubricId)
            .map { rubric ->
                Category(
                    name = request.name,
                    rubric = rubric,
                    description = request.description,
                    scalePointsTo = request.scalePointsTo
                )
            }
            .flatMap(categoryRepository::create)
            .map(Category::toDTO)
    }

    /**
     * Update a category.
     *
     * Note: A category may not update its rubric
     *
     * @param categoryId The id of the category to be updated
     * @param request The DTO representing the new / updated category
     *
     * @return The new and updated category if found
     *
     * @throws [CategoryNotFoundException] if the category was not found
     */
    @WithTransaction
    fun updateCategory(
        categoryId: CategoryId,
        request: AddCategoryRequest,
    ): Uni<AddCategoryResponse> {
        return categoryRepository.updateWithOrThrow(categoryId) { oldCategory ->
            oldCategory.name = request.name
            oldCategory.description = request.description
            oldCategory.scalePointsTo = request.scalePointsTo
            oldCategory
        }.map(Category::toDTO)
    }

    /**
     * Delete a category.
     *
     * @param categoryId The id of the category to deleted
     *
     * @return Nothing, Unit.
     *
     * @throws [CategoryNotFoundException] if the category was not found
     */
    @WithTransaction
    fun deleteCategory(categoryId: CategoryId): Uni<Unit> =
        categoryRepository.deleteWithOrThrow(categoryId)
}
