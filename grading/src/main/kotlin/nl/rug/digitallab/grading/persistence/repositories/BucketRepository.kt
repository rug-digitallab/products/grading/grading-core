package nl.rug.digitallab.grading.persistence.repositories

import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import nl.rug.digitallab.common.quarkus.hibernate.reactive.panache.repositories.BaseRepository
import nl.rug.digitallab.grading.BucketId
import nl.rug.digitallab.grading.exceptions.BucketNotFoundException
import nl.rug.digitallab.grading.persistence.entities.Bucket

/**
 * Bucket Repository class for handling operations related to Bucket entities.
 */
@ApplicationScoped
class BucketRepository : BaseRepository<Bucket, BucketId>() {
    /**
     * Find a bucket by ID or throw [BucketNotFoundException] if not found
     *
     * @param bucketId The ID of the bucket to search for
     *
     * @return The found [Bucket]
     *
     * @throws [BucketNotFoundException]
     */
    fun findByIdOrThrow(bucketId: BucketId): Uni<Bucket> = findByIdOrThrow(bucketId) { BucketNotFoundException(bucketId) }

    /**
     * Delete a bucket by ID or throw [BucketNotFoundException] if not found
     *
     * @param bucketId The ID of the bucket to delete
     *
     * @return Unit if the bucket was found and deleted
     *
     * @throws [BucketNotFoundException]
     */
    fun deleteWithOrThrow(bucketId: BucketId): Uni<Unit> = deleteByIdOrThrow(bucketId) { BucketNotFoundException(bucketId) }

    /**
     * Update a bucket by ID or throw [BucketNotFoundException] if not found
     *
     * @param bucketId The ID of the bucket to update
     * @param bucket A callback giving access to the old entity, allowing the caller to update the fields
     *
     * @return The found [Bucket]
     *
     * @throws [BucketNotFoundException]
     */
    fun updateWithOrThrow(
        bucketId: BucketId,
        bucket : (Bucket) -> Bucket,
    ): Uni<Bucket> = updateWithOrThrow(bucketId,  { BucketNotFoundException(bucketId) }, bucket)
}
