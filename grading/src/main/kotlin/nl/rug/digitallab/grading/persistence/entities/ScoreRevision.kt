package nl.rug.digitallab.grading.persistence.entities

import jakarta.persistence.*
import jakarta.validation.constraints.NotNull
import nl.rug.digitallab.grading.ScoreRevisionId
import nl.rug.digitallab.grading.UserId
import nl.rug.digitallab.grading.persistence.listeners.ScoreRevisionListener
import org.hibernate.annotations.CreationTimestamp
import java.math.BigDecimal
import java.time.Instant

/**
 * A single revision of a score.
 *
 * @property id The ID of this revision.
 * @property score The score this revision belongs to.
 * @property created When this revision was created.
 * @property grader The user grading the score.
 * @property points The grade achieved
 * @property bucket The bucket this grade links to if any
 */
@Entity
@EntityListeners(ScoreRevisionListener::class)
class ScoreRevision (
    @field:NotNull(message = "Grader cannot be null")
    var grader: UserId,

    @field:ManyToOne(fetch = FetchType.EAGER)
    val score: Score,

    @field:ManyToOne(fetch = FetchType.EAGER)
    var bucket: Bucket? = null,

    var points: BigDecimal? = null,
) {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    lateinit var id: ScoreRevisionId

    @CreationTimestamp
    lateinit var created: Instant
}
