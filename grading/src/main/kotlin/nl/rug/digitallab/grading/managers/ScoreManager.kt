package nl.rug.digitallab.grading.managers

import io.quarkus.hibernate.reactive.panache.common.WithSession
import io.quarkus.hibernate.reactive.panache.common.WithTransaction
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.grading.AssessmentId
import nl.rug.digitallab.grading.BucketId
import nl.rug.digitallab.grading.ScoreId
import nl.rug.digitallab.grading.UserId
import nl.rug.digitallab.grading.dtos.requests.AddScoreRequest
import nl.rug.digitallab.grading.dtos.requests.UpdateScoreRequest
import nl.rug.digitallab.grading.dtos.responses.AddScoreResponse
import nl.rug.digitallab.grading.exceptions.BucketDoesNotBelongException
import nl.rug.digitallab.grading.persistence.entities.Bucket
import nl.rug.digitallab.grading.persistence.entities.Criterion
import nl.rug.digitallab.grading.persistence.entities.Score
import nl.rug.digitallab.grading.persistence.entities.ScoreRevision
import nl.rug.digitallab.grading.persistence.repositories.AssessmentRepository
import nl.rug.digitallab.grading.persistence.repositories.CriterionRepository
import nl.rug.digitallab.grading.persistence.repositories.ScoreRepository
import org.hibernate.reactive.mutiny.Mutiny

/**
 * ScoreManager handles all business logic related to managing scores, including adding,
 * updating, retrieving, and deleting scores in the grading system.
 */
@ApplicationScoped
class ScoreManager {
    @Inject
    private lateinit var scoreRepository: ScoreRepository

    @Inject
    private lateinit var criterionRepository: CriterionRepository

    @Inject
    private lateinit var assessmentRepository: AssessmentRepository

    /**
     * Add a new score to a specific assessment
     *
     * @param assessmentId The id of the assessment
     * @param addScoreRequest The request containing score details
     *
     * @return The added [AddScoreResponse] if successful
     */
    @WithTransaction
    fun addScore(
        assessmentId: AssessmentId,
        addScoreRequest: AddScoreRequest,
    ): Uni<AddScoreResponse> {
        return criterionRepository.findByIdOrThrow(addScoreRequest.criterionId)
                .flatMap { criterion ->
                    val bucket = addScoreRequest.bucketId?.let { it ->
                        criterion.findBucketByIdOrThrow(addScoreRequest.bucketId) {
                            BucketDoesNotBelongException(
                                "Bucket ${addScoreRequest.bucketId} does not belong to the template rubric. You may only choose from ${criterion.buckets}"
                            )
                        }
                    }
                    assessmentRepository.findByIdOrThrow(assessmentId)
                        .map { assessment ->
                            val score = Score(
                                criterion = criterion,
                                assessment = assessment,
                            )
                            val scoreRevision = ScoreRevision(
                                    grader = UserId.randomUUID(), // https://gitlab.com/rug-digitallab/products/grading/grading-core/-/issues/23
                                    bucket = bucket,
                                    points = addScoreRequest.points,
                                    score = score,
                            )
                            score.addRevision(scoreRevision)
                            return@map score
                        }.flatMap(scoreRepository::create)
                }.map(Score::toDTO)
    }

    /**
     * Delete a score by its id
     *
     * @param scoreId The id of the score to delete
     *
     * @return A [Uni] that completes when the score is deleted
     */
    @WithTransaction
    fun deleteScore(
        scoreId: ScoreId,
    ): Uni<Unit> = scoreRepository.deleteByIdOrThrow(scoreId)

    /**
     * Update an existing score
     *
     * @param scoreId The id of the score to update
     * @param updateScoreRequest The new score details
     *
     * @return The updated [AddScoreResponse] if successful
     */
    @WithTransaction
    fun updateScore(
        scoreId: ScoreId,
        updateScoreRequest: UpdateScoreRequest,
    ): Uni<AddScoreResponse> {
        return scoreRepository.findByIdOrThrow(scoreId)
                .call { score -> Mutiny.fetch(score.assessment) }
                .call { score -> Mutiny.fetch(score.revisions) }
                .flatMap { score ->
                    val bucket = updateScoreRequest.bucketId?.let { it ->
                        score.criterion.findBucketByIdOrThrow(it) {
                            BucketDoesNotBelongException(
                                "Bucket $it does not belong to the template rubric. You may only choose from ${score.criterion.buckets}"
                            )
                        }
                    }
                    val revision = ScoreRevision(
                        grader = score.latestRevision.grader,
                        score = score,
                        points = updateScoreRequest.points,
                        bucket = bucket,
                    )
                    score.addRevision(revision)
                    scoreRepository.create(score)
                }
                .map(Score::toDTO)
    }

    /**
     * Retrieve a score by its id
     *
     * @param scoreId The id of the score to retrieve
     *
     * @return The found [AddScoreResponse] if the score exists
     */
    @WithSession
    fun findScore(scoreId: ScoreId): Uni<AddScoreResponse> = scoreRepository.findByIdOrThrow(scoreId).map(Score::toDTO)

    fun Criterion.findBucketByIdOrThrow(
        bucketId: BucketId,
        exception: () -> Exception,
    ): Bucket? = buckets
        ?.find { bucket -> bucket.id == bucketId }
        ?: throw exception()
}
