package nl.rug.digitallab.grading.exceptions


import nl.rug.digitallab.common.quarkus.exception.mapper.rest.MappedException
import nl.rug.digitallab.grading.AssignmentId
import org.jboss.resteasy.reactive.RestResponse.Status.NOT_FOUND

/**
 * Exception to throw when an assignment is not found or has no rubrics.
 */
class AssignmentNotFoundException(message: String) : MappedException(message, NOT_FOUND) {
    constructor(assignmentId: AssignmentId) :
            this("Assignment with id $assignmentId doest not exist or has no rubric.")
}
