package nl.rug.digitallab.grading.persistence.entities

import jakarta.persistence.*
import jakarta.validation.constraints.NotNull
import nl.rug.digitallab.grading.ScoreId
import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.JoinFormula
import org.hibernate.annotations.UpdateTimestamp
import java.time.Instant

/**
 * The score class, represents an evaluation of *one* criterion.
 *
 * A score simply keeps track of the grade (and the source of it) of a criterion in an [Assessment].
 *
 * @property id The id of this score.
 * @property revisions The revisions list of this score.
 * @property revisionCount The total number of revisions added to this score.
 * @property latestRevision The latest revision, to be used as the leading one when calculating the grade.
 * @property criterion The criterion this score belongs to.
 * @property assessment The assessment this score belongs to
 * @property created When this score was created
 * @property updated Last time this score was updated
 */
@Entity
@Table(
    uniqueConstraints = [UniqueConstraint(columnNames = ["criterion_id", "assessment_id"])]
)
class Score(
    @field:NotNull(message = "Criterion cannot be null")
    @field:ManyToOne(fetch = FetchType.EAGER)
    var criterion: Criterion,

    @field:NotNull(message = "Assessment cannot be null")
    @field:ManyToOne(fetch = FetchType.LAZY)
    var assessment: Assessment,

    @OneToMany(
        mappedBy = "score",
        cascade = [CascadeType.ALL],
        orphanRemoval = true,
        fetch = FetchType.LAZY
    )
    var revisions: MutableList<ScoreRevision> = mutableListOf()
) {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    lateinit var id: ScoreId

    @CreationTimestamp
    lateinit var created: Instant

    @UpdateTimestamp
    lateinit var updated: Instant

    var revisionCount: Int = 0

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinFormula(value = """
        (
            SELECT sr.id
            FROM score_revision sr
            WHERE sr.score_id = id
            ORDER BY sr.created DESC
            LIMIT 1
        )
    """)
    lateinit var latestRevision: ScoreRevision

    /**
     * Adds a new revision to the score and increments the revision count.
     *
     * @param revision The new revision to be added, representing an updated score state.
     */
    fun addRevision(revision: ScoreRevision) {
        revisions.add(revision)
        revisionCount += 1
    }

    /**
     * Updates the current revision the latest revision. @JoinFormula is not executed upon update / persist,
     * so we need to manually update the current assignment config.
     */
    @PostUpdate
    @PostPersist
    private fun updateLatestRevision() {
        latestRevision = revisions.maxBy(ScoreRevision::created)
    }

    @PrePersist
    @PreUpdate
    private fun validate() = criterionChecks()

    /**
     * Performs validation to ensure that the provided criterion is correct.
     *
     * 1. part of the given list of template criteria.
     *
     * @Throws IllegalStateException exception if the criterion does not belong to the template rubric associated with the current score.
     */
    private fun criterionChecks() {
        val templateCriteria = assessment.templateRubric.categories.flatMap(Category::criteria)
        val selectedCriterion = templateCriteria.find {it.id == criterion.id}
        checkNotNull(selectedCriterion) { "Criterion ${criterion.id} does not belong to template rubric " +
                "with " +
                "id ${assessment.templateRubric.id}. You may only choose from ${templateCriteria.map(Criterion::id)}" }
    }
}
