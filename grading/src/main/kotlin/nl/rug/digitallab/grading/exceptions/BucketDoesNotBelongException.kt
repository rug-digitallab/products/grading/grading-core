package nl.rug.digitallab.grading.exceptions

import nl.rug.digitallab.common.quarkus.exception.mapper.rest.MappedException
import org.jboss.resteasy.reactive.RestResponse.Status.BAD_REQUEST

/**
 * Exception to throw when a bucket does not belong for a rubric.
 */
class BucketDoesNotBelongException(message: String) : MappedException(message, BAD_REQUEST)
