package nl.rug.digitallab.grading.persistence.repositories

import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import nl.rug.digitallab.common.quarkus.hibernate.reactive.panache.repositories.BaseRepository
import nl.rug.digitallab.grading.CategoryId
import nl.rug.digitallab.grading.exceptions.CategoryNotFoundException
import nl.rug.digitallab.grading.persistence.entities.Category

/**
 * Category Repository class for handling operations related to Category entities.
 */
@ApplicationScoped
class CategoryRepository : BaseRepository<Category, CategoryId>() {
    /**
     * Find a category by ID or throw [CategoryNotFoundException] if not found
     *
     * @param categoryId The ID of the category to search for
     *
     * @return The found [Category]
     *
     * @throws [CategoryNotFoundException]
     */
    fun findByIdOrThrow(categoryId: CategoryId): Uni<Category> = findByIdOrThrow(categoryId) { CategoryNotFoundException(categoryId) }

    /**
     * Delete a category by ID or throw [CategoryNotFoundException] if not found
     *
     * @param categoryId The ID of the category to delete
     *
     * @return Unit if found and deleted
     *
     * @throws [CategoryNotFoundException]
     */
    fun deleteWithOrThrow(categoryId: CategoryId): Uni<Unit> = deleteByIdOrThrow(categoryId) { CategoryNotFoundException(categoryId) }

    /**
     * Update a category by ID or throw [CategoryNotFoundException] if not found
     *
     * @param categoryId The ID of the category to update
     * @param category A callback giving access to the old entity, allowing the caller to update the fields
     *
     * @return The updated [Category]
     *
     * @throws [CategoryNotFoundException]
     */
    fun updateWithOrThrow(
        categoryId: CategoryId,
        category : (Category) -> Category,
    ): Uni<Category> = updateWithOrThrow(categoryId, { CategoryNotFoundException(categoryId) }, category)
}
