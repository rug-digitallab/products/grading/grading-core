package nl.rug.digitallab.grading.dtos.requests

import java.math.BigDecimal

/**
 * The [AddBucketRequest] is the request DTO for adding a bucket to a criterion.
 *
 * @property name The name of the bucket.
 * @property description The description of the bucket.
 * @property points The points of the bucket, to be earned.
 */
data class AddBucketRequest(
    val name: String,
    val description: String,
    val points: BigDecimal,
)
