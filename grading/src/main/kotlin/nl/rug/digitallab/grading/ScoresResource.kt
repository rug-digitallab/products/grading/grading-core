package nl.rug.digitallab.grading

import io.micrometer.common.lang.NonNull
import io.smallrye.mutiny.Uni
import jakarta.inject.Inject
import jakarta.validation.Valid
import jakarta.ws.rs.*
import jakarta.ws.rs.core.MediaType
import nl.rug.digitallab.grading.dtos.requests.UpdateScoreRequest
import nl.rug.digitallab.grading.dtos.responses.AddScoreResponse
import nl.rug.digitallab.grading.managers.ScoreManager
import org.jboss.resteasy.reactive.ResponseStatus
import org.jboss.resteasy.reactive.RestPath
import org.jboss.resteasy.reactive.RestResponse.StatusCode

/**
 * Score resource. Handles all incoming requests related to scores in the grading system.
 */
@Path("/api/v1/grading/scores")
class ScoresResource {
    @Inject
    private lateinit var scoreManager: ScoreManager

    /**
     * Delete a score by its id
     *
     * @param scoreId The id of the score to delete
     *
     * @return A [Uni] that completes when the score is deleted
     */
    @DELETE
    @Path("{scoreId}")
    @Produces(MediaType.APPLICATION_JSON)
    @ResponseStatus(StatusCode.OK)
    fun deleteScore(
        @RestPath scoreId: ScoreId,
    ): Uni<Unit> = scoreManager.deleteScore(scoreId)

    /**
     * Update a score by its id
     *
     * @param scoreId The id of the score to update
     * @param updateScoreRequest The new details of the score to be updated
     *
     * @return The updated [AddScoreResponse] if successful
     */
    @PUT
    @Path("{scoreId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ResponseStatus(StatusCode.OK)
    fun updateScore(
        @RestPath scoreId: ScoreId,
        @Valid @NonNull updateScoreRequest: UpdateScoreRequest,
    ): Uni<AddScoreResponse> = scoreManager.updateScore(scoreId, updateScoreRequest)

    /**
     * Get a specific score by its id
     *
     * @param scoreId The id of the score to retrieve
     *
     * @return The found [AddScoreResponse] if the score exists
     */
    @GET
    @Path("{scoreId}")
    @Produces(MediaType.APPLICATION_JSON)
    @ResponseStatus(StatusCode.OK)
    fun getScore(
        @RestPath scoreId: ScoreId,
    ): Uni<AddScoreResponse> = scoreManager.findScore(scoreId)
}
