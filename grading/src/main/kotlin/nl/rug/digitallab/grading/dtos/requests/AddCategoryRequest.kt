package nl.rug.digitallab.grading.dtos.requests

import java.math.BigDecimal

/**
 * The [AddCategoryRequest] class is a data transfer object that is used to create a new category.
 *
 * @property name The name of the category.
 * @property description The description of the category.
 * @property scalePointsTo The scale to which the category is scaled.
 */
data class AddCategoryRequest(
    val name: String,
    val description: String,
    val scalePointsTo: BigDecimal,
)
