package nl.rug.digitallab.grading

import io.micrometer.common.lang.NonNull
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.RequestScoped
import jakarta.inject.Inject
import jakarta.validation.Valid
import jakarta.ws.rs.*
import jakarta.ws.rs.core.MediaType
import nl.rug.digitallab.grading.dtos.requests.AddScoreRequest
import nl.rug.digitallab.grading.dtos.responses.AddAssessmentResponse
import nl.rug.digitallab.grading.dtos.responses.AddScoreResponse
import nl.rug.digitallab.grading.managers.AssessmentsManager
import nl.rug.digitallab.grading.managers.ScoreManager
import org.jboss.resteasy.reactive.ResponseStatus
import org.jboss.resteasy.reactive.RestPath
import org.jboss.resteasy.reactive.RestResponse.StatusCode


/**
 * Assessment resource. Handles all incoming requests regarding assessments and the grading
 * of submissions across the DL.
 */
@Path("/api/v1/grading/assessments")
class AssessmentsResource {
    @Inject
    private lateinit var assessmentsManager: AssessmentsManager

    /**
     * List all assessments
     */
    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    @ResponseStatus(StatusCode.OK)
    fun listAllAssessments(): Uni<List<AddAssessmentResponse>> = assessmentsManager.listAllAssessment()

    /**
     * Get a specific assessment by its id
     *
     * @param assessmentId The id of the assessment to retrieve
     *
     * @return The assessment if found
     */
    @GET
    @Path("/{assessmentId}")
    @Produces(MediaType.APPLICATION_JSON)
    @ResponseStatus(StatusCode.OK)
    fun getAssessment(@RestPath assessmentId: AssessmentId): Uni<AddAssessmentResponse> = assessmentsManager.findAssessment(assessmentId)

    /**
     * Delete a specific assessment by its id
     *
     * @param assessmentId The id of the assessment to delete
     *
     * @return A [Uni] that completes when the assessment is deleted
     */
    @DELETE
    @Path("{assessmentId}")
    @Produces(MediaType.APPLICATION_JSON)
    @ResponseStatus(StatusCode.OK)
    fun deleteAssessment(@RestPath assessmentId: AssessmentId): Uni<Unit> = assessmentsManager.deleteAssessment(assessmentId)

    /**
     * See [ScoresSubResource]
     */
    @Path("{assessmentId}/scores")
    fun scoresSubResource(): Class<ScoresSubResource> = ScoresSubResource::class.java

    /**
     * The scores sub-resource for the assessment. Which is responsible for handling all score related
     * to a specific assessment in combination with a score.
     */
    @RequestScoped
    class ScoresSubResource {
        @Inject
        private lateinit var scoreManager: ScoreManager

        /**
         * Add a new score to a specific assessment
         *
         * @param assessmentId The id of the assessment to add the score to
         * @param addScoreRequest The score details to be added
         *
         * @return The added [AddScoreResponse] if successful
         */
        @PUT
        @Path("")
        @Produces(MediaType.APPLICATION_JSON)
        @Consumes(MediaType.APPLICATION_JSON)
        @ResponseStatus(StatusCode.CREATED)
        fun addScore(
            @RestPath assessmentId: AssessmentId,
            @Valid @NonNull addScoreRequest: AddScoreRequest,
        ): Uni<AddScoreResponse> = scoreManager.addScore(assessmentId, addScoreRequest)
    }
}
