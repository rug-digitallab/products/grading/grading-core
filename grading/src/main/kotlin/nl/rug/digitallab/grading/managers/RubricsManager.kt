package nl.rug.digitallab.grading.managers

import io.quarkus.hibernate.reactive.panache.common.WithSession
import io.quarkus.hibernate.reactive.panache.common.WithTransaction
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.grading.AssignmentId
import nl.rug.digitallab.grading.RubricId
import nl.rug.digitallab.grading.dtos.requests.AddRubricRequest
import nl.rug.digitallab.grading.dtos.responses.AddRubricResponse
import nl.rug.digitallab.grading.exceptions.RubricNotFoundException
import nl.rug.digitallab.grading.persistence.entities.Rubric
import nl.rug.digitallab.grading.persistence.repositories.RubricRepository
import org.jboss.logging.Logger


/**
 * The [RubricsManager] is the component that manages all of the [Rubric]s. It contains all business logic needed
 */
@ApplicationScoped
class RubricsManager {
    @Inject
    private lateinit var rubricRepository: RubricRepository

    @Inject
    private lateinit var log: Logger


    /**
     * Get the rubric from the [RubricRepository]
     *
     * @param rubricId [RubricId] of the rubric wanted
     *
     * @return The rubric if found
     *
     * @throws [RubricNotFoundException] in case the rubric is not found
     */
    @WithSession
    fun getRubric(rubricId: RubricId): Uni<AddRubricResponse> {
        return rubricRepository
            .findByIdOrThrow(rubricId)
            .map(Rubric::toDTO)
    }

    /**
     * Get a rubric by its assignment
     *
     * @param assignmentId The [AssignmentId] to find the rubric for
     *
     * @return The rubric of the assignment
     *
     * @throws [RubricNotFoundException]
     */
    @WithSession
    fun getRubricByAssignment(assignmentId: AssignmentId): Uni<AddRubricResponse> {
        return rubricRepository
            .findByAssignmentIdOrThrow(assignmentId)
            .map(Rubric::toDTO)
    }

    /**
     * Add a rubric to an assignment
     *
     * @param assignmentId The [AssignmentId] to add the rubric to
     * @param request The rubric DTO
     *
     * @return The id of the newly created [Rubric]
     */
    @WithTransaction
    fun addRubric(
        assignmentId: AssignmentId,
        request: AddRubricRequest,
    ): Uni<AddRubricResponse> {
        return rubricRepository
            .create(
                Rubric(
                    assignmentId = assignmentId,
                    name = request.name,
                    description = request.description,
                    scalePointsTo = request.scalePointsTo,
                )
            )
            .invoke { rubricCreated ->
                log.info("Adding Rubric ${rubricCreated.id} to assignment with id : $assignmentId")
            }.map(Rubric::toDTO)
    }

    /**
     * Update a specific rubric
     *
     * @param rubricId The id of the rubric to update
     * @param request The API representation the new rubric
     *
     * @return The updated rubric
     */
    @WithTransaction
    fun updateRubric(
        rubricId: RubricId,
        request: AddRubricRequest,
    ): Uni<AddRubricResponse> {
        return rubricRepository.updateWithOrThrow(rubricId) { oldRubric ->
            oldRubric.name = request.name
            oldRubric.description = request.description
            oldRubric.scalePointsTo = request.scalePointsTo
            oldRubric
        }.map(Rubric::toDTO)
    }

    /**
     * Delete a specific rubric.
     *
     * @param rubricId The id of the rubric to delete
     *
     * @return A [Uni] that completes when the rubric is deleted
     *
     * @throws [RubricNotFoundException] if the rubric is not found
     */
    @WithTransaction
    fun deleteRubric(rubricId: RubricId): Uni<Unit> = rubricRepository.deleteByIdOrThrow(rubricId)

    /**
     * Gets the rubric ready to be associated with an assessment
     */
    fun initializeRubricForAssessment(rubricId: RubricId): Uni<Rubric> =
        // potentially other logic
        rubricRepository.findByIdOrThrow(rubricId)
}
