package nl.rug.digitallab.grading.exceptions

import nl.rug.digitallab.common.quarkus.exception.mapper.rest.MappedException
import nl.rug.digitallab.grading.CategoryId
import org.jboss.resteasy.reactive.RestResponse.Status.NOT_FOUND

/**
 * Exception to throw when a category is not found.
 */
class CategoryNotFoundException(message: String) : MappedException(message, NOT_FOUND) {
    constructor(categoryId: CategoryId) :
            this("Category with id $categoryId doest not exist.")
}
