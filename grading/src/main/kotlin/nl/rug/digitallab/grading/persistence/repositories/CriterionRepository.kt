package nl.rug.digitallab.grading.persistence.repositories

import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import nl.rug.digitallab.common.quarkus.hibernate.reactive.panache.repositories.BaseRepository
import nl.rug.digitallab.grading.CriterionId
import nl.rug.digitallab.grading.exceptions.CriterionNotFoundException
import nl.rug.digitallab.grading.persistence.entities.Criterion

/**
 * Criterion Repository class for handling operations related to Criterion entities.
 */
@ApplicationScoped
class CriterionRepository : BaseRepository<Criterion, CriterionId>() {
    /**
     * Find a criterion by ID or throw [CriterionNotFoundException] if not found
     *
     * @param criterionId The ID of the criterion to search for
     *
     * @return The found [Criterion]
     *
     * @throws [CriterionNotFoundException]
     */
    fun findByIdOrThrow(criterionId: CriterionId): Uni<Criterion> = findByIdOrThrow(criterionId) { CriterionNotFoundException(criterionId) }

    /**
     * Delete a criterion by ID or throw [CriterionNotFoundException] if not found
     *
     * @param criterionId The ID of the criterion to delete
     *
     * @return Unit if found and deleted
     *
     * @throws [CriterionNotFoundException]
     */
    fun deleteWithOrThrow(criterionId: CriterionId): Uni<Unit> = deleteByIdOrThrow(criterionId) { CriterionNotFoundException(criterionId) }

    /**
     * Update a criterion by ID or throw [CriterionNotFoundException] if not found
     *
     * @param criterionId The ID of the criterion to update
     * @param criterion A callback giving access to the old entity, allowing the caller to update the fields
     *
     * @return The updated [Criterion]
     *
     * @throws [CriterionNotFoundException]
     */
    fun updateWithOrThrow(
        criterionId: CriterionId,
        criterion : (Criterion) -> Criterion,
    ): Uni<Criterion> = updateWithOrThrow(criterionId, { CriterionNotFoundException(criterionId) }, criterion)
}
