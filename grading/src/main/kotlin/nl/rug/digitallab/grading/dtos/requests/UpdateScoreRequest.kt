package nl.rug.digitallab.grading.dtos.requests

import nl.rug.digitallab.grading.BucketId
import java.math.BigDecimal

/**
 * The request class for the score of a criterion
 *
 * @property points The grade being assigned.
 * @property bucketId The id of the bucket this score originates from.
 *              **Used when the grade is from a bucket**
 */
data class UpdateScoreRequest(
    val points: BigDecimal? = null,
    val bucketId: BucketId? = null,
)
