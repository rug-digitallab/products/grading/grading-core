package nl.rug.digitallab.grading.exceptions


import nl.rug.digitallab.common.quarkus.exception.mapper.rest.MappedException
import nl.rug.digitallab.grading.RubricId
import org.jboss.resteasy.reactive.RestResponse.Status.NOT_FOUND

/**
 * Exception to throw when a rubric with a specified [RubricId] is not found.
 */
class RubricNotFoundException(message: String) : MappedException(message, NOT_FOUND) {
    constructor(rubricId: RubricId) : this("Rubric with $rubricId not found.")
}
