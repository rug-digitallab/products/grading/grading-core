package nl.rug.digitallab.grading.managers

import nl.rug.digitallab.grading.dtos.responses.*
import nl.rug.digitallab.grading.persistence.entities.*

/**
 * Converts a [Rubric] to a [AddRubricResponse].
 *
 * @receiver The rubric to convert.
 * @return The converted rubric.
 * @see AddRubricResponse
 */
fun Rubric.toDTO(): AddRubricResponse {
    return AddRubricResponse(
        id = id,
        assignmentId = assignmentId,
        name = name,
        description = description,
        categories = categories.map(Category::toDTO).toSet(),
        scalePointsTo = scalePointsTo,
        created = created,
        updated = updated,
    )
}

/**
 * Converts a [Category] to a [AddCategoryResponse].
 *
 * @receiver The category to convert.
 * @return The converted category.
 * @see AddCategoryResponse
 */
fun Category.toDTO(): AddCategoryResponse {
    return AddCategoryResponse(
        id = id,
        name = name,
        description = description,
        criteria = criteria.map(Criterion::toDTO).toSet(),
        scalePointsTo = scalePointsTo,
        created = created,
        updated = updated,
    )
}

/**
 * Converts a [Criterion] to a [AddCriterionResponse].
 *
 * @receiver The criterion to convert.
 * @return The converted criterion.
 * @see AddCriterionResponse
 */
fun Criterion.toDTO(): AddCriterionResponse {
    return AddCriterionResponse(
        id = id,
        name = name,
        description = description,
        buckets = buckets?.map(Bucket::toDTO)?.toSet(),
        allowOverride = allowOverride,
        externalPoints = externalPoints,
        points = points,
        source = source,
        created = created,
        updated = updated,
    )
}

/**
 * Converts a [Bucket] to a [AddBucketResponse].
 *
 * @receiver The bucket to convert.
 * @return The converted bucket.
 * @see AddBucketResponse
 */
fun Bucket.toDTO(): AddBucketResponse {
    return AddBucketResponse(
        id = id,
        name = name,
        description = description,
        points = points,
        created = created,
        updated = updated,
    )
}

/**
 * Converts an [Assessment] to its [AddAssessmentResponse] including all necessary fields.
 */
fun Assessment.toDTO(): AddAssessmentResponse {
    return AddAssessmentResponse(
        id = id,
        grade = grade,
        scores = scores.map(Score::toDTO),
        rubricId = templateRubric.id,
        created = created,
        updated = updated,
    )
}

/**
 * Converts a [ScoreRequest] to [Score] converting all necessary fields
 */
fun Score.toDTO(): AddScoreResponse {
    return AddScoreResponse(
        id = id,
        criterionId = criterion.id,
        grader = latestRevision.grader,
        points = latestRevision.points!!,
        bucketId = latestRevision.bucket?.id,
        assessmentId = assessment.id,
        created = created,
        updated = latestRevision.created,
    )

}

