package nl.rug.digitallab.grading

import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.RequestScoped
import jakarta.inject.Inject
import jakarta.validation.Valid
import jakarta.validation.constraints.NotNull
import jakarta.ws.rs.*
import jakarta.ws.rs.core.MediaType
import nl.rug.digitallab.grading.dtos.requests.AddCategoryRequest
import nl.rug.digitallab.grading.dtos.requests.AddCriterionRequest
import nl.rug.digitallab.grading.dtos.responses.AddCategoryResponse
import nl.rug.digitallab.grading.dtos.responses.AddCriterionResponse
import nl.rug.digitallab.grading.managers.CategoriesManager
import nl.rug.digitallab.grading.managers.CriteriaManager
import org.jboss.resteasy.reactive.ResponseStatus
import org.jboss.resteasy.reactive.RestPath
import org.jboss.resteasy.reactive.RestResponse.StatusCode

/**
 * The [CategoriesResource]. Handles all incoming requests related to categories.
 */
@Path("/api/v1/grading/categories")
class CategoriesResource {
    @Inject
    private lateinit var categoriesManager: CategoriesManager

    /**
     * Get a category by its id.
     *
     * @param categoryId The category id to get.
     *
     * @return The [AddCategoryResponse] of the found Category, if found
     *
     * @throws [CategoryNotFoundException] if the category was not found
     */
    @GET
    @Path("{categoryId}")
    @Produces(MediaType.APPLICATION_JSON)
    fun getCategory(
        @RestPath categoryId: CategoryId,
    ): Uni<AddCategoryResponse> = categoriesManager.getCategory(categoryId)

    /**
     * Update a category by its id.
     *
     * @param categoryId The category id to update.
     * @param categoryRequest The new category.
     *
     * @return The [AddCategoryResponse] of the updated Category, if found
     *
     * @throws [CategoryNotFoundException] if the category was not found
     */
    @PUT
    @Path("{categoryId}")
    @ResponseStatus(StatusCode.OK)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    fun updateCategory(
        @RestPath categoryId: CategoryId,
        @Valid @NotNull categoryRequest: AddCategoryRequest,
    ): Uni<AddCategoryResponse> = categoriesManager.updateCategory(categoryId, categoryRequest)

    /**
     * Delete a category by its id.
     *
     * @param categoryId The category id to update.
     *
     * @return Nothing, Unit
     *
     * @throws [CategoryNotFoundException] if the category was not found
     */
    @DELETE
    @Path("{categoryId}")
    @ResponseStatus(StatusCode.NO_CONTENT)
    fun deleteCategory(
        @RestPath categoryId: CategoryId,
    ): Uni<Unit> = categoriesManager.deleteCategory(categoryId)

    /**
     * See [CriteriaSubResource]
     */
    @Path("{categoryId}/criteria")
    fun criteriaSubResource(): Class<CriteriaSubResource> = CriteriaSubResource::class.java

    /**
     * The criteria sub-resource for the category. Which is responsible for handling all criteria related
     * to a specific category in combination with a criterion.
     */
    @RequestScoped
    class CriteriaSubResource {
        @Inject
        private lateinit var criteriaManager: CriteriaManager

        /**
         * Create a new criterion and add it to the category, so also a rubric.
         *
         * @param categoryId The category id to add the criterion to.
         * @param criterionRequest The criterion as a DTO, using the [AddCriterionRequest] as an API representation.
         *
         * @return The [AddCriterionResponse] of the created Criterion, if created
         *
         * @throws [CategoryNotFoundException] if the category was not found
         */
        @POST
        @Path("")
        @ResponseStatus(StatusCode.CREATED)
        @Produces(MediaType.APPLICATION_JSON)
        @Consumes(MediaType.APPLICATION_JSON)
        fun createCriterion(
            @RestPath categoryId: CategoryId,
            @Valid @NotNull criterionRequest: AddCriterionRequest,
        ): Uni<AddCriterionResponse> = criteriaManager.addCriterion(categoryId, criterionRequest)
    }
}
