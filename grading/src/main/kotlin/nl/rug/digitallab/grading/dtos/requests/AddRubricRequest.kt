package nl.rug.digitallab.grading.dtos.requests

import java.math.BigDecimal

/**
 * The [AddRubricRequest] class is a data transfer object that is used to create a new rubric.
 *
 * @property name The name of the rubric.
 * @property description The description of the rubric.
 * @property scalePointsTo The scale that the rubric is scaled to.
 */
data class AddRubricRequest(
    val name: String,
    val description: String,
    val scalePointsTo: BigDecimal,
)
