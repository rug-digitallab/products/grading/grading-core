package nl.rug.digitallab.grading.persistence.entities

import jakarta.persistence.*
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull
import jakarta.validation.constraints.Size
import nl.rug.digitallab.grading.AssignmentId
import nl.rug.digitallab.grading.RubricId
import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.math.BigDecimal
import java.time.Instant

/**
 * Represents a grading rubric which defines the criteria for scoring and evaluating an assignment.
 * The rubric consists of various categories each contributing to the total score, which can be scaled.
 *
 * @property id The id of the rubric.
 * @property name The name of the rubric, must not be null or blank.
 * @property description Description of the rubric, providing further details if necessary.
 * @property categories A list of categories for the rubric
 * @property scalePointsTo The range to which the rubric should be scaled.
 * @property assignmentId The identifier of the assignment that this rubric is linked to.
 * @property created The timestamp when the rubric was initially created, automatically set.
 * @property updated The timestamp of the last update made to the rubric, automatically set.
 */
@Entity
class Rubric(
    @field:Column(updatable = false)
    var assignmentId: AssignmentId,

    @field:NotBlank(message = " Rubric name cannot be null or blank")
    var name: String,

    @field:NotNull(message = "Bucket description cannot be null")
    @field:Size(max = 10000, message = "Description cannot be longer than 10000 characters")
    var description: String,

    @field:OneToMany(
        mappedBy = "rubric",
        cascade = [CascadeType.ALL],
        orphanRemoval = true,
        fetch = FetchType.EAGER
    )
    var categories: MutableSet<Category> = mutableSetOf(),

    var scalePointsTo: BigDecimal,
) {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    lateinit var id: RubricId

    @CreationTimestamp
    lateinit var created: Instant

    @UpdateTimestamp
    lateinit var updated: Instant
}
