package nl.rug.digitallab.grading

import java.util.UUID

/**
 * Type aliases for various IDs used across the grading system, all based on [UUID].
 */
typealias UserId = UUID
typealias RubricId = UUID
typealias AssignmentId = UUID
typealias CriterionId = UUID
typealias BucketId = UUID
typealias CategoryId = UUID
typealias AssessmentId = UUID
typealias ScoreId = UUID
typealias SubmissionId = UUID
typealias ScoreRevisionId = UUID
