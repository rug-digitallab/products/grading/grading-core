package nl.rug.digitallab.grading.managers

import io.quarkus.hibernate.reactive.panache.common.WithSession
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.grading.AssessmentId
import nl.rug.digitallab.grading.RubricId
import nl.rug.digitallab.grading.dtos.responses.AddAssessmentResponse
import nl.rug.digitallab.grading.persistence.entities.Assessment
import nl.rug.digitallab.grading.persistence.repositories.AssessmentRepository
import org.hibernate.reactive.mutiny.Mutiny

/**
 * Manages operations related to assessments, including listing, finding,
 * deleting, and starting assessments based on rubrics.
 */
@ApplicationScoped
class AssessmentsManager {
    @Inject
    private lateinit var rubricsManager: RubricsManager

    @Inject
    private lateinit var assessmentRepository: AssessmentRepository

    /**
     * Retrieves a list of all assessments.
     *
     * @return A [Uni] emitting a list of [AddAssessmentResponse] objects representing all assessments.
     */
    @WithSession
    fun listAllAssessment(): Uni<List<AddAssessmentResponse>> =
        assessmentRepository.listAll().map { it.map(Assessment::toDTO) }

    /**
     * Finds an assessment by its ID or throws an exception if not found.
     *
     * @param assessmentId The ID of the assessment to find.
     * @return A [Uni] emitting an [AddAssessmentResponse] representing the found assessment.
     */
    @WithSession
    fun findAssessment(assessmentId: AssessmentId): Uni<AddAssessmentResponse> =
        assessmentRepository.findByIdOrThrow(assessmentId).map(Assessment::toDTO)

    /**
     * Deletes an assessment by its ID.
     *
     * @param assessmentId The ID of the assessment to delete.
     * @return A [Uni] indicating the completion of the delete operation.
     */
    @WithSession
    fun deleteAssessment(assessmentId: AssessmentId): Uni<Unit> =
        assessmentRepository.deleteByIdOrThrow(assessmentId)

    /**
     * Starts an assessment for the specified rubric, initializing the rubric
     * and creating a new assessment.
     *
     * @param rubricId The ID of the rubric for which the assessment is being started.
     * @return A [Uni] emitting a [AddAssessmentResponse] representing the newly created assessment.
     */
    @WithSession
    fun startAssessment(rubricId: RubricId): Uni<AddAssessmentResponse> {
        return rubricsManager.initializeRubricForAssessment(rubricId)
            .onItem().transformToUni { rubric ->
                // make sure rubric exists
                val assessment = Assessment(rubric)
                assessmentRepository.create(assessment)
            }
            .call { assessment -> Mutiny.fetch(assessment.templateRubric)}
            .map(Assessment::toDTO)
    }
}
