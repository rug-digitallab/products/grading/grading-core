package nl.rug.digitallab.grading.managers

import io.quarkus.hibernate.reactive.panache.common.WithSession
import io.quarkus.hibernate.reactive.panache.common.WithTransaction
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.grading.BucketId
import nl.rug.digitallab.grading.CriterionId
import nl.rug.digitallab.grading.dtos.requests.AddBucketRequest
import nl.rug.digitallab.grading.dtos.responses.AddBucketResponse
import nl.rug.digitallab.grading.persistence.entities.Bucket
import nl.rug.digitallab.grading.persistence.entities.Rubric
import nl.rug.digitallab.grading.persistence.repositories.*

/**
 * The [BucketsManager] is the component that manages all of the [Rubric]s. It contains all business logic needed
 */
@ApplicationScoped
class BucketsManager {
    @Inject
    private lateinit var bucketRepository: BucketRepository

    @Inject
    private lateinit var criterionRepository: CriterionRepository

    /**
     * Get the bucket from the [BucketRepository]
     *
     * @param bucketId The id of the bucket needed
     *
     * @return The bucket if found
     *
     * @throws [BucketNotFoundException] if the category was not found
     */
    @WithSession
    fun getBucket(bucketId: BucketId): Uni<AddBucketResponse> =
        bucketRepository.findByIdOrThrow(bucketId).map(Bucket::toDTO)

    /**
     * Add a bucket to a [Criterion] subsequently adding it to category and thus a rubric
     *
     * @param criterionId The id of the criterion to add the bucket to
     * @param request The DTO representing the bucket to add
     *
     * @return The bucket if found the criterion was found
     *
     * @throws [CriterionNotFoundException] if the category was not found
     */
    @WithTransaction
    fun addBucket(
        criterionId: CriterionId,
        request: AddBucketRequest,
    ): Uni<AddBucketResponse> {
        return criterionRepository.findByIdOrThrow(criterionId)
            .map { criterion ->
                Bucket(
                    criterion = criterion,
                    name = request.name,
                    description = request.description,
                    points = request.points
                )
            }
            .flatMap(bucketRepository::create)
            .map(Bucket::toDTO)
    }

    /**
     * Update a bucket.
     *
     * Note: A bucket may not update its criterion
     *
     * @param bucketId The id of the bucket to be updated
     * @param request The DTO representing the new / updated bucket
     *
     * @return The new and updated bucket if found
     *
     * @throws [BucketNotFoundException] if the category was not found
     */
    @WithTransaction
    fun updateBucket(
        bucketId: BucketId,
        request: AddBucketRequest,
    ): Uni<AddBucketResponse> {
        return bucketRepository.updateWithOrThrow(bucketId) { oldBucket ->
            oldBucket.name = request.name
            oldBucket.points = request.points
            oldBucket.description = request.description
            oldBucket
        }.map(Bucket::toDTO)
    }

    /**
     * Delete a bucket.
     *
     * @param bucketId The id of the bucket to deleted
     *
     * @return Nothing, Unit.
     *
     * @throws [BucketNotFoundException] if the category was not found
     */
    @WithTransaction
    fun deleteBucket(bucketId: BucketId): Uni<Unit> =
        bucketRepository.deleteWithOrThrow(bucketId)
}
