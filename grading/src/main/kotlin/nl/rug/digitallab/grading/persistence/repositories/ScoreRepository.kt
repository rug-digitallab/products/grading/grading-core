package nl.rug.digitallab.grading.persistence.repositories

import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import nl.rug.digitallab.common.quarkus.hibernate.reactive.panache.repositories.BaseRepository
import nl.rug.digitallab.grading.ScoreId
import nl.rug.digitallab.grading.exceptions.ScoreNotFoundException
import nl.rug.digitallab.grading.persistence.entities.Score

/**
 * Repository class for handling operations related to the [Score] entity.
 * Extends [BaseRepository] and provides methods to find, update, and delete scores
 * while throwing [ScoreNotFoundException] if the score is not found.
 */
@ApplicationScoped
class ScoreRepository : BaseRepository<Score, ScoreId>() {
    /**
     * Find a score by its id or throw [ScoreNotFoundException] if not found.
     *
     * @param scoreId The id of the score to search for
     * @return The found [Score]
     * @throws [ScoreNotFoundException] if the score is not found
     */
    fun findByIdOrThrow(scoreId: ScoreId): Uni<Score> = findByIdOrThrow(scoreId) { ScoreNotFoundException(scoreId) }

    /**
     * Delete a score by its id or throw [ScoreNotFoundException] if not found.
     *
     * @param scoreId The id of the score to delete
     * @return A [Uni] that completes when the score is deleted
     * @throws [ScoreNotFoundException] if the score is not found
     */
    fun deleteByIdOrThrow(scoreId: ScoreId): Uni<Unit> = deleteByIdOrThrow(scoreId) { ScoreNotFoundException(scoreId) }
}

