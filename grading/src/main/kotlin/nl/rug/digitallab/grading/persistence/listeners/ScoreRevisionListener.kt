package nl.rug.digitallab.grading.persistence.listeners

import jakarta.persistence.PrePersist
import jakarta.persistence.PreUpdate
import nl.rug.digitallab.grading.enums.CriterionSource
import nl.rug.digitallab.grading.persistence.entities.ScoreRevision
import java.math.BigDecimal

/**
 * Entity listener that will listen to any updates to the [ScoreRevision] class.
 */
class ScoreRevisionListener {
    /**
     * Validates the integrity and consistency of the score revision before persisting or updating it.
     *
     * This method ensures the following:
     * - The selected criterion in the score matches the criteria defined in the associated template rubric.
     * - Essential configuration checks are performed based on the criterion source (e.g., bucket, constant, manual, or external),
     *   ensuring the score complies with the associated criterion configuration.
     *
     * The method performs two primary validation steps:
     * 1. Criterion validation using `criterionChecks` to verify that the selected criterion exists in the template rubric
     *    and is correctly associated with the score.
     * 2. Configuration checks using `configurationChecks` to validate that the score conforms to the expected setup
     *    determined by the criterion source.
     *
     * Throws:
     * - RuntimeException if the selected criterion does not belong to the template rubric.
     * - Other exceptions as applicable during configuration validation, ensuring integrity of the score.
     */
    @PrePersist
    @PreUpdate
    fun validate(scoreRevision: ScoreRevision) {
        scoreRevision.configurationChecks()
    }

    /**
     * Performs configuration checks for the current score revision based on the criterion's source type.
     *
     * This method determines the appropriate processing flow based on the source of the criterion's points.
     * Depending on the `CriterionSource`, it invokes one of the following methods:
     *
     * - [bucketSourceFlow]: For criteria with a source of type `BUCKET`, validates and processes the bucket selection.
     * - [constantSourceFlow]: For criteria with a constant source type, assigns the predefined points directly.
     * - [manualSourceFlow]: For criteria with a manual source type, validates and processes the manually assigned points.
     * - [externalSourceFlow]: For criteria with an external source type, prepares handling for external point sources.
     */
    private fun ScoreRevision.configurationChecks() {
        // check to see indeed the config matches
        // we check the template rubric with the received score
        when(score.criterion.source) {
            CriterionSource.BUCKET -> bucketSourceFlow()
            CriterionSource.CONSTANT -> constantSourceFlow()
            CriterionSource.MANUAL -> manualSourceFlow()
            CriterionSource.EXTERNAL -> externalSourceFlow()
        }
    }

    /**
     * Handles the flow for manually assigning a score to a criterion in a grading process.
     *
     * This method ensures that when the score is set manually, the provided points fall within the
     * bounds of the allowed points for the criterion. If a maximum limit of points is defined for
     * the associated criterion, the assigned points will be constrained by this limit.
     *
     * The method performs the following:
     * - Verifies that the `points` field is not null, throwing an exception if no manual grade is provided.
     * - Checks if the associated criterion has a maximum allowable points limit.
     * - Assigns and optionally scales the points based on the provided manual score and the criterion's limit.
     *
     * Throws:
     * - IllegalStateException if `points` is null when a manual score is expected.
     */
    private fun ScoreRevision.manualSourceFlow() {
        checkNotNull(points) { "Score $id is set to manual, but no grade is provided" }
        if (score.criterion.points != null) {
            assignPointsAndScale(
                minOf(points!!, score.criterion.points!!)
            )
        } else {
            assignPointsAndScale(points!!)
        }
    }

    /**
     * Handles the constant scoring source flow for the current score revision.
     *
     * This method directly assigns the predefined points from the scoring criterion to the score,
     * bypassing any supplemental validation or calculation logic. It ensures that the assigned points
     * align with the defined criterion.
     *
     * Implementation Details:
     * - The method retrieves the predefined points from the criterion associated with the score.
     * - It invokes `assignPointsAndScale(assignedPoints)` to apply the points to the current score revision,
     *   accounting for any necessary scaling logic handled within `assignPointsAndScale`.
     *
     * Preconditions:
     * - The criterion associated with the score must have a non-null `points` value.
     *
     * Postconditions:
     * - The score will have the criterion's predefined points assigned and scaled appropriately.
     */
    private fun ScoreRevision.constantSourceFlow() {
        assignPointsAndScale(score.criterion.points!!)
    }

    /**
     * Handles the scoring process for an external source.
     *
     * This method is intended to manage the flow of assigning points sourced externally to the scoring
     * system. It delegates the task of assigning these points and applying any necessary scaling to the
     * `assignPointsAndScale` method. The default point value of `BigDecimal.ZERO` is passed, which assumes
     * no points are directly attributed from the external source without additional context.
     */
    private fun ScoreRevision.externalSourceFlow() {
        // Get the external points from source here https://gitlab.com/rug-digitallab/products/grading/grading-core/-/issues/24
        assignPointsAndScale(BigDecimal.ZERO)
    }

    /**
     * Validates and processes the selection of a scoring bucket for the current score revision.
     *
     * This method ensures that the selected scoring bucket is part of the criterion's defined
     * bucket set in the associated template rubric. If the validation succeeds, it assigns the
     * corresponding points to the score after applying necessary scaling.
     *
     * The method performs the following:
     * - Ensures that a valid bucket is selected.
     * - Checks that the selected bucket belongs to the criterion's template rubric to prevent
     *   misuse or invalid associations.
     * - Propagates the points associated with the selected bucket to be applied to the score.
     *
     * Throws an exception if:
     * - No bucket is selected when the method is called.
     * - The selected bucket does not belong to the valid set of buckets in the template rubric.
     */
    private fun ScoreRevision.bucketSourceFlow() {
        val selectedBucket = score.criterion.buckets!!.find { it.id == bucket!!.id }
        assignPointsAndScale(selectedBucket!!.points)
    }

    /**
     * Assigns the provided points to the score, considering whether overrides are allowed.
     * If the associated criterion allows point overrides and an existing value is set for points,
     * the original value is retained. Otherwise, the assigned points are applied.
     *
     * @param assignedPoints The points to be assigned to the score.
     */
    private fun ScoreRevision.assignPointsAndScale(assignedPoints: BigDecimal) {
        if (score.criterion.allowOverride && points != null) return // we allow override, leave points the original value
        points = assignedPoints
    }
}

