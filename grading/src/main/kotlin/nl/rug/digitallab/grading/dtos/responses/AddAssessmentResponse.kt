package nl.rug.digitallab.grading.dtos.responses

import nl.rug.digitallab.grading.AssessmentId
import nl.rug.digitallab.grading.CriterionId
import nl.rug.digitallab.grading.RubricId
import java.math.BigDecimal
import java.time.Instant

/**
 * A response class for the assessment class.
 *
 * @property id The id of the assessment.
 * @property grade The grade for the assessment calculate based on the scores.
 * @property scores The criteria list of [CriterionId] to [AddScoreResponse] defining the grading.
 * @property rubricId The id of the template rubric this assessment is associated with
 * @property created The creation date of the assessment.
 * @property updated The last update date of the assessment.
 */
data class AddAssessmentResponse(
    val id: AssessmentId,
    val grade: BigDecimal?,
    val scores: List<AddScoreResponse>,
    val rubricId: RubricId,
    val created: Instant,
    val updated: Instant,
)
