package nl.rug.digitallab.grading.exceptions

import nl.rug.digitallab.common.quarkus.exception.mapper.rest.MappedException
import nl.rug.digitallab.grading.CriterionId
import org.jboss.resteasy.reactive.RestResponse.Status.NOT_FOUND

/**
 * Exception to throw when a criterion is not found.
 */
class CriterionNotFoundException(message: String) : MappedException(message, NOT_FOUND) {
    constructor(criterionId: CriterionId) :
            this("Criterion with id $criterionId doest not exist.")
}
