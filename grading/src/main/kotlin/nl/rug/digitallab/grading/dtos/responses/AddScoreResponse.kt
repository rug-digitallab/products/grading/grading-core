package nl.rug.digitallab.grading.dtos.responses

import nl.rug.digitallab.grading.*
import java.math.BigDecimal
import java.net.URI
import java.time.Instant

/**
 * The response class for the score of a criterion
 *
 * @property id The id of the score.
 * @property criterionId The id of the criterion this score is linked to
 * @property points The grade of the score.
 * @property grader The userId of the grader, the person who is submitting this request.
 * @property assessmentId The assessment this score belongs to.
 * @property bucketId The id of the bucket this score originates from.
 *              **Used when the grade is from a bucket**
 * @property source The source where this grade can be fetched from.
 *              **Used when the grade is external**
 * @property created The creation date of the score.
 * @property updated The last update date of the score.
 */
data class AddScoreResponse(
    val id: ScoreId,
    val criterionId: CriterionId,
    val assessmentId: AssessmentId,
    val grader: UserId,
    val points: BigDecimal,
    val bucketId: BucketId? = null,
    val source: URI? = null,
    val created: Instant,
    val updated: Instant,
)
