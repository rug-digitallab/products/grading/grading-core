package nl.rug.digitallab.grading.exceptions

import nl.rug.digitallab.common.quarkus.exception.mapper.rest.MappedException
import nl.rug.digitallab.grading.ScoreId
import org.jboss.resteasy.reactive.RestResponse

/**
 * Exception to throw when a score is not found.
 */
class ScoreNotFoundException(message: String) : MappedException(message, RestResponse.Status.NOT_FOUND) {
    constructor(scoreId: ScoreId) : this("Score with id $scoreId not found")
}
