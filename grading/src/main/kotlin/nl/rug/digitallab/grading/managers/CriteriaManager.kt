package nl.rug.digitallab.grading.managers

import io.quarkus.hibernate.reactive.panache.common.WithSession
import io.quarkus.hibernate.reactive.panache.common.WithTransaction
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.grading.CategoryId
import nl.rug.digitallab.grading.CriterionId
import nl.rug.digitallab.grading.dtos.requests.AddCriterionRequest
import nl.rug.digitallab.grading.dtos.responses.AddCriterionResponse
import nl.rug.digitallab.grading.persistence.entities.Criterion
import nl.rug.digitallab.grading.persistence.repositories.*

/**
 * The [CriteriaManager] is the component that manages all of the [Criterion]. It contains all business logic needed
 */
@ApplicationScoped
class CriteriaManager {
    @Inject
    private lateinit var criterionRepository: CriterionRepository

    @Inject
    private lateinit var categoryRepository: CategoryRepository

    /**
     * Get the criterion from the [CriterionRepository]
     *
     * @param criterionId [CriterionId] of the criterion wanted
     *
     * @return The criterion if found
     *
     * @throws [CriterionNotFoundExceptions] in case the criterion is not found
     */
    @WithSession
    fun getCriterion(criterionId: CriterionId): Uni<AddCriterionResponse> {
        return criterionRepository
            .findByIdOrThrow(criterionId)
            .map(Criterion::toDTO)
    }

    /**
     * Add a criterion to a [Category] subsequently adding it to a rubric
     *
     * @param categoryId The id of the category to add the criterion to
     * @param request The [AddCriterionRequest] representing the criterion to add
     *
     * @return A [AddCriterionResponse] of the added [Criterion]
     *
     *  @throws [CategoryNotFoundException] if the category is not found
     */
    @WithTransaction
    fun addCriterion(
        categoryId: CategoryId,
        request: AddCriterionRequest,
    ): Uni<AddCriterionResponse> {
        return categoryRepository.findByIdOrThrow(categoryId)
            .map { category ->
                Criterion(
                    category = category,
                    name = request.name,
                    description = request.description,
                    externalPoints = request.externalPoints,
                    allowOverride = request.allowOverride,
                    source = request.source,
                    points = request.points,
                )
            }
            .flatMap(criterionRepository::create)
            .map(Criterion::toDTO)
    }

    /**
     * Update a specific criterion.
     *
     * Note: a criterion may not update the category it belongs to
     *
     * @param criterionId The id of the criterion to update
     * @param request The API representation the new criterion
     *
     * @return The updated criterion
     *
     * @throws [CriterionNotFoundException] if the criterion is not found
     */
    @WithTransaction
    fun updateCriterion(
        criterionId: CriterionId,
        request: AddCriterionRequest,
    ): Uni<AddCriterionResponse> {
        return criterionRepository.updateWithOrThrow(criterionId) { oldCriterion ->
            oldCriterion.name = request.name
            oldCriterion.description = request.description
            oldCriterion.externalPoints = request.externalPoints
            oldCriterion.allowOverride = request.allowOverride
            oldCriterion.source = request.source
            oldCriterion.points = request.points
            oldCriterion
        }.map(Criterion::toDTO)
    }

    /**
     * Delete a specific criterion.
     *
     * @param criterionId The id of the criterion to delete
     *
     * @return A [Uni] that completes when the criterion is deleted
     *
     * @throws [CriterionNotFoundException] if the criterion is not found
     */
    @WithTransaction
    fun deleteCriterion(criterionId: CriterionId): Uni<Unit> =
        criterionRepository.deleteWithOrThrow(criterionId)
}
