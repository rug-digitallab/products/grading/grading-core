package nl.rug.digitallab.grading.persistence.entities

import jakarta.persistence.*
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull
import nl.rug.digitallab.grading.CategoryId
import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.math.BigDecimal
import java.time.Instant

/**
 * A category in a rubric. This is strictly a way to organize some criteria that fall under the same category.
 * Categories break down the rubric into different areas of assessment, each contributing to the total score
 * based on their weight. Categories contain specific criteria which detail the assessment rules or standards.
 *
 * @property name The name of the category, must not be null or blank.
 * @property rubric The rubric this category belongs to.
 * @property id The id of the category.
 * @property description Description of the category, providing additional detail about what is assessed within
 *                       this category.
 * @property criteria A list of criteria that belong to this category.
 * @property scalePointsTo The range to which the category is scaled. If not specified the category is scaled to
 *                   the maximum score of the rubric.
 * @property created The timestamp when the category was created, automatically set.
 * @property updated The timestamp of the last update made to the category, automatically set.
 */
@Entity
class Category(
    @field:NotBlank(message = "Category name cannot be null or blank")
    var name: String,

    @field:ManyToOne(
        fetch = FetchType.LAZY,
    )
    var rubric: Rubric,

    @field:NotNull(message = "Bucket description cannot be null")
    var description: String,

    @field:OneToMany(
        mappedBy = "category",
        cascade = [CascadeType.ALL],
        orphanRemoval = true,
        fetch = FetchType.EAGER,
    )
    var criteria: MutableSet<Criterion> = mutableSetOf(),

    var scalePointsTo: BigDecimal,
) {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    lateinit var id: CategoryId

    @CreationTimestamp
    lateinit var created: Instant

    @UpdateTimestamp
    lateinit var updated: Instant
}
