package nl.rug.digitallab.grading

import io.smallrye.mutiny.Uni
import jakarta.inject.Inject
import jakarta.validation.Valid
import jakarta.validation.constraints.NotNull
import jakarta.ws.rs.*
import jakarta.ws.rs.core.MediaType
import nl.rug.digitallab.grading.dtos.requests.AddBucketRequest
import nl.rug.digitallab.grading.dtos.responses.AddBucketResponse
import nl.rug.digitallab.grading.managers.BucketsManager
import org.jboss.resteasy.reactive.ResponseStatus
import org.jboss.resteasy.reactive.RestPath
import org.jboss.resteasy.reactive.RestResponse.StatusCode

/**
 * The [BucketsResource]. Handles all incoming requests related to buckets.
 */
@Path("/api/v1/grading/buckets")
class BucketsResource {
    @Inject
    private lateinit var bucketsManager: BucketsManager

    /**
     * Create a new bucket and add it to the criterion, subsequently to a rubric.
     *
     * @param criterionId The criterion id to add the bucket to.
     * @param bucketRequest The bucket as a DTO, using the [AddBucketRequest] as the API representation.
     *
     * @return The [AddBucketResponse] of the created bucket, if created
     *
     * @throws [BucketNotFoundException] if the bucket was not found
     */
    @POST
    @Path("{criterionId}/buckets")
    @ResponseStatus(StatusCode.CREATED)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    fun createBucket(
        @RestPath criterionId: CriterionId,
        @Valid @NotNull bucketRequest: AddBucketRequest,
    ): Uni<AddBucketResponse> = bucketsManager.addBucket(criterionId, bucketRequest)

    /**
     * Get a bucket by its id.
     *
     * @param bucketId The bucket id to get.
     *
     * @return The [AddBucketResponse] of the found bucket, if found
     *
     * @throws [BucketNotFoundException] if the bucket was not found
     */
    @GET
    @Path("{bucketId}")
    @Produces(MediaType.APPLICATION_JSON)
    fun getBucket(
        @RestPath bucketId: BucketId,
    ): Uni<AddBucketResponse> = bucketsManager.getBucket(bucketId)

    /**
     * Update a bucket by its id.
     *
     * @param bucketId The bucket id to get.
     * @param bucketRequest The new bucket
     *
     * @return The [AddBucketResponse] of the updated Criterion, if found
     *
     * @throws [BucketNotFoundException] if the bucket was not found
     */
    @PUT
    @Path("{bucketId}")
    @ResponseStatus(StatusCode.OK)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    fun updateBucket(
        @RestPath bucketId: BucketId,
        @Valid @NotNull bucketRequest: AddBucketRequest,
    ): Uni<AddBucketResponse> = bucketsManager.updateBucket(bucketId, bucketRequest)

    /**
     * Delete a bucket by its id.
     *
     * @param bucketId The bucket id to get.
     *
     * @return Nothing, Unit
     *
     * @throws [BucketNotFoundException] if the bucket was not found
     */
    @DELETE
    @Path("{bucketId}")
    @ResponseStatus(StatusCode.NO_CONTENT)
    fun deleteBucket(
        @RestPath bucketId: BucketId,
    ): Uni<Unit> = bucketsManager.deleteBucket(bucketId)
}
