package nl.rug.digitallab.grading.persistence.repositories

import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import nl.rug.digitallab.common.quarkus.hibernate.reactive.panache.repositories.BaseRepository
import nl.rug.digitallab.grading.AssessmentId
import nl.rug.digitallab.grading.exceptions.AssessmentNotFoundException
import nl.rug.digitallab.grading.persistence.entities.Assessment

/**
 * Assessment repository handles all operations related to [Assessment] and uses the [BaseRepository] class.
 */
@ApplicationScoped
class AssessmentRepository : BaseRepository<Assessment, AssessmentId>() {
    /**
     * Find an assessment by id, or throw a [AssessmentNotFoundException]. And calculate the grade.
     *
     * @param assessmentId The id of the assessment
     *
     * @throws [AssessmentNotFoundException]
     */
    fun findByIdOrThrow(assessmentId: AssessmentId): Uni<Assessment> =
        findByIdOrThrow(assessmentId) { AssessmentNotFoundException(assessmentId) }
            .invoke(Assessment::calculateGrade)

    /**
     * Delete an assessment by its id or throws [AssessmentNotFoundException] if not found
     *
     * @param assessmentId The id of the assessment
     */
    fun deleteByIdOrThrow(assessmentId: AssessmentId): Uni<Unit> = deleteByIdOrThrow(assessmentId) { AssessmentNotFoundException(assessmentId) }
}
