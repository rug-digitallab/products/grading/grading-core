package nl.rug.digitallab.grading.persistence.entities.external

import jakarta.persistence.Embeddable
import java.math.BigDecimal
import java.net.URI

/**
 * Interface to represent a points source for the grading service to fetch the grade from.
 *
 * @property externalSource The relative path to the resource that should serve the points for this criterion.
 * @property scalePointsTo The maximum external score will be scaled proportionally to the given value.
 *
 */
@Embeddable
data class PointsSource (
    val externalSource: URI,
    val scalePointsTo: BigDecimal,
)
