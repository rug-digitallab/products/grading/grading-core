package nl.rug.digitallab.grading.dtos.responses

import nl.rug.digitallab.grading.CriterionId
import nl.rug.digitallab.grading.enums.CriterionSource
import nl.rug.digitallab.grading.persistence.entities.external.PointsSource
import java.math.BigDecimal
import java.time.Instant

/**
 * The [AddCriterionResponse] class is a data transfer object that represents a criterion in the grading service.
 *
 * @property id The id of the criterion.
 * @property name The name of the criterion.
 * @property description The description of the criterion.
 * @property buckets The bucket requests of the criterion.
 * @property allowOverride The custom grade of the criterion.
 * @property externalPoints The external points source if any.
 * @property points The points allowed for this criterion.
 * @property source The derived source of points for this criterion.
 * @property created The creation date of the criterion.
 * @property updated The last update date of the criterion.
 */
data class AddCriterionResponse(
    val id: CriterionId,
    val name: String,
    val description: String,
    val buckets: Set<AddBucketResponse>?,
    val allowOverride: Boolean,
    val source: CriterionSource,
    val externalPoints: PointsSource? = null,
    val points: BigDecimal?,
    val created: Instant,
    val updated: Instant,
)
