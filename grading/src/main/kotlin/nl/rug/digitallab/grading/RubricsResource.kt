package nl.rug.digitallab.grading


import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.RequestScoped
import jakarta.inject.Inject
import jakarta.validation.Valid
import jakarta.validation.constraints.NotNull
import jakarta.ws.rs.*
import jakarta.ws.rs.core.MediaType
import nl.rug.digitallab.grading.dtos.requests.AddCategoryRequest
import nl.rug.digitallab.grading.dtos.requests.AddRubricRequest
import nl.rug.digitallab.grading.dtos.responses.*
import nl.rug.digitallab.grading.managers.AssessmentsManager
import nl.rug.digitallab.grading.managers.CategoriesManager
import nl.rug.digitallab.grading.managers.RubricsManager
import org.jboss.resteasy.reactive.ResponseStatus
import org.jboss.resteasy.reactive.RestPath
import org.jboss.resteasy.reactive.RestResponse.StatusCode

/**
 * The [RubricsResource]. Handles all incoming requests related to rubrics.
 */
@Path("/api/v1/grading/rubrics")
class RubricsResource {
    @Inject
    private lateinit var rubricsManager: RubricsManager

    /**
     * Get the rubric using its [RubricId].
     *
     * @param rubricId The unique identifier of the [Rubric]
     *
     * @return The rubric with the specified id.
     */
    @GET
    @Path("{rubricId}")
    @Produces(MediaType.APPLICATION_JSON)
    fun getRubric(
        @RestPath rubricId: RubricId,
    ): Uni<AddRubricResponse> = rubricsManager.getRubric(rubricId)

    /**
     * Delete a rubric by its [RubricId].
     *
     * @param rubricId The [RubricId] for the rubric to be deleted
     */
    @DELETE
    @Path("{rubricId}")
    @ResponseStatus(StatusCode.NO_CONTENT)
    @Produces(MediaType.APPLICATION_JSON)
    fun deleteRubric(
        @RestPath rubricId: RubricId,
    ): Uni<Unit> = rubricsManager.deleteRubric(rubricId)

    /**
     * Update a rubric given its [RubricId].
     *
     * @param rubricId The [RubricId] of the rubric to update
     * @param rubric The new representation of the rubric
     *
     * @return The updated rubric
     */
    @PUT
    @Path("{rubricId}")
    @ResponseStatus(StatusCode.OK)
    @Produces(MediaType.APPLICATION_JSON)
    fun updateRubric(
        @RestPath rubricId: RubricId,
        @Valid @NotNull rubric: AddRubricRequest,
    ): Uni<AddRubricResponse> = rubricsManager.updateRubric(rubricId, rubric)

    /**
     * Get the rubric for a given assignment by its [AssignmentId]
     *
     * @param assignmentId The id if the assignment to get the rubric for
     *
     * @return The rubric linked to this assignment, if any is found
     */
    @GET
    @Path("assignment/{assignmentId}/")
    @Produces(MediaType.APPLICATION_JSON)
    fun getRubricByAssignment(
        @RestPath assignmentId: AssignmentId,
    ): Uni<AddRubricResponse> = rubricsManager.getRubricByAssignment(assignmentId)

    /**
     * Create a new rubric and add it to the assignment using the [AssignmentId] as the fk.
     *
     * @param assignmentId The assignment id to which rubric belongs to.
     * @param rubricDto The rubric as a DTO, using the [AddRubricRequest] as an API representation of the [Rubric]
     *
     * @return The [AddRubricResponse] of the created rubric
     */
    @POST
    @Path("assignment/{assignmentId}/")
    @ResponseStatus(StatusCode.CREATED)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    fun createRubric(
        @RestPath assignmentId: AssignmentId,
        @Valid @NotNull rubricDto: AddRubricRequest,
    ): Uni<AddRubricResponse> {
        return rubricsManager
            .addRubric(assignmentId, rubricDto)
    }

    /**
     * See [AssessmentsSubResource]
     */
    @Path("{rubricId}/assessments")
    fun assessmentsSubResource(): Class<AssessmentsSubResource> = AssessmentsSubResource::class.java

    /**
     * See [CategoriesSubResource]
     */
    @Path("{rubricId}/categories")
    fun categoriesSubResource(): Class<CategoriesSubResource> = CategoriesSubResource::class.java

    /**
     * The assessments sub-resource for the rubric. Which is responsible for handling all assessment related
     * to a specific rubric in combination with an assessment.
     */
    @RequestScoped
    class AssessmentsSubResource {
        @Inject
        private lateinit var assessmentsManager: AssessmentsManager

        /**
         * Start grading a submission, by its rubric id.
         * Creates an assessment
         */
        @POST
        @Path("")
        @Produces(MediaType.APPLICATION_JSON)
        @ResponseStatus(StatusCode.CREATED)
        fun startGradingSubmission(
            @RestPath rubricId: RubricId,
            /* @RestPath submissionId: SubmissionId, */
        ): Uni<AddAssessmentResponse> {
            // https://gitlab.com/rug-digitallab/products/grading/grading-core/-/issues/25
            return assessmentsManager.startAssessment(rubricId)
        }
    }

    /**
     * The categories sub-resource for the rubric. Which is responsible for handling all category related
     * to a specific rubric in combination with a category.
     */
    @RequestScoped
    class CategoriesSubResource {
        @Inject
        private lateinit var categoriesManager: CategoriesManager

        /**
         * Create a new category and add it to the rubric.
         *
         * @param rubricId The rubric id to add the category to.
         * @param categoryRequest The category as a DTO, using the [AddCategoryRequest] as an API representation
         *
         * @return The [AddCriterionResponse] of the created Criterion, if created
         *
         * @throws [CategoryNotFoundException] if the category was not found
         */
        @POST
        @Path("")
        @ResponseStatus(StatusCode.CREATED)
        @Produces(MediaType.APPLICATION_JSON)
        @Consumes(MediaType.APPLICATION_JSON)
        fun createCategory(
            @RestPath rubricId: RubricId,
            @Valid @NotNull categoryRequest: AddCategoryRequest,
        ): Uni<AddCategoryResponse> = categoriesManager.addCategory(rubricId, categoryRequest)
    }
}
