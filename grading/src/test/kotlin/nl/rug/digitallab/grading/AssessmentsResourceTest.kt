package nl.rug.digitallab.grading

import io.quarkus.test.junit.QuarkusTest
import io.restassured.http.ContentType
import io.restassured.module.kotlin.extensions.*
import jakarta.ws.rs.core.UriBuilder
import nl.rug.digitallab.grading.RubricsResourceTest.Companion.getRubricByIdEndpoint
import nl.rug.digitallab.grading.RubricsResourceTest.Companion.startGradingSubmission
import nl.rug.digitallab.grading.dtos.requests.AddCriterionRequest
import nl.rug.digitallab.grading.dtos.requests.AddScoreRequest
import nl.rug.digitallab.grading.dtos.responses.AddAssessmentResponse
import nl.rug.digitallab.grading.dtos.responses.AddRubricResponse
import nl.rug.digitallab.grading.dtos.responses.AddScoreResponse
import nl.rug.digitallab.grading.enums.CriterionSource
import nl.rug.digitallab.grading.persistence.entities.external.PointsSource
import nl.rug.digitallab.grading.utils.TestUtil
import nl.rug.digitallab.grading.utils.TestUtil.assertBigDecimalEquals
import org.jboss.resteasy.reactive.RestResponse.StatusCode
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.net.URI
import kotlin.reflect.jvm.javaMethod

@QuarkusTest
class AssessmentsResourceTest {
    @Test
    fun `Listing all assessments should return a list of assessments`() {
        // add a sample assessment to the database
        val rubricId = RubricsResourceTest.addRubric(TestUtil.generateAddRubricRequest())
        val assessmentId = startGradingSubmission(rubricId).id

        val assessments = When {
            get(listAllAssessmentsEndpoint())
        } Then {
            statusCode(StatusCode.OK)
        } Extract {
            body().`as`(Array<AddAssessmentResponse>::class.java)
        }

        assertNotEquals(0, assessments.size)
    }

    @Test
    fun `Getting an assessment by id should return the assessment`() {
        // add a sample assessment to the database
        val rubricId = RubricsResourceTest.addRubric(TestUtil.generateAddRubricRequest())
        val assessmentId = startGradingSubmission(rubricId).id

        val assessment = When {
            get(getAssessmentByIdEndpoint(assessmentId))
        } Then {
            statusCode(StatusCode.OK)
        } Extract {
            body().`as`(AddAssessmentResponse::class.java)
        }
        assertEquals(assessmentId, assessment.id)
        assertNotNull(assessment.created)
        assertNotNull(assessment.updated)
        assertBigDecimalEquals(assessment.grade!!, BigDecimal.ZERO)
    }

    @Test
    fun `Getting a non existing assessment should return 400 status code`() {
        val randomId = AssessmentId.randomUUID()

        When {
            delete(getAssessmentByIdEndpoint(randomId))
        } Then {
            statusCode(StatusCode.NOT_FOUND)
        }
    }


    @Test
    fun `Deleting an assessment should return a 200 status code`() {
        // add a sample assessment to the database
        val rubricId = RubricsResourceTest.addRubric(TestUtil.generateAddRubricRequest())
        val assessmentId = startGradingSubmission(rubricId).id

        When {
            delete(deleteAssessmentEndpoint(assessmentId))
        } Then {
            statusCode(StatusCode.OK)
        }
    }

    @Test
    fun `Deleting a non existing assessment should return 400 status code`() {
        val randomId = AssessmentId.randomUUID()

        When {
            delete(deleteAssessmentEndpoint(randomId))
        } Then {
            statusCode(StatusCode.NOT_FOUND)
        }
    }

    @Test
    fun `Creating a score with a bucket source should succeed`() {
        val rubric = generateAndAddCustomCriterionRubric(
            addCriterionRequest = TestUtil.generateAddCriterionRequest().copy(
                source = CriterionSource.BUCKET
            )
        )
        val assessment = startGradingSubmission(rubric.id)
        val criterion = rubric.categories.first().criteria.first()
        val score = TestUtil.generateAddScoreRequest(
            criterionId = criterion.id,
            bucketId = criterion.buckets!!.first().id,
        )
        val addScoreResponse = addScore(assessment.id, score)
        val bucketPoints = criterion.buckets.first().points

        // revision is added afterwards so it shouldn't be equal
        assertNotEquals(addScoreResponse.created, addScoreResponse.updated)
        assertEquals(assessment.id, addScoreResponse.assessmentId)
        assertEquals(criterion.source.code, CriterionSource.BUCKET.code)
        assertEquals(score.bucketId, addScoreResponse.bucketId)
        assertEquals(score.criterionId, addScoreResponse.criterionId)
        assertEquals(bucketPoints, addScoreResponse.points)
    }

    @Test
    fun `Creating a score with a wrong bucket source should fail`() {
        val correctRubric = generateAndAddCustomCriterionRubric(
            addCriterionRequest = TestUtil.generateAddCriterionRequest().copy(
                source = CriterionSource.BUCKET
            )
        )
        val wrongRubric = generateAndAddCustomCriterionRubric(
            addCriterionRequest = TestUtil.generateAddCriterionRequest().copy(
                source = CriterionSource.BUCKET
            )
        )

        val assessment = startGradingSubmission(correctRubric.id)
        val wrongAssessment = startGradingSubmission(wrongRubric.id)

        val criterion = correctRubric.categories.first().criteria.first()
        val wrongCriterion = wrongRubric.categories.first().criteria.first()

        val score = TestUtil.generateAddScoreRequest(
            criterionId = criterion.id,
            bucketId = wrongCriterion.buckets!!.first().id,
        )

        Given {
            contentType(ContentType.JSON)
            body(score)
        } When {
            put(addScoreEndpoint(assessment.id))
        } Then {
            statusCode(StatusCode.BAD_REQUEST)
        }
    }

    @Test
    fun `Creating a score with a wrong criterion source should fail`() {
        val correctRubric = generateAndAddCustomCriterionRubric(
            addCriterionRequest = TestUtil.generateAddCriterionRequest().copy(
                source = CriterionSource.BUCKET
            )
        )
        val wrongRubric = generateAndAddCustomCriterionRubric(
            addCriterionRequest = TestUtil.generateAddCriterionRequest().copy(
                source = CriterionSource.BUCKET
            )
        )

        val assessment = startGradingSubmission(correctRubric.id)
        val wrongAssessment = startGradingSubmission(wrongRubric.id)

        val criterion = correctRubric.categories.first().criteria.first()
        val wrongCriterion = wrongRubric.categories.first().criteria.first()

        val score = TestUtil.generateAddScoreRequest(
            criterionId = wrongCriterion.id,
            bucketId = wrongCriterion.buckets!!.first().id,
        )

        Given {
            contentType(ContentType.JSON)
            body(score)
        } When {
            put(addScoreEndpoint(assessment.id))
        } Then {
            statusCode(StatusCode.INTERNAL_SERVER_ERROR)
        }
    }

    @Test
    fun `Creating more than 1 score for the same criterion source should fail`() {
        val rubric = generateAndAddCustomCriterionRubric(
            addCriterionRequest = TestUtil.generateAddCriterionRequest().copy(
                source = CriterionSource.BUCKET
            )
        )
        val assessment = startGradingSubmission(rubric.id)

        val criterion = rubric.categories.first().criteria.first()

        val score = TestUtil.generateAddScoreRequest(
            criterionId = criterion.id,
            bucketId = criterion.buckets!!.first().id,
        )

        val score2 = TestUtil.generateAddScoreRequest(
            criterionId = criterion.id,
            bucketId = criterion.buckets.first().id,
        )

        Given {
            contentType(ContentType.JSON)
            body(score)
        } When {
            put(addScoreEndpoint(assessment.id))
        } Then {
            statusCode(StatusCode.CREATED)
        }

        Given {
            contentType(ContentType.JSON)
            body(score)
        } When {
            put(addScoreEndpoint(assessment.id))
        } Then {
            statusCode(StatusCode.BAD_REQUEST)
        }
    }


    @Test
    fun `Creating a score without a bucket for a bucket source should fail`() {
        val rubric = generateAndAddCustomCriterionRubric(
            addCriterionRequest = TestUtil.generateAddCriterionRequest().copy(
                source = CriterionSource.BUCKET
            )
        )

        val assessment = startGradingSubmission(rubric.id)

        val criterion = rubric.categories.first().criteria.first()
        val score = TestUtil.generateAddScoreRequest(
            criterionId = criterion.id,
        )

        Given {
            contentType(ContentType.JSON)
            body(score)
        } When {
            put(addScoreEndpoint(assessment.id))
        } Then {
            statusCode(StatusCode.INTERNAL_SERVER_ERROR)
        }
    }

    @Test
    fun `Creating a score not allowing overrides should fail for bucket source`() {
        val rubric = generateAndAddCustomCriterionRubric(
            addCriterionRequest = TestUtil.generateAddCriterionRequest().copy(
                source = CriterionSource.BUCKET
            )
        )
        val criterion = rubric.categories.first().criteria.first()
        val assessment = startGradingSubmission(rubric.id)
        val score = TestUtil.generateAddScoreRequest(
            criterionId = criterion.id,
            points = BigDecimal(100),
        )

        When {
            put(addScoreEndpoint(assessment.id))
        } Then {
            statusCode(StatusCode.INTERNAL_SERVER_ERROR) // check fails
        }
    }

    @Test
    fun `Creating a score allowing overrides should succeed for bucket source`() {
        val rubric = generateAndAddCustomCriterionRubric(
            addCriterionRequest = TestUtil.generateAddCriterionRequest().copy(
                source = CriterionSource.BUCKET,
                allowOverride = true
            )
        )
        val assessment = startGradingSubmission(rubric.id)
        val score = TestUtil.generateAddScoreRequest(
            criterionId = rubric.categories.first().criteria.first().id,
            points = BigDecimal(100),
            bucketId = rubric.categories.first().criteria.first().buckets!!.first().id,
        )

        val addScoreResponse = addScore(assessment.id, score)
        val bucketPoints = rubric.categories.first().criteria.first().buckets!!.first().points

        assertEquals(score.bucketId, addScoreResponse.bucketId)
        assertEquals(score.criterionId, addScoreResponse.criterionId)
        assertEquals(score.points, addScoreResponse.points)
    }

    @Test
    fun `Creating a score for a bucket source with no points should result in taking the buckets points`() {
        val rubric = generateAndAddCustomCriterionRubric(
            addCriterionRequest = TestUtil.generateAddCriterionRequest().copy(
                source = CriterionSource.BUCKET,
                allowOverride = true
            )
        )
        val assessment = startGradingSubmission(rubric.id)
        val score = TestUtil.generateAddScoreRequest(
            criterionId = rubric.categories.first().criteria.first().id,
            bucketId = rubric.categories.first().criteria.first().buckets!!.first().id,
        )

        val addScoreResponse = addScore(assessment.id, score)
        val bucketPoints = rubric.categories.first().criteria.first().buckets!!.first().points

        assertEquals(score.bucketId, addScoreResponse.bucketId)
        assertEquals(score.criterionId, addScoreResponse.criterionId)
        assertEquals(bucketPoints, addScoreResponse.points)
    }

    @Test
    fun `Creating a score with manual points should succeed`() {
        val rubric = generateAndAddCustomCriterionRubric(
            addCriterionRequest = TestUtil.generateAddCriterionRequest().copy(
                source = CriterionSource.MANUAL,
                points = BigDecimal(10)
            )
        )
        val assessment = startGradingSubmission(rubric.id)
        val score = TestUtil.generateAddScoreRequest(
            criterionId = rubric.categories.first().criteria.first().id,
            points = rubric.categories.first().criteria.first().points,
        )

        val addScoreResponse = addScore(assessment.id, score)

        // revision is added afterwards so it shouldn't be equal
        assertNotEquals(addScoreResponse.created, addScoreResponse.updated)
        assertBigDecimalEquals(addScoreResponse.points, score.points!!)
        assertEquals(score.bucketId, addScoreResponse.bucketId)
        assertEquals(score.criterionId, addScoreResponse.criterionId)
    }

    @Test
    fun `Creating a score with no points for a manual points source should fail`() {
        val rubric = generateAndAddCustomCriterionRubric(
            addCriterionRequest = TestUtil.generateAddCriterionRequest().copy(
                source = CriterionSource.MANUAL,
                points = BigDecimal(10)
            )
        )
        val assessment = startGradingSubmission(rubric.id)
        val score = TestUtil.generateAddScoreRequest(
            criterionId = rubric.categories.first().criteria.first().id,
        )

        Given {
            contentType(ContentType.JSON)
            body(score)
        } When {
            put(addScoreEndpoint(assessment.id))
        } Then {
            statusCode(StatusCode.INTERNAL_SERVER_ERROR) // check fails
        }
    }

    @Test
    fun `Creating a score with manual points more than possible should fail`() {
        val rubric = generateAndAddCustomCriterionRubric(
            addCriterionRequest = TestUtil.generateAddCriterionRequest().copy(
                source = CriterionSource.MANUAL,
                allowOverride = false,
                points = BigDecimal(100)
            )
        )
        val assessment = startGradingSubmission(rubric.id)
        val score = TestUtil.generateAddScoreRequest(
            criterionId = rubric.categories.first().criteria.first().id,
            points = BigDecimal(110),
        )

        val addScoreResponse = addScore(assessment.id, score)

        assertBigDecimalEquals(addScoreResponse.points, rubric.categories.first().criteria.first().points!!)
        assertEquals(score.bucketId, addScoreResponse.bucketId)
        assertEquals(score.criterionId, addScoreResponse.criterionId)
    }

    @Test
    fun `Creating a score not allowing overrides should be ignored for manual source`() {
        val rubric = generateAndAddCustomCriterionRubric(
            addCriterionRequest = TestUtil.generateAddCriterionRequest().copy(
                source = CriterionSource.MANUAL,
                points = BigDecimal(100)
            )
        )
        val assessment = startGradingSubmission(rubric.id)
        val score = TestUtil.generateAddScoreRequest(
            criterionId = rubric.categories.first().criteria.first().id,
            points = BigDecimal(1000),
        )

        val addScoreResponse = addScore(assessment.id, score)

        assertBigDecimalEquals(addScoreResponse.points, rubric.categories.first().criteria.first().points!!)
        assertEquals(score.bucketId, addScoreResponse.bucketId)
        assertEquals(score.criterionId, addScoreResponse.criterionId)
    }

    @Test
    fun `Creating a score not allowing overrides for a non maxed criterion should be not be ignored for manual source`() {
        val rubric = generateAndAddCustomCriterionRubric(
            addCriterionRequest = TestUtil.generateAddCriterionRequest().copy(
                source = CriterionSource.MANUAL,
            )
        )
        val criterion = rubric.categories.elementAt(0).criteria.elementAt(0)
        val assessment = startGradingSubmission(rubric.id)
        val score = TestUtil.generateAddScoreRequest(
            criterionId = rubric.categories.first().criteria.first().id,
            points = BigDecimal(1000),
        )

        val addScoreResponse = addScore(assessment.id, score)

        assertBigDecimalEquals(addScoreResponse.points, score.points!!)
        assertEquals(score.bucketId, addScoreResponse.bucketId)
        assertEquals(score.criterionId, addScoreResponse.criterionId)
    }

    @Test
    fun `Creating a score allowing overrides should succeed for manual source`() {
        val rubric = generateAndAddCustomCriterionRubric(
            addCriterionRequest = TestUtil.generateAddCriterionRequest().copy(
                source = CriterionSource.MANUAL,
                allowOverride = true,
                points = BigDecimal(100)
            )
        )
        val assessment = startGradingSubmission(rubric.id)
        val score = TestUtil.generateAddScoreRequest(
            criterionId = rubric.categories.first().criteria.first().id,
            points = BigDecimal(200),
            bucketId = rubric.categories.first().criteria.first().buckets!!.first().id,
        )

        val addScoreResponse = addScore(assessment.id, score)
        val bucketPoints = rubric.categories.first().criteria.first().buckets!!.first().points

        assertEquals(score.bucketId, addScoreResponse.bucketId)
        assertEquals(score.criterionId, addScoreResponse.criterionId)
        assertEquals(score.points, addScoreResponse.points)
    }

    @Test
    fun `Creating a score with constant points should succeed`() {
        val rubric = generateAndAddCustomCriterionRubric(
            addCriterionRequest = TestUtil.generateAddCriterionRequest().copy(
                source = CriterionSource.CONSTANT,
                points = BigDecimal(10)
            )
        )
        val assessment = startGradingSubmission(rubric.id)
        val score = TestUtil.generateAddScoreRequest(
            criterionId = rubric.categories.first().criteria.first().id,
            points = rubric.categories.first().criteria.first().points,
        )

        val addScoreResponse = addScore(assessment.id, score)

        // revision is added afterwards so it shouldn't be equal
        assertNotEquals(addScoreResponse.created, addScoreResponse.updated)
        assertBigDecimalEquals(addScoreResponse.points, score.points!!)
        assertEquals(score.bucketId, addScoreResponse.bucketId)
        assertEquals(score.criterionId, addScoreResponse.criterionId)
    }

    @Test
    fun `Creating a score not allowing overrides should be ignored for constant source`() {
        val rubric = generateAndAddCustomCriterionRubric(
            addCriterionRequest = TestUtil.generateAddCriterionRequest().copy(
                source = CriterionSource.CONSTANT,
                points = BigDecimal(100),
            )
        )
        val assessment = startGradingSubmission(rubric.id)
        val score = TestUtil.generateAddScoreRequest(
            criterionId = rubric.categories.first().criteria.first().id,
            points = BigDecimal(300),
        )

        val addScoreResponse = addScore(assessment.id, score)
        val criterion = rubric.categories.first().criteria.first()

        assertBigDecimalEquals(addScoreResponse.points, criterion.points!!)
        assertEquals(score.bucketId, addScoreResponse.bucketId)
        assertEquals(score.criterionId, addScoreResponse.criterionId)
    }

    @Test
    fun `Creating a score allowing overrides with should take the override points`() {
        val rubric = generateAndAddCustomCriterionRubric(
            addCriterionRequest = TestUtil.generateAddCriterionRequest().copy(
                source = CriterionSource.CONSTANT,
                points = BigDecimal(100),
                allowOverride = true,
            )
        )
        val assessment = startGradingSubmission(rubric.id)
        val score = TestUtil.generateAddScoreRequest(
            criterionId = rubric.categories.first().criteria.first().id,
            points = BigDecimal(200),
        )

        val addScoreResponse = addScore(assessment.id, score)


        assertEquals(score.bucketId, addScoreResponse.bucketId)
        assertBigDecimalEquals(score.points!!, addScoreResponse.points)
    }

    @Test
    fun `Creating a score allowing overrides with no points should result in taking the constant points`() {
        val rubric = generateAndAddCustomCriterionRubric(
            addCriterionRequest = TestUtil.generateAddCriterionRequest().copy(
                source = CriterionSource.CONSTANT,
                points = BigDecimal(100),
                allowOverride = true,
            )
        )
        val assessment = startGradingSubmission(rubric.id)
        val score = TestUtil.generateAddScoreRequest(
            criterionId = rubric.categories.first().criteria.first().id,
        )

        val addScoreResponse = addScore(assessment.id, score)
        val constantPoints = rubric.categories.first().criteria.first().points

        assertBigDecimalEquals(addScoreResponse.points, rubric.categories.first().criteria.first().points!!)
        assertEquals(score.bucketId, addScoreResponse.bucketId)
        assertEquals(constantPoints, addScoreResponse.points)
    }

    @Test
    fun `Creating an external score should succeed`() {
        val rubric = generateAndAddCustomCriterionRubric(
            addCriterionRequest = TestUtil.generateAddCriterionRequest().copy(
                source = CriterionSource.EXTERNAL,
                externalPoints = PointsSource(
                    externalSource = URI.create("https://yay.com"),
                    scalePointsTo = BigDecimal(10),
                )
            )
        )
        val criterion = rubric.categories.first().criteria.first()
        val assessment = startGradingSubmission(rubric.id)
        val score = TestUtil.generateAddScoreRequest(
            criterionId = criterion.id,
            points = criterion.points,
        )
        addScore(assessment.id, score)
    }

    /*
     * Helper functions for the resource tests
     */
    companion object {
        fun listAllAssessmentsEndpoint(): String {
            return UriBuilder
                .fromResource(AssessmentsResource::class.java)
                .path(AssessmentsResource::listAllAssessments.javaMethod)
                .build()
                .toString()
        }

        fun getAssessmentById(assessmentId: AssessmentId): AddAssessmentResponse {
            val assessment = When {
                get(getAssessmentByIdEndpoint(assessmentId))
            } Then {
                statusCode(StatusCode.OK)
            } Extract {
                body().`as`(AddAssessmentResponse::class.java)
            }
            return assessment
        }

        fun getAssessmentByIdEndpoint(assessmentId: AssessmentId): String {
            return UriBuilder
                .fromResource(AssessmentsResource::class.java)
                .path(AssessmentsResource::getAssessment.javaMethod)
                .build(assessmentId)
                .toString()
        }

        fun deleteAssessmentEndpoint(assessmentId: AssessmentId): String {
            return UriBuilder
                .fromResource(AssessmentsResource::class.java)
                .path(AssessmentsResource::deleteAssessment.javaMethod)
                .build(assessmentId)
                .toString()
        }

        fun addScore(assessmentId: AssessmentId, addScoreRequest: AddScoreRequest): AddScoreResponse {
            return Given {
                contentType(ContentType.JSON)
                body(addScoreRequest)
            } When {
                put(addScoreEndpoint(assessmentId))
            } Then {
                statusCode(StatusCode.CREATED)
            } Extract {
                body().`as`(AddScoreResponse::class.java)
            }
        }

        fun addScoreEndpoint(assessmentId: AssessmentId): String {
            return UriBuilder
                .fromResource(AssessmentsResource::class.java)
                .path(AssessmentsResource::scoresSubResource.javaMethod)

                .build(assessmentId)
                .toString()
        }

        fun generateAndAddCustomCriterionRubric(addCriterionRequest: AddCriterionRequest) : AddRubricResponse {
            val addRubricRequest = TestUtil.generateAddRubricRequest()
            val addCategoryRequest = TestUtil.generateAddCategoryRequest()

            val addBucketRequest = TestUtil.generateAddBucketRequest()

            val rubricId = RubricsResourceTest.addRubric(addRubricRequest)
            val categoryId = CategoriesResourceTest.addCategory(addCategoryRequest, rubricId)
            val addCriterionResponse = CriteriaResourceTest.addCriterion(addCriterionRequest, categoryId)
            val bucketId = BucketsResourceTest.addBucket(addBucketRequest, addCriterionResponse.id)
            return When {
                get(getRubricByIdEndpoint(rubricId))
            } Then {
                statusCode(StatusCode.OK)
            } Extract {
                body().`as`(AddRubricResponse::class.java)
            }
        }
    }
}
