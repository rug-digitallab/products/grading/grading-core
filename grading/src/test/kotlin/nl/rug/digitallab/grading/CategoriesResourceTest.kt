package nl.rug.digitallab.grading

import io.quarkus.test.junit.QuarkusTest
import io.restassured.http.ContentType
import io.restassured.module.kotlin.extensions.Extract
import io.restassured.module.kotlin.extensions.Given
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import jakarta.ws.rs.core.UriBuilder
import nl.rug.digitallab.grading.dtos.requests.AddCategoryRequest
import nl.rug.digitallab.grading.dtos.responses.AddCategoryResponse
import nl.rug.digitallab.grading.utils.TestUtil
import nl.rug.digitallab.grading.utils.TestUtil.doCompare
import org.jboss.resteasy.reactive.RestResponse.StatusCode
import org.junit.jupiter.api.Test
import kotlin.reflect.jvm.javaMethod

@QuarkusTest
class CategoriesResourceTest {

    @Test
    fun `Adding a new category should return the category ID`() {
        val rubricId = RubricsResourceTest.addRubric(TestUtil.generateAddRubricRequest())
        val addCategoryRequest = TestUtil.generateAddCategoryRequest()

        addCategory(addCategoryRequest, rubricId)
    }

    @Test
    fun `Adding an invalid category should return 400`() {
        val rubricId = RubricsResourceTest.addRubric(TestUtil.generateAddRubricRequest())
        val invalidCategoryRequest = TestUtil.generateInvalidCategory()

        Given {
            contentType(ContentType.JSON)
            body(invalidCategoryRequest)
        } When {
            post(addCategoryEndpoint(rubricId))
        } Then {
            statusCode(StatusCode.BAD_REQUEST)
        }
    }

    @Test
    fun `Getting a category by ID should return the correct category`() {
        val rubricId = RubricsResourceTest.addRubric(TestUtil.generateAddRubricRequest())
        val addCategoryRequest = TestUtil.generateAddCategoryRequest()
        val categoryId = addCategory(addCategoryRequest, rubricId)

        val addCategoryResponse =
            When {
                get(getCategoryByIdEndpoint(categoryId))
            } Then {
                statusCode(StatusCode.OK)
            } Extract {
                body().`as`(AddCategoryResponse::class.java)
            }

        addCategoryResponse.doCompare(addCategoryRequest)
    }

    @Test
    fun `Getting a non-existing category should return a 404`() {
        val nonExistentCategoryId = CategoryId.randomUUID()

        When {
            get(getCategoryByIdEndpoint(nonExistentCategoryId))
        } Then {
            statusCode(StatusCode.NOT_FOUND)
        }
    }

    @Test
    fun `Updating a category should return the updated category`() {
        val rubricId = RubricsResourceTest.addRubric(TestUtil.generateAddRubricRequest())
        val addCategoryRequest = TestUtil.generateAddCategoryRequest()
        val categoryId = addCategory(addCategoryRequest, rubricId)

        val updatedCategoryRequest = addCategoryRequest.copy(name = "Updated Name")

        val updatedAddCategoryResponse =
            Given {
                contentType(ContentType.JSON)
                body(updatedCategoryRequest)
            } When {
                put(updateCategoryEndpoint(categoryId))
            } Then {
                statusCode(StatusCode.OK)
            } Extract {
                body().`as`(AddCategoryResponse::class.java)
            }

        updatedAddCategoryResponse.doCompare(updatedCategoryRequest)
    }

    @Test
    fun `Updating a non-existing category should return a 404`() {
        val nonExistentCategoryId = CategoryId.randomUUID()
        val updateCategoryRequest = TestUtil.generateAddCategoryRequest()

        Given {
            contentType(ContentType.JSON)
            body(updateCategoryRequest)
        } When {
            put(updateCategoryEndpoint(nonExistentCategoryId))
        } Then {
            statusCode(StatusCode.NOT_FOUND)
        }
    }

    @Test
    fun `Deleting a category should return 204`() {
        val rubricId = RubricsResourceTest.addRubric(TestUtil.generateAddRubricRequest())
        val addCategoryRequest = TestUtil.generateAddCategoryRequest()
        val categoryId = addCategory(addCategoryRequest, rubricId)

        When {
            delete(deleteCategoryEndpoint(categoryId))
        } Then {
            statusCode(StatusCode.NO_CONTENT)
        }
    }

    @Test
    fun `Deleting a non-existing category should return 404`() {
        val nonExistentCategoryId = CategoryId.randomUUID()

        When {
            delete(deleteCategoryEndpoint(nonExistentCategoryId))
        } Then {
            statusCode(StatusCode.NOT_FOUND)
        }
    }

    /*
     * Helper functions for the resource tests
     */

    companion object {
        fun addCategory(addCategoryRequest: AddCategoryRequest, rubricId: RubricId): CategoryId {
            val response =
                Given {
                    contentType(ContentType.JSON)
                    body(addCategoryRequest)
                } When {
                    post(addCategoryEndpoint(rubricId))
                } Then {
                    statusCode(StatusCode.CREATED)
                } Extract {
                    body().`as`(AddCategoryResponse::class.java)
                }
            return response.id
        }

        fun addCategoryEndpoint(rubricId: RubricId): String {
            return UriBuilder
                .fromResource(RubricsResource::class.java)
                .path(RubricsResource::categoriesSubResource.javaMethod)
                .path(RubricsResource.CategoriesSubResource::createCategory.javaMethod)
                .build(rubricId)
                .toString()
        }

        fun getCategoryByIdEndpoint(categoryId: CategoryId): String {
            return UriBuilder
                .fromResource(CategoriesResource::class.java)
                .path(CategoriesResource::getCategory.javaMethod)
                .build(categoryId)
                .toString()
        }

        fun updateCategoryEndpoint(categoryId: CategoryId): String {
            return UriBuilder
                .fromResource(CategoriesResource::class.java)
                .path(CategoriesResource::updateCategory.javaMethod)
                .build(categoryId)
                .toString()
        }

        fun deleteCategoryEndpoint(categoryId: CategoryId): String {
            return UriBuilder
                .fromResource(CategoriesResource::class.java)
                .path(CategoriesResource::deleteCategory.javaMethod)
                .build(categoryId)
                .toString()
        }
    }
}
