package nl.rug.digitallab.grading.utils

import nl.rug.digitallab.grading.AssignmentId
import nl.rug.digitallab.grading.enums.CriterionSource
import nl.rug.digitallab.grading.persistence.entities.*
import nl.rug.digitallab.grading.utils.TestUtil.assertBigDecimalEquals
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import java.math.BigDecimal

object ReposTestUtil {
    fun generateRandomRubricEntity(): Rubric {
        return Rubric(
            assignmentId = AssignmentId.randomUUID(),
            name = "Random Rubric Name",
            description = "Random Rubric Description",
            scalePointsTo = BigDecimal(100)
        )
    }

    fun generateRandomCategoryEntity(rubric: Rubric): Category {
        val category = Category(
            rubric = rubric,
            name = "Random Category Name",
            description = "Random Category Description",
            scalePointsTo = BigDecimal(10),
        )
        return category
    }

    fun generateRandomCriterionEntity(category: Category): Criterion {
        val criterion = Criterion(
            category = category,
            name = "Random Criterion Name",
            description = "Random Criterion Description",
            allowOverride = false,
            source = CriterionSource.BUCKET,
        )
        return criterion
    }

    fun generateRandomBucketEntity(criterion: Criterion): Bucket {
        return Bucket(
            criterion = criterion,
            name = "Random Bucket Name",
            description = "Random Bucket Description",
            points = BigDecimal(5)
        )
    }

    fun generateRandomAssessmentEntity(rubric: Rubric) : Assessment {
        return Assessment(
            templateRubric = rubric
        )
    }

    fun Rubric.doCompare(foundRubric: Rubric) {
        assertEquals(id, foundRubric.id)
        assertEquals(name, foundRubric.name)
        assertEquals(description, foundRubric.description)
        assertBigDecimalEquals(scalePointsTo, foundRubric.scalePointsTo)
        assertEquals(created, foundRubric.created)
        assertEquals(updated, foundRubric.updated)
    }

    fun Category.doCompare(category: Category) {
        assertEquals(id, category.id)
        assertEquals(name, category.name)
        assertEquals(description, category.description)
        assertBigDecimalEquals(scalePointsTo, category.scalePointsTo)
        assertEquals(created, category.created)
        assertEquals(updated, category.updated)
        criteria.forEachIndexed { index, criterion ->
            criterion.doCompare(category.criteria.elementAt(index))
            assertEquals(criterion.category, this)
        }
    }

    fun Criterion.doCompare(criterion: Criterion) {
        assertEquals(id, criterion.id)
        assertEquals(name, criterion.name)
        assertEquals(description, criterion.description)
        assertEquals(allowOverride, criterion.allowOverride)

        externalPoints?.run {
            assertEquals(externalPoints!!.externalSource, criterion.externalPoints!!.externalSource)
            assertBigDecimalEquals(externalPoints!!.scalePointsTo, criterion.externalPoints!!.scalePointsTo)
        }

        assertEquals(created, criterion.created)
        assertEquals(updated, criterion.updated)
        buckets?.forEachIndexed { index, bucket ->
            bucket.doCompare(criterion.buckets!!.elementAt(index))
            assertEquals(bucket.criterion, this)
        }
    }

    fun Bucket.doCompare(bucket: Bucket) {
        assertEquals(id, bucket.id)
        assertEquals(name, bucket.name)
        assertEquals(description, bucket.description)
        assertBigDecimalEquals(points, bucket.points)
        assertEquals(created, bucket.created)
        assertEquals(updated, bucket.updated)
    }

    fun Score.doCompare(score: Score) {
        assertEquals(id, score.id)
        assertEquals(criterion.id, score.criterion.id)
        assertEquals(assessment.id, score.assessment.id)
        revisions.forEachIndexed { index, scoreRevision ->
            scoreRevision.doCompare(score.revisions.elementAt(index))
        }
        latestRevision.doCompare(score.latestRevision)
        assertEquals(revisionCount, score.revisionCount)
        assertEquals(created, score.created)
        assertEquals(updated, score.updated)
    }

    fun ScoreRevision.doCompare(scoreRevision: ScoreRevision) {
        assertEquals(id, scoreRevision.id)
        assertEquals(score.id, scoreRevision.score.id)
        assertEquals(grader, scoreRevision.grader)
        assertEquals(created, scoreRevision.created)
        assertEquals(bucket?.id, scoreRevision.bucket?.id)
        if (points != null && scoreRevision != null) {
            assertBigDecimalEquals(points!!, scoreRevision.points!!)
        }
    }

    fun Assessment.doCompare(assessment: Assessment) {
        assertEquals(id, assessment.id)
        assertEquals(templateRubric.id, assessment.templateRubric.id)
        templateRubric.doCompare(assessment.templateRubric)
        assertNotNull(assessment.grade!!)
        assertEquals(created, assessment.created)
        assertEquals(updated, assessment.updated)
        scores.forEachIndexed { index, score ->
            score.doCompare(assessment.scores.elementAt(index))
            assertEquals(score.assessment, this)
        }
    }
}
