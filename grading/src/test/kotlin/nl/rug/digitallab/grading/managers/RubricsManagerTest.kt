package nl.rug.digitallab.grading.managers

import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import jakarta.inject.Inject
import nl.rug.digitallab.grading.AssignmentId
import nl.rug.digitallab.grading.RubricId
import nl.rug.digitallab.grading.dtos.responses.AddRubricResponse
import nl.rug.digitallab.grading.exceptions.AssignmentNotFoundException
import nl.rug.digitallab.grading.exceptions.RubricNotFoundException
import nl.rug.digitallab.grading.utils.TestUtil
import nl.rug.digitallab.grading.utils.TestUtil.doCompare
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

@QuarkusTest
class RubricsManagerTest {
    @Inject
    lateinit var rubricsManager: RubricsManager

    @Test
    @RunOnVertxContext
    fun `Getting a rubric by ID should return the correct rubric`(asserter: UniAsserter) {
        val addRubricRequest = TestUtil.generateAddRubricRequest()
        val assignmentId = AssignmentId.randomUUID()

        asserter
            .execute<AddRubricResponse> {
                rubricsManager.addRubric(assignmentId, addRubricRequest)
                    .invoke { rubric -> asserter.putData("rubricId", rubric.id) }
            }
            .assertThat({
                val rubricId = asserter.getData("rubricId") as RubricId
                rubricsManager.getRubric(rubricId)
            }) { rubric ->
                rubric.doCompare(addRubricRequest)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Getting a rubric by assignmentId should return the correct rubric`(asserter: UniAsserter) {
        val addRubricRequest = TestUtil.generateAddRubricRequest()
        val assignmentId = AssignmentId.randomUUID()

        asserter
            .execute<AddRubricResponse> {
                rubricsManager.addRubric(assignmentId, addRubricRequest)
            }
            .assertThat({
                rubricsManager.getRubricByAssignment(assignmentId)
            }) { rubric ->
                rubric.doCompare(addRubricRequest)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Getting a non-existing rubric by ID should throw an exception`(asserter: UniAsserter) {
        val nonExistingRubricId = RubricId.randomUUID()

        asserter
            .assertFailedWith({ rubricsManager.getRubric(nonExistingRubricId) }) { e ->
                assertEquals(RubricNotFoundException::class.java, e.javaClass)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Getting a non-existing rubric by non-existing assignmentId should throw an exception`(asserter: UniAsserter) {
        val nonExistingAssignmentId = AssignmentId.randomUUID()

        asserter
            .assertFailedWith({ rubricsManager.getRubricByAssignment(nonExistingAssignmentId) }) { e ->
                assertEquals(AssignmentNotFoundException::class.java, e.javaClass)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Updating a rubric should return the updated rubric`(asserter: UniAsserter) {
        val assignmentId = AssignmentId.randomUUID()
        val addRubricRequest = TestUtil.generateAddRubricRequest()
        val expectedUpdatedRubric = addRubricRequest.copy(name = "Hello")

        asserter
            .execute<AddRubricResponse> {
                rubricsManager.addRubric(assignmentId, addRubricRequest)
                    .invoke { rubric -> asserter.putData("rubricId", rubric.id) }
            }
            .assertThat({
                val rubricId = asserter.getData("rubricId") as RubricId
                rubricsManager.updateRubric(rubricId, expectedUpdatedRubric)
            }) { updatedRubric ->
                updatedRubric.doCompare(expectedUpdatedRubric)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Deleting a non-existing rubric should throw an exception`(asserter: UniAsserter) {
        val nonExistingRubricId = RubricId.randomUUID()

        asserter
            .assertFailedWith({ rubricsManager.deleteRubric(nonExistingRubricId) }) { e ->
                assertEquals(RubricNotFoundException::class.java, e.javaClass)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Deleting a rubric should delete the rubric`(asserter: UniAsserter) {
        val addRubricRequest = TestUtil.generateAddRubricRequest()
        val assignmentId = AssignmentId.randomUUID()

        asserter
            .execute<AddRubricResponse> {
                rubricsManager.addRubric(assignmentId, addRubricRequest)
                    .invoke { rubric -> asserter.putData("rubricId", rubric.id) }
            }
            .execute<Unit> {
                val rubricId = asserter.getData("rubricId") as RubricId
                rubricsManager.deleteRubric(rubricId)
            }
            .assertFailedWith({
                val rubricId = asserter.getData("rubricId") as RubricId
                rubricsManager.getRubric(rubricId)
            }) { e ->
                assertEquals(RubricNotFoundException::class.java, e.javaClass)
            }
    }

}
