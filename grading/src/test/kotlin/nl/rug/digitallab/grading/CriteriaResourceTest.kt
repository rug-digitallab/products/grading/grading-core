package nl.rug.digitallab.grading

import io.quarkus.test.junit.QuarkusTest
import io.restassured.http.ContentType
import io.restassured.module.kotlin.extensions.Extract
import io.restassured.module.kotlin.extensions.Given
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import jakarta.ws.rs.core.UriBuilder
import nl.rug.digitallab.grading.dtos.requests.AddCriterionRequest
import nl.rug.digitallab.grading.dtos.responses.AddCriterionResponse
import nl.rug.digitallab.grading.utils.TestUtil
import nl.rug.digitallab.grading.utils.TestUtil.assertBigDecimalEquals
import org.jboss.resteasy.reactive.RestResponse.StatusCode
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import kotlin.reflect.jvm.javaMethod

@QuarkusTest
class CriteriaResourceTest {

    @Test
    fun `Adding a new criterion should return the criterion ID`() {
        val rubricId = RubricsResourceTest.addRubric(TestUtil.generateAddRubricRequest())
        val categoryId = CategoriesResourceTest.addCategory(TestUtil.generateAddCategoryRequest(), rubricId)
        val addCriterionRequest = TestUtil.generateAddCriterionRequest()
        val addCriterionResponse = addCriterion(addCriterionRequest, categoryId)

        assertEquals(addCriterionResponse.source.code, addCriterionRequest.source.code)
    }

    @Test
    fun `Adding an invalid criterion should return 400`() {
        val rubricId = RubricsResourceTest.addRubric(TestUtil.generateAddRubricRequest())
        val categoryId = CategoriesResourceTest.addCategory(TestUtil.generateAddCategoryRequest(), rubricId)
        val invalidCriterionRequest = TestUtil.generateInvalidCriterion()

        Given {
            contentType(ContentType.JSON)
            body(invalidCriterionRequest)
        } When {
            post(addCriterionEndpoint(categoryId))
        } Then {
            statusCode(StatusCode.BAD_REQUEST)
        }
    }

    @Test
    fun `Getting a criterion by ID should return the correct criterion`() {
        val rubricId = RubricsResourceTest.addRubric(TestUtil.generateAddRubricRequest())
        val categoryId = CategoriesResourceTest.addCategory(TestUtil.generateAddCategoryRequest(), rubricId)
        val addCriterionRequest = TestUtil.generateAddCriterionRequest()
        val criterion = addCriterion(addCriterionRequest, categoryId)

        val addCriterionResponse =
            When {
                get(getCriterionByIdEndpoint(criterion.id))
            } Then {
                statusCode(StatusCode.OK)
            } Extract {
                body().`as`(AddCriterionResponse::class.java)
            }

        addCriterionResponse.doCompare(addCriterionRequest)
    }

    @Test
    fun `Getting a non-existing criterion should return a 404`() {
        val nonExistentCriterionId = CriterionId.randomUUID()

        When {
            get(getCriterionByIdEndpoint(nonExistentCriterionId))
        } Then {
            statusCode(StatusCode.NOT_FOUND)
        }
    }

    @Test
    fun `Updating a criterion should return the updated criterion`() {
        val rubricId = RubricsResourceTest.addRubric(TestUtil.generateAddRubricRequest())
        val categoryId = CategoriesResourceTest.addCategory(TestUtil.generateAddCategoryRequest(), rubricId)
        val addCriterionRequest = TestUtil.generateAddCriterionRequest()
        val criterion = addCriterion(addCriterionRequest, categoryId)

        val updatedCriterionRequest = addCriterionRequest.copy(name = "Updated Name")

        val updatedAddCriterionResponse =
            Given {
                contentType(ContentType.JSON)
                body(updatedCriterionRequest)
            } When {
                put(updateCriterionEndpoint(criterion.id))
            } Then {
                statusCode(StatusCode.OK)
            } Extract {
                body().`as`(AddCriterionResponse::class.java)
            }

        updatedAddCriterionResponse.doCompare(updatedCriterionRequest)
    }

    @Test
    fun `Updating a non-existing criterion should return a 404`() {
        val nonExistentCriterionId = CriterionId.randomUUID()
        val updateCriterionRequest = TestUtil.generateAddCriterionRequest()

        Given {
            contentType(ContentType.JSON)
            body(updateCriterionRequest)
        } When {
            put(updateCriterionEndpoint(nonExistentCriterionId))
        } Then {
            statusCode(StatusCode.NOT_FOUND)
        }
    }

    @Test
    fun `Deleting a criterion should return 204`() {
        val rubricId = RubricsResourceTest.addRubric(TestUtil.generateAddRubricRequest())
        val categoryId = CategoriesResourceTest.addCategory(TestUtil.generateAddCategoryRequest(), rubricId)
        val addCriterionRequest = TestUtil.generateAddCriterionRequest()
        val criterion = addCriterion(addCriterionRequest, categoryId)

        When {
            delete(deleteCriterionEndpoint(criterion.id))
        } Then {
            statusCode(StatusCode.NO_CONTENT)
        }
    }

    @Test
    fun `Deleting a non-existing criterion should return 404`() {
        val nonExistentCriterionId = CriterionId.randomUUID()

        When {
            delete(deleteCriterionEndpoint(nonExistentCriterionId))
        } Then {
            statusCode(StatusCode.NOT_FOUND)
        }
    }

    /*
     * Helper functions for the resource tests
     */

    companion object {
        fun addCriterion(addCriterionRequest: AddCriterionRequest, categoryId: CategoryId): AddCriterionResponse {
            val response =
                Given {
                    contentType(ContentType.JSON)
                    body(addCriterionRequest)
                } When {
                    post(addCriterionEndpoint(categoryId))
                } Then {
                    statusCode(StatusCode.CREATED)
                } Extract {
                    body().`as`(AddCriterionResponse::class.java)
                }
            return response
        }

        fun addCriterionEndpoint(categoryId: CategoryId): String {
            return UriBuilder
                .fromResource(CategoriesResource::class.java)
                .path(CategoriesResource::criteriaSubResource.javaMethod)
                .path(CategoriesResource.CriteriaSubResource::createCriterion.javaMethod)
                .build(categoryId)
                .toString()
        }

        fun getCriterionByIdEndpoint(criterionId: CriterionId): String {
            return UriBuilder
                .fromResource(CriteriaResource::class.java)
                .path(CriteriaResource::getCriterion.javaMethod)
                .build(criterionId)
                .toString()
        }

        fun updateCriterionEndpoint(criterionId: CriterionId): String {
            return UriBuilder
                .fromResource(CriteriaResource::class.java)
                .path(CriteriaResource::updateCriterion.javaMethod)
                .build(criterionId)
                .toString()
        }

        fun deleteCriterionEndpoint(criterionId: CriterionId): String {
            return UriBuilder
                .fromResource(CriteriaResource::class.java)
                .path(CriteriaResource::deleteCriterion.javaMethod)
                .build(criterionId)
                .toString()
        }

        fun AddCriterionResponse.doCompare(expected: AddCriterionRequest) {
            assertEquals(name, expected.name)
            assertEquals(description, expected.description)
            assertEquals(source.code, expected.source.code)
            externalPoints?.run {
                assertEquals(externalPoints!!.externalSource, expected.externalPoints!!.externalSource)
                assertBigDecimalEquals(externalPoints!!.scalePointsTo, expected.externalPoints!!.scalePointsTo)
            }
        }
    }
}
