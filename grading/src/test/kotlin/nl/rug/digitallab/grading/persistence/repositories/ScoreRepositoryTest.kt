package nl.rug.digitallab.grading.persistence.repositories

import io.quarkus.test.hibernate.reactive.panache.TransactionalUniAsserter
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import jakarta.inject.Inject
import nl.rug.digitallab.grading.ScoreId
import nl.rug.digitallab.grading.exceptions.ScoreNotFoundException
import nl.rug.digitallab.grading.persistence.entities.Assessment
import nl.rug.digitallab.grading.persistence.entities.Score
import nl.rug.digitallab.grading.persistence.entities.ScoreRevision
import nl.rug.digitallab.grading.utils.ReposTestUtil
import nl.rug.digitallab.grading.utils.ReposTestUtil.doCompare
import org.hibernate.HibernateException
import org.hibernate.reactive.mutiny.Mutiny
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.util.UUID

@QuarkusTest
class ScoreRepositoryTest {
    @Inject
    lateinit var rubricRepository: RubricRepository

    @Inject
    lateinit var categoryRepository: CategoryRepository

    @Inject
    lateinit var criterionRepository: CriterionRepository

    @Inject
    lateinit var bucketRepository: BucketRepository

    @Inject
    lateinit var assessmentRepository: AssessmentRepository

    @Inject
    lateinit var scoreRepository: ScoreRepository


    @Test
    @RunOnVertxContext
    fun `Finding a score by ID should return the correct score`(asserter: TransactionalUniAsserter) {
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)
        val criterion = ReposTestUtil.generateRandomCriterionEntity(category)
        val bucket = ReposTestUtil.generateRandomBucketEntity(criterion)
        val assessment = Assessment(
            templateRubric = rubric
        )
        val score = Score(
            criterion = criterion,
            assessment = assessment,
        )
        val scoreRevision = ScoreRevision(
            grader = UUID.randomUUID(),
            score = score,
            bucket = bucket,
        )
        score.addRevision(scoreRevision)

        val createUni =
            rubricRepository.create(rubric)
                .flatMap {
                    categoryRepository.create(category)
                }.flatMap {
                    criterionRepository.create(criterion)
                }.flatMap {
                    bucketRepository.create(bucket)
                }.flatMap { 
                    assessmentRepository.create(assessment)
                }.flatMap {
                    assessmentRepository.findByIdOrThrow(assessment.id)
                }.flatMap { assessment ->
                    score.assessment = assessment
                    criterionRepository.findByIdOrThrow(criterion.id)
                }.flatMap { criterion ->
                    score.criterion = criterion
                    scoreRepository.create(score)
                }

        asserter
            .execute<Score> {
                createUni
            }
            .assertThat({
                scoreRepository.findByIdOrThrow(score.id).call { found -> Mutiny.fetch(found.revisions) }
            }) { foundScore ->
                score.doCompare(foundScore)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Adding a score with a non-associated bucket should fail`(asserter: TransactionalUniAsserter) {
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)
        val criterion = ReposTestUtil.generateRandomCriterionEntity(category)
        val rightBucket = ReposTestUtil.generateRandomBucketEntity(criterion)

        val wrongRubric = ReposTestUtil.generateRandomRubricEntity()
        val wrongCategory = ReposTestUtil.generateRandomCategoryEntity(wrongRubric)
        val wrongCriterion = ReposTestUtil.generateRandomCriterionEntity(wrongCategory)
        val wrongBucket = ReposTestUtil.generateRandomBucketEntity(wrongCriterion)

        val assessment = Assessment(
            templateRubric = rubric
        )
        val score = Score(
            criterion = criterion,
            assessment = assessment,
        )
        val scoreRevision = ScoreRevision(
            grader = UUID.randomUUID(),
            score = score,
            bucket = wrongBucket,
        )
        score.addRevision(scoreRevision)

        val createUni =
            rubricRepository.create(rubric).flatMap { rubricRepository.create(wrongRubric) }
                .flatMap {
                    categoryRepository.create(category).flatMap {
                        categoryRepository.create(wrongCategory)
                    }
                }.flatMap {
                    criterionRepository.create(criterion).flatMap {
                        criterionRepository.create(wrongCriterion)
                    }
                }.flatMap {
                    bucketRepository.create(rightBucket).flatMap {
                        bucketRepository.create(wrongBucket)
                    }
                }.flatMap {
                    assessmentRepository.create(assessment)
                }.flatMap {
                    assessmentRepository.findByIdOrThrow(assessment.id)
                }.flatMap { assessment ->
                    score.assessment = assessment
                    criterionRepository.findByIdOrThrow(criterion.id)
                }.flatMap { criterion ->
                    score.criterion = criterion
                    scoreRepository.create(score)
                }

        asserter
            .assertFailedWith({
                createUni
            }) { e ->
                assertEquals(HibernateException::class.java, e.javaClass)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Deleting a score by ID should remove it from the repository`(asserter: TransactionalUniAsserter) {
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)
        val criterion = ReposTestUtil.generateRandomCriterionEntity(category)
        val bucket = ReposTestUtil.generateRandomBucketEntity(criterion)
        val assessment = Assessment(
            templateRubric = rubric
        )
        val score = Score(
            criterion = criterion,
            assessment = assessment,
        )
        val scoreRevision = ScoreRevision(
            grader = UUID.randomUUID(),
            score = score,
            bucket = bucket,
        )
        score.addRevision(scoreRevision)

        val createUni =
            rubricRepository.create(rubric)
                .flatMap {
                    categoryRepository.create(category)
                }.flatMap {
                    criterionRepository.create(criterion)
                }.flatMap {
                    bucketRepository.create(bucket)
                }.flatMap {
                    assessmentRepository.create(assessment)
                }.flatMap {
                    assessmentRepository.findByIdOrThrow(assessment.id)
                }.flatMap { assessment ->
                    score.assessment = assessment
                    criterionRepository.findByIdOrThrow(criterion.id)
                }.flatMap { criterion ->
                    score.criterion = criterion
                    scoreRepository.create(score)
                }

        asserter
            .execute<Score> {
                createUni
            }.execute<Unit> {
                scoreRepository.deleteByIdOrThrow(score.id)
            }
            .assertFailedWith({
                scoreRepository.findByIdOrThrow(score.id)
            }) { e ->
                assertEquals(ScoreNotFoundException::class.java, e.javaClass)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Fetching a non-existent score ID should throw ScoreNotFoundException`(asserter: TransactionalUniAsserter) {
        val nonExistentScoreId = ScoreId.randomUUID()

        asserter
            .assertFailedWith({
                scoreRepository.findByIdOrThrow(nonExistentScoreId)
            }) { e ->
                assertEquals(ScoreNotFoundException::class.java, e.javaClass)
            }
    }


    @Test
    @RunOnVertxContext
    fun `Updating a score should persist changes to the repository`(asserter: TransactionalUniAsserter) {
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)
        val criterion = ReposTestUtil.generateRandomCriterionEntity(category).also {
            it.allowOverride = true
        }
        val bucket = ReposTestUtil.generateRandomBucketEntity(criterion)
        val assessment = Assessment(
            templateRubric = rubric
        )
        val score = Score(
            criterion = criterion,
            assessment = assessment,
        )
        val scoreRevision = ScoreRevision(
            grader = UUID.randomUUID(),
            score = score,
            bucket = bucket,
        )
        score.addRevision(scoreRevision)


        val createUni =
            rubricRepository.create(rubric)
                .flatMap {
                    categoryRepository.create(category)
                }.flatMap {
                    criterionRepository.create(criterion)
                }.flatMap {
                    bucketRepository.create(bucket)
                }.flatMap {
                    assessmentRepository.create(assessment)
                }.flatMap {
                    assessmentRepository.findByIdOrThrow(assessment.id)
                }.flatMap { assessment ->
                    score.assessment = assessment
                    criterionRepository.findByIdOrThrow(criterion.id)
                }.flatMap { criterion ->
                    score.criterion = criterion
                    scoreRepository.create(score)
                }

        asserter
            .execute<Score> {
                createUni
            }.execute<Score> {
                scoreRepository.findByIdOrThrow(score.id)
                    .call { found -> Mutiny.fetch(found.revisions) }
                    .call { found -> Mutiny.fetch(found.assessment) }
                    .flatMap { newScoreEntity ->
                        val scoreRevision2 = ScoreRevision(
                            grader = UUID.randomUUID(),
                            score = score,
                            bucket = bucket,
                            points = BigDecimal(1000)
                        )
                        score.addRevision(scoreRevision2)
                        newScoreEntity.addRevision(scoreRevision2)
                        scoreRepository.create(newScoreEntity).invoke { persisted ->
                            asserter.putData("scoreRevision2", scoreRevision2)
                        }
                    }
            }.assertThat({
                scoreRepository.findByIdOrThrow(score.id).call { found -> Mutiny.fetch(found.revisions) }
            }) { updatedScore ->
                val scoreRevision2 = asserter.getData("scoreRevision2") as ScoreRevision
                assertEquals(updatedScore.revisionCount, 2)
                updatedScore.latestRevision.doCompare(scoreRevision2)
                //!! order of retrieval is not at all guaranteed to be the same as the insertion
                // so we need to sort by the created col
                updatedScore.revisions.minBy(ScoreRevision::created).doCompare(scoreRevision)
                assertEquals(updatedScore.revisions.size, 2)
                assertNotEquals(updatedScore.updated, updatedScore.created)
            }
    }

}
