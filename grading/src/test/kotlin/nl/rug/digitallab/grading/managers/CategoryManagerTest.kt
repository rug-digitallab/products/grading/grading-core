package nl.rug.digitallab.grading.managers

import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import jakarta.inject.Inject
import nl.rug.digitallab.grading.CategoryId
import nl.rug.digitallab.grading.RubricId
import nl.rug.digitallab.grading.dtos.responses.AddCategoryResponse
import nl.rug.digitallab.grading.exceptions.CategoryNotFoundException
import nl.rug.digitallab.grading.persistence.entities.Rubric
import nl.rug.digitallab.grading.persistence.repositories.RubricRepository
import nl.rug.digitallab.grading.utils.ReposTestUtil
import nl.rug.digitallab.grading.utils.TestUtil
import nl.rug.digitallab.grading.utils.TestUtil.doCompare
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.math.BigDecimal

@QuarkusTest
class CategoryManagerTest {

    @Inject
    lateinit var categoriesManager: CategoriesManager

    @Inject
    lateinit var rubricRepository: RubricRepository


    @Test
    @RunOnVertxContext
    fun `Adding a category should return the correct category`(asserter: UniAsserter) {
        val addCategoryRequest = TestUtil.generateAddCategoryRequest()

        val rubric = ReposTestUtil.generateRandomRubricEntity()
        asserter
            .execute<Rubric> {
                rubricRepository.create(rubric)
                .invoke { rubric -> asserter.putData("rubricId", rubric.id) }
            }
            .assertThat({
                categoriesManager.addCategory(
                    asserter.getData("rubricId") as RubricId,
                    addCategoryRequest,
                )
            }) { categoryResponse ->
                categoryResponse.doCompare(addCategoryRequest)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Getting a category by ID should return the correct category`(asserter: UniAsserter) {
        val addCategoryRequest = TestUtil.generateAddCategoryRequest()
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        asserter
            .execute<Rubric> {
                rubricRepository.create(rubric)
                    .invoke { rubric -> asserter.putData("rubricId", rubric.id) }
            }
            .execute<AddCategoryResponse> {
                categoriesManager.addCategory(
                    asserter.getData("rubricId") as RubricId,
                    addCategoryRequest,
                ).invoke { category -> asserter.putData("categoryId", category.id) }
            }
            .assertThat({
                categoriesManager.getCategory(asserter.getData("categoryId") as CategoryId)
            }) { categoryResponse ->
                categoryResponse.doCompare(addCategoryRequest)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Updating a category should return the updated category`(asserter: UniAsserter) {
        val addCategoryRequest = TestUtil.generateAddCategoryRequest()
        val updatedCategoryRequest = addCategoryRequest.copy(
            name = "Updated Category Name",
            description = "Updated Category Description",
            scalePointsTo = BigDecimal(50)
        )
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        asserter
            .execute<Rubric> {
                rubricRepository.create(rubric)
                    .invoke { rubric -> asserter.putData("rubricId", rubric.id) }
            }
            .execute<AddCategoryResponse> {
                categoriesManager.addCategory(
                    asserter.getData("rubricId") as RubricId,
                    addCategoryRequest,
                ).invoke { category -> asserter.putData("categoryId", category.id) }
            }
            .assertThat({
                categoriesManager.updateCategory(
                    asserter.getData("categoryId") as CategoryId,
                    updatedCategoryRequest,
                )
            }) { categoryResponse ->
                categoryResponse.doCompare(updatedCategoryRequest)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Deleting a category should remove the category`(asserter: UniAsserter) {
        val addCategoryRequest = TestUtil.generateAddCategoryRequest()

        val rubric = ReposTestUtil.generateRandomRubricEntity()
        asserter
            .execute<Rubric> {
                rubricRepository.create(rubric)
                    .invoke { rubric -> asserter.putData("rubricId", rubric.id) }
            }
            .execute<AddCategoryResponse> {
                categoriesManager.addCategory(
                    asserter.getData("rubricId") as RubricId,
                    addCategoryRequest,
                ).invoke { category -> asserter.putData("categoryId", category.id) }
            }
            .execute<Unit> {
                categoriesManager.deleteCategory(
                    asserter.getData("categoryId") as CategoryId,
                )
            }
            .assertFailedWith({
                categoriesManager.getCategory(asserter.getData("categoryId") as CategoryId)
            }) { e ->
                assertEquals(CategoryNotFoundException::class.java, e.javaClass)
            }
    }
}
