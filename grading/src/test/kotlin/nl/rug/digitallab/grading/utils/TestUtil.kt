package nl.rug.digitallab.grading.utils

import nl.rug.digitallab.grading.BucketId
import nl.rug.digitallab.grading.CriterionId
import nl.rug.digitallab.grading.dtos.requests.*
import nl.rug.digitallab.grading.dtos.responses.*
import nl.rug.digitallab.grading.enums.CriterionSource
import org.junit.jupiter.api.Assertions.assertEquals
import java.math.BigDecimal

object TestUtil {
    fun generateInvalidRubric(): InvalidRubric {
        val rubric = generateAddRubricRequest()
        val invalidRubric = InvalidRubric(
            description = rubric.description,
            scalePointsTo = rubric.scalePointsTo,
        )
        return invalidRubric
    }

    data class InvalidRubric(
        val description: String,
        val scalePointsTo: BigDecimal,
    )

    fun generateAddRubricRequest(): AddRubricRequest {
        return AddRubricRequest(
            name = "Rubric",
            description = "Description",
            scalePointsTo = BigDecimal(10.00),
        )
    }

    fun generateAddCategoryRequest(): AddCategoryRequest {
        return AddCategoryRequest(
            name = "Category",
            description = "Description",
            scalePointsTo = BigDecimal(10.00),
        )
    }

    fun generateInvalidCategory(): InvalidCategory {
        val category = generateAddCategoryRequest()
        return InvalidCategory(
            description = category.description,
            scalePointsTo = category.scalePointsTo,
        )
    }

    data class InvalidCategory(
        val description: String,
        val scalePointsTo: BigDecimal,
    )

    fun generateAddCriterionRequest(): AddCriterionRequest {
        return AddCriterionRequest(
            name = "Criterion",
            description = "Description",
            allowOverride = false,
            source = CriterionSource.BUCKET,
        )
    }

    fun generateInvalidCriterion(): InvalidCriterion {
        val criterion = generateAddCriterionRequest()
        return InvalidCriterion(
            description = criterion.description,
            manualPoints = criterion.points,
        )
    }

    fun generateScoreRequest(
        criterionId: CriterionId,
        bucketId: BucketId,
    ): AddScoreRequest {
        return AddScoreRequest(
            criterionId = criterionId,
            bucketId = bucketId
        )
    }

    fun generateUpdateScoreRequest(
        bucketId: BucketId,
    ): UpdateScoreRequest {
        return UpdateScoreRequest(
            bucketId = bucketId
        )
    }

    data class InvalidCriterion(
        val description: String,
        val manualPoints: BigDecimal? = null,
    )

    fun generateAddBucketRequest(): AddBucketRequest {
        return AddBucketRequest(
            name = "Bucket",
            description = "Description",
            points = BigDecimal(10.00),
        )
    }

    fun generateInvalidBucket(): InvalidBucket {
        val bucket = generateAddBucketRequest()
        return InvalidBucket(
            description = bucket.description,
            points = bucket.points,
        )
    }

    data class InvalidBucket(
        val description: String,
        val points: BigDecimal,
    )

    // Comparison method for BucketResponse and AddBucketRequest
    fun AddBucketResponse.doCompare(other: AddBucketRequest) {
        assertEquals(other.name, name)
        assertEquals(other.description, description)
        assertBigDecimalEquals(other.points, points)
    }

    fun AddScoreResponse.doCompare(other: AddScoreRequest) {
        assertEquals(criterionId, other.criterionId)
        assertEquals(points, other.points)
        assertEquals(bucketId, other.bucketId)
    }

    /**
     * We do this here, so we don't have to compare to 0 every time we compare two BigDecimals
     **/
    fun assertBigDecimalEquals(expected: BigDecimal, actual: BigDecimal) {
        assertEquals(0, expected.compareTo(actual)) {
            "Expected $expected but was $actual"
        }
    }

    fun AddRubricResponse.doCompare(expected: AddRubricRequest) {
        assertEquals(name, expected.name)
        assertEquals(description, expected.description)
        assertBigDecimalEquals(scalePointsTo, expected.scalePointsTo)
    }

    fun AddCategoryResponse.doCompare(other: AddCategoryRequest) {
        assertEquals(name, other.name)
        assertEquals(description, other.description)
        assertBigDecimalEquals(scalePointsTo, other.scalePointsTo)
    }

    fun AddCriterionResponse.doCompare(other: AddCriterionRequest) {
        assertEquals(name, other.name)
        assertEquals(description, other.description)
        assertEquals(allowOverride, other.allowOverride)

        externalPoints?.run {
            assertEquals(externalPoints!!.externalSource, other.externalPoints!!.externalSource)
            assertBigDecimalEquals(externalPoints!!.scalePointsTo, other.externalPoints!!.scalePointsTo)
        }
    }

    fun generateAddScoreRequest(
        criterionId: CriterionId,
        points: BigDecimal? = null,
        bucketId: BucketId? = null,
    ): AddScoreRequest {
        return AddScoreRequest(
            criterionId = criterionId,
            points = points,
            bucketId = bucketId,
        )
    }

    fun generateUpdateScoreRequest(
        points: BigDecimal? = null,
        bucketId: BucketId? = null,
    ): UpdateScoreRequest {
        return UpdateScoreRequest(
            points = points,
            bucketId = bucketId,
        )
    }
}
