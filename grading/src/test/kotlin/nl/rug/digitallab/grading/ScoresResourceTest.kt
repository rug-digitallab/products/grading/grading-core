package nl.rug.digitallab.grading

import io.quarkus.test.junit.QuarkusTest
import io.restassured.http.ContentType
import io.restassured.module.kotlin.extensions.Extract
import io.restassured.module.kotlin.extensions.Given
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import jakarta.ws.rs.core.UriBuilder
import nl.rug.digitallab.grading.AssessmentsResourceTest.Companion.addScore
import nl.rug.digitallab.grading.AssessmentsResourceTest.Companion.addScoreEndpoint
import nl.rug.digitallab.grading.AssessmentsResourceTest.Companion.generateAndAddCustomCriterionRubric
import nl.rug.digitallab.grading.RubricsResourceTest.Companion.startGradingSubmission
import nl.rug.digitallab.grading.dtos.requests.UpdateScoreRequest
import nl.rug.digitallab.grading.dtos.responses.AddScoreResponse
import nl.rug.digitallab.grading.enums.CriterionSource
import nl.rug.digitallab.grading.utils.TestUtil
import nl.rug.digitallab.grading.utils.TestUtil.assertBigDecimalEquals
import org.jboss.resteasy.reactive.RestResponse.StatusCode
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import kotlin.reflect.jvm.javaMethod


@QuarkusTest
class ScoresResourceTest {
    @Test
    fun `Getting a score should return a 200 response code`() {
        val addScoreResponse = generateAndAddScore()
        val scoreId = addScoreResponse.id

        val getScoreResponse = getScore(scoreId)
        assertEquals(getScoreResponse, addScoreResponse)

        val assessment = AssessmentsResourceTest.getAssessmentById(addScoreResponse.assessmentId)
        assertBigDecimalEquals(assessment.grade!!, addScoreResponse.points)
    }

    @Test
    fun `Getting a non existing score should return a 404 response code`() {
        val randomId = ScoreId.randomUUID()
        When {
            get(getScoreEndpoint(randomId))
        } Then {
            statusCode(StatusCode.NOT_FOUND)
        }
    }

    @Test
    fun `Adding a score with a bucket that doesnt belong to the rubric should throw an error`() {
        val rubric = generateAndAddCustomCriterionRubric(
            addCriterionRequest = TestUtil.generateAddCriterionRequest().copy(
                source = CriterionSource.BUCKET
            )
        )
        val assessment = startGradingSubmission(rubric.id)
        val criterion = rubric.categories.first().criteria.first()
        val score = TestUtil.generateAddScoreRequest(
            criterionId = criterion.id,
            bucketId = BucketId.randomUUID(),
        )

        Given {
            contentType(ContentType.JSON)
            body(score)
        } When {
            put(addScoreEndpoint(assessment.id))
        } Then {
            statusCode(StatusCode.BAD_REQUEST)
        }
    }

    @Test
    fun `Deleting a score should return a 200 response code`() {
        val addScoreResponse = generateAndAddScore()
        val scoreId = addScoreResponse.id

        When {
            get(deleteScoreEndpoint(scoreId))
        } Then {
            statusCode(StatusCode.OK)
        }
    }

    @Test
    fun `Getting a deleted score should return a 404 response code`() {
        val addScoreResponse = generateAndAddScore()
        val scoreId = addScoreResponse.id

        When {
            delete(deleteScoreEndpoint(scoreId))
        } Then {
            statusCode(StatusCode.OK)
        }

        When {
            get(getScoreEndpoint(scoreId))
        } Then {
            statusCode(StatusCode.NOT_FOUND)
        }
    }

    @Test
    fun `Updating a non existing score should return a 404 response code`() {
        val randomId = ScoreId.randomUUID()
        val updateScoreRequest = TestUtil.generateUpdateScoreRequest(
            points = BigDecimal(100),
        )

        Given {
            contentType(ContentType.JSON)
            body(updateScoreRequest)
        } When {
            put(updateScoreEndpoint(randomId))
        } Then {
            statusCode(StatusCode.NOT_FOUND)
        }
    }

    @Test
    fun `Updating a score with a bucket that doesnt belong to the rubric should throw an error`() {
        val rubric = generateAndAddCustomCriterionRubric(
            addCriterionRequest = TestUtil.generateAddCriterionRequest().copy(
                source = CriterionSource.BUCKET
            )
        )
        val assessment = startGradingSubmission(rubric.id)
        val criterion = rubric.categories.first().criteria.first()
        val score = TestUtil.generateAddScoreRequest(
            criterionId = criterion.id,
            bucketId = criterion.buckets!!.first().id
        )

        val addedScore = addScore(assessment.id, score)

        val updateScoreRequest = addedScore.copy(
            bucketId = BucketId.randomUUID(),
        )

        Given {
            contentType(ContentType.JSON)
            body(updateScoreRequest)
        } When {
            put(updateScoreEndpoint(addedScore.id))
        } Then {
            statusCode(StatusCode.BAD_REQUEST)
        }
    }

    @Test
    fun `Updating a score should return the new bucket points`() {
        val rubric = generateAndAddCustomCriterionRubric(
            addCriterionRequest = TestUtil.generateAddCriterionRequest().copy(
                source = CriterionSource.BUCKET
            )
        )
        val criterion = rubric.categories.first().criteria.first()
        val firstBucket = criterion.buckets!!.first()
        val assessment = startGradingSubmission(rubric.id)
        val score = TestUtil.generateAddScoreRequest(
            criterionId = criterion.id,
            bucketId = criterion.buckets.first().id,
        )
        val addScoreResponse = addScore(assessment.id, score)

        assertNotEquals(addScoreResponse.updated, addScoreResponse.created)
        assertEquals(addScoreResponse.bucketId, firstBucket.id)
        assertBigDecimalEquals(addScoreResponse.points, firstBucket.points)

        val fetchedAssessment = AssessmentsResourceTest.getAssessmentById(addScoreResponse.assessmentId)
        assertBigDecimalEquals(fetchedAssessment.grade!!, addScoreResponse.points)

        val updatedBucket = TestUtil.generateAddBucketRequest().copy(
            name = "Updated bucket",
            points = BigDecimal(99),
        )

        val bucketId = BucketsResourceTest.addBucket(updatedBucket, criterion.id)

        val updateScoreRequest = TestUtil.generateUpdateScoreRequest(
            bucketId = bucketId,
        )

        val updateScoreResponse = updateScore(addScoreResponse.id, updateScoreRequest)

        assertEquals(bucketId, updateScoreResponse.bucketId)
        assertBigDecimalEquals(updatedBucket.points, updateScoreResponse.points)

        val fetchedAssessment2 = AssessmentsResourceTest.getAssessmentById(updateScoreResponse.assessmentId)
        assertBigDecimalEquals(fetchedAssessment2.grade!!, updateScoreResponse.points)
    }

    @Test
    fun `Updating a score should return the new manual points`() {
        val rubric = generateAndAddCustomCriterionRubric(
            addCriterionRequest = TestUtil.generateAddCriterionRequest().copy(
                source = CriterionSource.MANUAL,
            )
        )
        val criterion = rubric.categories.first().criteria.first()

        val assessment = startGradingSubmission(rubric.id)
        val score = TestUtil.generateAddScoreRequest(
            criterionId = criterion.id,
            points = BigDecimal(100),
        )
        val addScoreResponse = addScore(assessment.id, score)

        assertBigDecimalEquals(addScoreResponse.points, score.points!!)

        val updatedScore = TestUtil.generateUpdateScoreRequest(
            points = BigDecimal(200),
        )
        val updatedScoreResponse = updateScore(addScoreResponse.id, updatedScore)

        assertBigDecimalEquals(updatedScoreResponse.points, updatedScore.points!!)

        val getScoreResponse = getScore(addScoreResponse.id)
        assertBigDecimalEquals(getScoreResponse.points, updatedScoreResponse.points)
    }

    companion object {
        fun getScoreEndpoint(scoreId: ScoreId): String {
            return UriBuilder
                .fromResource(ScoresResource::class.java)
                .path(ScoresResource::getScore.javaMethod)
                .build(scoreId)
                .toString()
        }

        fun updateScoreEndpoint(scoreId: ScoreId): String {
            return UriBuilder
                .fromResource(ScoresResource::class.java)
                .path(ScoresResource::updateScore.javaMethod)
                .build(scoreId)
                .toString()
        }

        fun deleteScoreEndpoint(scoreId: ScoreId): String {
            return UriBuilder
                .fromResource(ScoresResource::class.java)
                .path(ScoresResource::deleteScore.javaMethod)
                .build(scoreId)
                .toString()
        }

        fun getScore(scoreId: ScoreId) : AddScoreResponse {
            return When {
                get(getScoreEndpoint(scoreId))
            } Then {
                statusCode(StatusCode.OK)
            } Extract {
                body().`as`(AddScoreResponse::class.java)
            }
        }

        fun updateScore(scoreId: ScoreId, updateScoreRequest: UpdateScoreRequest) : AddScoreResponse {
            return Given {
                contentType(ContentType.JSON)
                body(updateScoreRequest)
            } When {
                put(updateScoreEndpoint(scoreId))
            } Then {
                statusCode(StatusCode.OK)
            } Extract {
                body().`as`(AddScoreResponse::class.java)
            }
        }

        fun generateAndAddScore() : AddScoreResponse {
            val rubric = generateAndAddCustomCriterionRubric(
                addCriterionRequest = TestUtil.generateAddCriterionRequest().copy(
                    source = CriterionSource.BUCKET
                )
            )
            val assessment = startGradingSubmission(rubric.id)
            val criterion = rubric.categories.first().criteria.first()
            val score = TestUtil.generateAddScoreRequest(
                criterionId = criterion.id,
                bucketId = criterion.buckets!!.first().id,
            )
            return addScore(assessment.id, score)
        }
    }
}
