package nl.rug.digitallab.grading.persistence.repositories

import io.quarkus.test.hibernate.reactive.panache.TransactionalUniAsserter
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import jakarta.inject.Inject
import nl.rug.digitallab.grading.BucketId
import nl.rug.digitallab.grading.exceptions.BucketNotFoundException
import nl.rug.digitallab.grading.persistence.entities.Bucket
import nl.rug.digitallab.grading.utils.ReposTestUtil
import nl.rug.digitallab.grading.utils.ReposTestUtil.doCompare
import nl.rug.digitallab.grading.utils.TestUtil
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.math.BigDecimal

@QuarkusTest
class BucketRepositoryTest {
    @Inject
    lateinit var rubricRepository: RubricRepository

    @Inject
    lateinit var categoryRepository: CategoryRepository

    @Inject
    lateinit var criterionRepository: CriterionRepository

    @Inject
    lateinit var bucketRepository: BucketRepository

    @Test
    @RunOnVertxContext
    fun `Finding a bucket by ID should return the correct bucket`(asserter: TransactionalUniAsserter) {
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)
        val criterion = ReposTestUtil.generateRandomCriterionEntity(category)
        val bucket = ReposTestUtil.generateRandomBucketEntity(criterion)

        val createUni =
            rubricRepository.create(rubric)
                .flatMap { rubric ->
                    categoryRepository.create(category)
                }.flatMap { category ->
                    criterionRepository.create(criterion)
                }.flatMap { criterion ->
                    bucketRepository.create(bucket)
                }

        asserter
            .execute<Bucket> {
                createUni
            }
            .assertThat({
                bucketRepository.findByIdOrThrow(bucket.id)
            }) { foundBucket ->
                bucket.doCompare(foundBucket)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Finding a bucket by ID should throw an exception if the bucket does not exist`(asserter: TransactionalUniAsserter) {
        val bucketId = BucketId.randomUUID()
        asserter
            .assertFailedWith({
                bucketRepository.findByIdOrThrow(bucketId) { BucketNotFoundException(bucketId) }
            }) { e ->
                assertEquals(BucketNotFoundException::class.java, e.javaClass)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Deleting a bucket by ID should remove the bucket`(asserter: TransactionalUniAsserter) {
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)
        val criterion = ReposTestUtil.generateRandomCriterionEntity(category)
        val bucket = ReposTestUtil.generateRandomBucketEntity(criterion)

        val createUni =
            rubricRepository.create(rubric)
                .flatMap { rubric ->
                    categoryRepository.create(category)
                }.flatMap { category ->
                    criterionRepository.create(criterion)
                }.flatMap { criterion ->
                    bucketRepository.create(bucket)
                }

        asserter
            .execute<Bucket> {
                createUni
            }
            .execute<Unit> { bucketRepository.deleteWithOrThrow(bucket.id) }
            .assertFailedWith({
                bucketRepository.findByIdOrThrow(bucket.id)
            }) { e ->
                assertEquals(BucketNotFoundException::class.java, e.javaClass)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Updating a bucket by ID should modify and return the updated bucket`(asserter: TransactionalUniAsserter) {
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)
        val criterion = ReposTestUtil.generateRandomCriterionEntity(category)
        val bucket = ReposTestUtil.generateRandomBucketEntity(criterion)

        val createUni =
            rubricRepository.create(rubric)
                .flatMap { _ ->
                    categoryRepository.create(category)
                }.flatMap { _ ->
                    criterionRepository.create(criterion)
                }.flatMap { _ ->
                    bucketRepository.create(bucket)
                }

        asserter
            .execute<Bucket> {
                createUni
            }
            .execute<Bucket> {
                bucketRepository.updateWithOrThrow(bucket.id) {
                    it.name = "Updated Name"
                    it.description = "Updated Description"
                    it.points = it.points.add(BigDecimal.TEN)
                    it
                }
            }
            .assertThat({
                bucketRepository.findByIdOrThrow(bucket.id)
            }) { updatedBucket ->
                assertEquals("Updated Name", updatedBucket.name)
                assertEquals("Updated Description", updatedBucket.description)
                TestUtil.assertBigDecimalEquals(bucket.points.add(BigDecimal.TEN), updatedBucket.points)
            }
    }
}
