package nl.rug.digitallab.grading.managers

import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import jakarta.inject.Inject
import nl.rug.digitallab.grading.ScoreId
import nl.rug.digitallab.grading.dtos.responses.AddScoreResponse
import nl.rug.digitallab.grading.exceptions.ScoreNotFoundException
import nl.rug.digitallab.grading.persistence.entities.Assessment
import nl.rug.digitallab.grading.persistence.entities.Bucket
import nl.rug.digitallab.grading.persistence.repositories.*
import nl.rug.digitallab.grading.utils.ReposTestUtil
import nl.rug.digitallab.grading.utils.TestUtil
import nl.rug.digitallab.grading.utils.TestUtil.assertBigDecimalEquals
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.math.BigDecimal

@QuarkusTest
class ScoreManagerTest {
    @Inject
    lateinit var scoreManager: ScoreManager

    @Inject
    lateinit var rubricRepository: RubricRepository

    @Inject
    lateinit var categoryRepository: CategoryRepository

    @Inject
    lateinit var bucketRepository: BucketRepository

    @Inject
    lateinit var criterionRepository: CriterionRepository

    @Inject
    lateinit var assessmentRepository: AssessmentRepository

    @Test
    @RunOnVertxContext
    fun `Adding a score should return the correct score`(asserter: UniAsserter) {
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)
        val criterion = ReposTestUtil.generateRandomCriterionEntity(category)
        val bucket = ReposTestUtil.generateRandomBucketEntity(criterion)
        val assessment = ReposTestUtil.generateRandomAssessmentEntity(rubric)

        val createUni =
            rubricRepository.create(rubric)
                .flatMap {
                    categoryRepository.create(category)
                }.flatMap {
                    criterionRepository.create(criterion)
                }.flatMap {
                    bucketRepository.create(bucket)
                }.flatMap {
                    assessmentRepository.create(assessment)
                }

        asserter
            .execute<Assessment> { createUni }
            .assertThat({
                val scoreRequest = TestUtil.generateScoreRequest(
                    criterionId =  criterion.id,
                    bucketId = bucket.id,
                )
                scoreManager.addScore(
                    assessmentId = assessment.id,
                    addScoreRequest = scoreRequest,
                ).invoke { score ->
                    asserter.putData("scoreId", score.id)
                }
            }) { scoreResponse ->
                assertBigDecimalEquals(bucket.points, scoreResponse.points)
                assertEquals(bucket.id, scoreResponse.bucketId)
                assertEquals(asserter.getData("scoreId") as ScoreId, scoreResponse.id)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Finding a score by ID should return the correct score`(asserter: UniAsserter) {
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)
        val criterion = ReposTestUtil.generateRandomCriterionEntity(category)
        val bucket = ReposTestUtil.generateRandomBucketEntity(criterion)
        val assessment = ReposTestUtil.generateRandomAssessmentEntity(rubric)

        val createUni =
            rubricRepository.create(rubric)
                .flatMap {
                    categoryRepository.create(category)
                }.flatMap {
                    criterionRepository.create(criterion)
                }.flatMap {
                    bucketRepository.create(bucket)
                }.flatMap {
                    assessmentRepository.create(assessment)
                }

        asserter
            .execute<Assessment> { createUni }
            .execute<AddScoreResponse> {
                val scoreRequest = TestUtil.generateScoreRequest(
                    criterion.id,
                    bucket.id,
                )
                scoreManager.addScore(
                    assessment.id,
                    scoreRequest,
                ).invoke { score ->
                    asserter.putData("scoreId", score.id)
                }
            }
            .assertThat({
                scoreManager.findScore(asserter.getData("scoreId") as ScoreId)
            }) { scoreResponse ->
                assertBigDecimalEquals(bucket.points, scoreResponse.points)
                assertEquals(bucket.id, scoreResponse.bucketId)
                assertEquals(asserter.getData("scoreId") as ScoreId, scoreResponse.id)
            }
    }


    @Test
    @RunOnVertxContext
    fun `Updating a score should return the updated score`(asserter: UniAsserter) {
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)
        val criterion = ReposTestUtil.generateRandomCriterionEntity(category)
        val bucket = ReposTestUtil.generateRandomBucketEntity(criterion)
        val secondBucket = Bucket(criterion, "name", "desc", BigDecimal(90))
        val assessment = ReposTestUtil.generateRandomAssessmentEntity(rubric)

        val createUni =
            rubricRepository.create(rubric)
                .flatMap {
                    categoryRepository.create(category)
                }.flatMap {
                    criterionRepository.create(criterion)
                }.flatMap {
                    bucketRepository.create(bucket)
                }.flatMap {
                    bucketRepository.create(secondBucket)
                }.flatMap {
                    assessmentRepository.create(assessment)
                }

        asserter
            .execute<Assessment> { createUni }
            .execute<AddScoreResponse> {
                val scoreRequest = TestUtil.generateScoreRequest(
                    criterion.id,
                    bucket.id,
                )
                scoreManager.addScore(
                    assessment.id,
                    scoreRequest,
                ).invoke { score ->
                    asserter.putData("scoreId", score.id)
                }
            }
            .assertThat({
                val updatedScoreRequest = TestUtil.generateUpdateScoreRequest(
                    secondBucket.id,
                )
                scoreManager.updateScore(
                    asserter.getData("scoreId") as ScoreId,
                    updatedScoreRequest
                ).invoke { updatedScore ->
                    asserter.putData("updatedScoreId", updatedScore.id)
                }
            }) { updatedScoreResponse ->
                assertBigDecimalEquals(secondBucket.points, updatedScoreResponse.points)
                assertEquals(secondBucket.id, updatedScoreResponse.bucketId)
                assertEquals(asserter.getData("scoreId") as ScoreId, updatedScoreResponse.id)
                assertEquals(asserter.getData("updatedScoreId") as ScoreId, updatedScoreResponse.id)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Deleting a score should remove the score`(asserter: UniAsserter) {
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)
        val criterion = ReposTestUtil.generateRandomCriterionEntity(category)
        val bucket = ReposTestUtil.generateRandomBucketEntity(criterion)
        val assessment = ReposTestUtil.generateRandomAssessmentEntity(rubric)

        val createUni =
            rubricRepository.create(rubric)
                .flatMap {
                    categoryRepository.create(category)
                }.flatMap {
                    criterionRepository.create(criterion)
                }.flatMap {
                    bucketRepository.create(bucket)
                }.flatMap {
                    assessmentRepository.create(assessment)
                }

        asserter
            .execute<Assessment> { createUni }
            .execute<AddScoreResponse> {
                val scoreRequest = TestUtil.generateScoreRequest(criterion.id, bucket.id)
                scoreManager.addScore(
                    assessment.id,
                    scoreRequest
                ).invoke { score ->
                    asserter.putData("scoreId", score.id)
                }
            }
            .execute<Unit> {
                scoreManager.deleteScore(asserter.getData("scoreId") as ScoreId)
            }
            .assertFailedWith({
                scoreManager.findScore(asserter.getData("scoreId") as ScoreId)
            }) { e ->
                assertEquals(ScoreNotFoundException::class.java, e.javaClass)
            }
    }

}
