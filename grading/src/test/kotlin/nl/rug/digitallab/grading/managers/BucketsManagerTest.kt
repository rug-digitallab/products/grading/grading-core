package nl.rug.digitallab.grading.managers

import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import jakarta.inject.Inject
import nl.rug.digitallab.grading.BucketId
import nl.rug.digitallab.grading.CriterionId
import nl.rug.digitallab.grading.dtos.responses.AddBucketResponse
import nl.rug.digitallab.grading.exceptions.BucketNotFoundException
import nl.rug.digitallab.grading.persistence.entities.Category
import nl.rug.digitallab.grading.persistence.entities.Criterion
import nl.rug.digitallab.grading.persistence.entities.Rubric
import nl.rug.digitallab.grading.persistence.repositories.CategoryRepository
import nl.rug.digitallab.grading.persistence.repositories.CriterionRepository
import nl.rug.digitallab.grading.persistence.repositories.RubricRepository
import nl.rug.digitallab.grading.utils.ReposTestUtil
import nl.rug.digitallab.grading.utils.TestUtil
import nl.rug.digitallab.grading.utils.TestUtil.doCompare
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.math.BigDecimal

@QuarkusTest
class BucketsManagerTest {

    @Inject
    lateinit var bucketsManager: BucketsManager

    @Inject
    lateinit var rubricRepository: RubricRepository

    @Inject
    lateinit var categoryRepository: CategoryRepository

    @Inject
    lateinit var criterionRepository: CriterionRepository


    @Test
    @RunOnVertxContext
    fun `Adding a bucket should return the correct bucket`(asserter: UniAsserter) {
        val addBucketRequest = TestUtil.generateAddBucketRequest()
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)
        val criterion = ReposTestUtil.generateRandomCriterionEntity(category)

        asserter
            .execute<Rubric> { rubricRepository.create(rubric) }
            .execute<Category> { categoryRepository.create(category) }
            .execute<Criterion> { criterionRepository.create(criterion).invoke{ criterion ->
                asserter.putData("criterionId", criterion.id)
            }}
            .assertThat({
                bucketsManager.addBucket(
                    asserter.getData("criterionId") as CriterionId,
                    addBucketRequest,
                )
            }) { bucketResponse ->
                bucketResponse.doCompare(addBucketRequest)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Getting a bucket by ID should return the correct bucket`(asserter: UniAsserter) {
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)
        val criterion = ReposTestUtil.generateRandomCriterionEntity(category)
        val addBucketRequest =  TestUtil.generateAddBucketRequest()

        asserter
            .execute<Rubric> { rubricRepository.create(rubric) }
            .execute<Category> { categoryRepository.create(category) }
            .execute<Criterion> { criterionRepository.create(criterion).invoke{ criterion ->
                asserter.putData("criterionId", criterion.id)
            }}
            .execute<AddBucketResponse> {
                bucketsManager.addBucket(
                    asserter.getData("criterionId") as CriterionId,
                    addBucketRequest,
                ).invoke { bucket -> asserter.putData("bucketId", bucket.id) }
            }
            .assertThat({
                bucketsManager.getBucket(asserter.getData("bucketId") as BucketId)
            }) { bucketResponse ->
                bucketResponse.doCompare(addBucketRequest)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Updating a bucket should return the updated bucket`(asserter: UniAsserter) {
        val addBucketRequest =  TestUtil.generateAddBucketRequest()
        val updatedBucketRequest = addBucketRequest.copy(
            name = "Updated Bucket Name",
            description = "Updated Bucket Description",
            points = BigDecimal(50)
        )
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)
        val criterion = ReposTestUtil.generateRandomCriterionEntity(category)

        asserter
            .execute<Rubric> { rubricRepository.create(rubric) }
            .execute<Category> { categoryRepository.create(category) }
            .execute<Criterion> { criterionRepository.create(criterion).invoke{ criterion ->
                asserter.putData("criterionId", criterion.id)
            }}
            .execute<AddBucketResponse> {
                bucketsManager.addBucket(
                    asserter.getData("criterionId") as CriterionId,
                    addBucketRequest,
                ).invoke { bucket -> asserter.putData("bucketId", bucket.id) }
            }
            .assertThat({
                bucketsManager.updateBucket(
                    asserter.getData("bucketId") as BucketId,
                    updatedBucketRequest,
                )
            }) { bucketResponse ->
                bucketResponse.doCompare(updatedBucketRequest)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Deleting a bucket should remove the bucket`(asserter: UniAsserter) {
        val addBucketRequest =  TestUtil.generateAddBucketRequest()
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)
        val criterion = ReposTestUtil.generateRandomCriterionEntity(category)

        asserter
            .execute<Rubric> { rubricRepository.create(rubric) }
            .execute<Category> { categoryRepository.create(category) }
            .execute<Criterion> { criterionRepository.create(criterion).invoke{ criterion ->
                asserter.putData("criterionId", criterion.id)
            }}
            .execute<AddBucketResponse> {
                bucketsManager.addBucket(
                    asserter.getData("criterionId") as CriterionId,
                    addBucketRequest,
                ).invoke { bucket -> asserter.putData("bucketId", bucket.id) }
            }
            .execute<Unit> {
                bucketsManager.deleteBucket(
                    asserter.getData("bucketId") as BucketId,
                )
            }
            .assertFailedWith({
                bucketsManager.getBucket(asserter.getData("bucketId") as BucketId)
            }) { e ->
                assertEquals(BucketNotFoundException::class.java, e.javaClass)
            }
    }
}
