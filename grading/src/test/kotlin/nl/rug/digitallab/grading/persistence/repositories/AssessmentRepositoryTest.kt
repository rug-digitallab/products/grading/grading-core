package nl.rug.digitallab.grading.persistence.repositories

import io.quarkus.test.hibernate.reactive.panache.TransactionalUniAsserter
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import jakarta.inject.Inject
import nl.rug.digitallab.grading.AssessmentId
import nl.rug.digitallab.grading.exceptions.AssessmentNotFoundException
import nl.rug.digitallab.grading.persistence.entities.Assessment
import nl.rug.digitallab.grading.utils.ReposTestUtil
import nl.rug.digitallab.grading.utils.ReposTestUtil.doCompare
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

@QuarkusTest
class AssessmentRepositoryTest {
    @Inject
    lateinit var rubricRepository: RubricRepository

    @Inject
    lateinit var assessmentRepository: AssessmentRepository

    @Test
    @RunOnVertxContext
    fun `Finding an assessment by ID should return the correct assessment`(asserter: TransactionalUniAsserter) {
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val assessment = Assessment(rubric)

        val createUni = rubricRepository.create(rubric)
            .flatMap { rubric ->
                assessmentRepository.create(assessment)
            }

        asserter
            .execute<Assessment> {
                createUni
            }
            .assertThat({
                assessmentRepository.findByIdOrThrow(assessment.id)
            }) { foundAssessment ->
                assessment.doCompare(foundAssessment)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Finding an assessment by ID should throw an exception if the assessment does not exist`(asserter: TransactionalUniAsserter) {
        val assessmentId = AssessmentId.randomUUID()
        asserter
            .assertFailedWith({
                assessmentRepository.findByIdOrThrow(assessmentId)
            }) { e ->
                assertEquals(AssessmentNotFoundException::class.java, e.javaClass)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Deleting an assessment by ID should remove the assessment`(asserter: TransactionalUniAsserter) {
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val assessment = Assessment(rubric)

        val createUni = rubricRepository.create(rubric)
            .flatMap { rubric ->
                assessmentRepository.create(assessment)
            }

        asserter
            .execute<Assessment> {
                createUni
            }
            .execute<Unit> { assessmentRepository.deleteByIdOrThrow(assessment.id) }
            .assertFailedWith({
                assessmentRepository.findByIdOrThrow(assessment.id)
            }) { e ->
                assertEquals(AssessmentNotFoundException::class.java, e.javaClass)
            }
    }
}
