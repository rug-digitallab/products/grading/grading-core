package nl.rug.digitallab.grading.managers

import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.quarkus.test.vertx.UniAsserter
import jakarta.inject.Inject
import nl.rug.digitallab.grading.CategoryId
import nl.rug.digitallab.grading.CriterionId
import nl.rug.digitallab.grading.dtos.responses.AddCriterionResponse
import nl.rug.digitallab.grading.exceptions.CriterionNotFoundException
import nl.rug.digitallab.grading.persistence.entities.Category
import nl.rug.digitallab.grading.persistence.entities.Rubric
import nl.rug.digitallab.grading.persistence.repositories.CategoryRepository
import nl.rug.digitallab.grading.persistence.repositories.RubricRepository
import nl.rug.digitallab.grading.utils.ReposTestUtil
import nl.rug.digitallab.grading.utils.TestUtil
import nl.rug.digitallab.grading.utils.TestUtil.doCompare
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

@QuarkusTest
class CriteriaManagerTest {

    @Inject
    lateinit var criteriaManager: CriteriaManager

    @Inject
    lateinit var rubricRepository: RubricRepository

    @Inject
    lateinit var categoryRepository: CategoryRepository

    @Test
    @RunOnVertxContext
    fun `Adding a criterion should return the correct criterion`(asserter: UniAsserter) {
        val addCriterionRequest = TestUtil.generateAddCriterionRequest()

        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)

        asserter
            .execute<Rubric> {
                rubricRepository.create(rubric)
                    .invoke { rubric -> asserter.putData("rubricId", rubric.id) }
            }
            .execute<Category> {
                categoryRepository.create(category)
                    .invoke { category -> asserter.putData("categoryId", category.id) }
            }
            .assertThat({
                criteriaManager.addCriterion(
                    asserter.getData("categoryId") as CategoryId,
                    addCriterionRequest,
                )
            }) { criterionResponse ->
                criterionResponse.doCompare(addCriterionRequest)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Getting a criterion by ID should return the correct criterion`(asserter: UniAsserter) {
        val addCriterionRequest = TestUtil.generateAddCriterionRequest()

        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)

        asserter
            .execute<Rubric> {
                rubricRepository.create(rubric)
                    .invoke { rubric -> asserter.putData("rubricId", rubric.id) }
            }
            .execute<Category> {
                categoryRepository.create(category)
                    .invoke { category -> asserter.putData("categoryId", category.id) }
            }
            .execute<AddCriterionResponse> {
                criteriaManager.addCriterion(
                    asserter.getData("categoryId") as CategoryId,
                    addCriterionRequest,
                ).invoke { criterion -> asserter.putData("criterionId", criterion.id) }
            }
            .assertThat({
                criteriaManager.getCriterion(asserter.getData("criterionId") as CriterionId)
            }) { criterionResponse ->
                criterionResponse.doCompare(addCriterionRequest)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Updating a criterion should return the updated criterion`(asserter: UniAsserter) {
        val addCriterionRequest = TestUtil.generateAddCriterionRequest()
        val updatedCriterionRequest = addCriterionRequest.copy(
            name = "Updated Criterion Name",
            description = "Updated Criterion Description",
            allowOverride = true,
        )

        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)

        asserter
            .execute<Rubric> {
                rubricRepository.create(rubric)
                    .invoke { rubric -> asserter.putData("rubricId", rubric.id) }
            }
            .execute<Category> {
                categoryRepository.create(category)
                    .invoke { category -> asserter.putData("categoryId", category.id) }
            }
            .execute<AddCriterionResponse> {
                criteriaManager.addCriterion(
                    asserter.getData("categoryId") as CategoryId,
                    addCriterionRequest,
                ).invoke { criterion -> asserter.putData("criterionId", criterion.id) }
            }
            .assertThat({
                criteriaManager.updateCriterion(
                    asserter.getData("criterionId") as CriterionId,
                    updatedCriterionRequest,
                )
            }) { criterionResponse ->
                criterionResponse.doCompare(updatedCriterionRequest)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Deleting a criterion should remove the criterion`(asserter: UniAsserter) {
        val addCriterionRequest = TestUtil.generateAddCriterionRequest()

        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)

        asserter
            .execute<Rubric> {
                rubricRepository.create(rubric)
                    .invoke { rubric -> asserter.putData("rubricId", rubric.id) }
            }
            .execute<Category> {
                categoryRepository.create(category)
                    .invoke { category -> asserter.putData("categoryId", category.id) }
            }
            .execute<AddCriterionResponse> {
                criteriaManager.addCriterion(
                    asserter.getData("categoryId") as CategoryId,
                    addCriterionRequest,
                ).invoke { criterion -> asserter.putData("criterionId", criterion.id) }
            }
            .execute<Unit> {
                criteriaManager.deleteCriterion(
                    asserter.getData("criterionId") as CriterionId,
                )
            }
            .assertFailedWith({
                criteriaManager.getCriterion(asserter.getData("criterionId") as CriterionId)
            }) { e ->
                assertEquals(CriterionNotFoundException::class.java, e.javaClass)
            }
    }
}
