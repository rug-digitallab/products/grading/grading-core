package nl.rug.digitallab.grading

import io.quarkus.test.junit.QuarkusTest
import io.restassured.http.ContentType
import io.restassured.module.kotlin.extensions.Extract
import io.restassured.module.kotlin.extensions.Given
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import jakarta.ws.rs.core.UriBuilder
import nl.rug.digitallab.grading.dtos.requests.AddBucketRequest
import nl.rug.digitallab.grading.dtos.responses.AddBucketResponse
import nl.rug.digitallab.grading.utils.TestUtil
import nl.rug.digitallab.grading.utils.TestUtil.doCompare
import org.jboss.resteasy.reactive.RestResponse.StatusCode
import org.junit.jupiter.api.Test
import kotlin.reflect.jvm.javaMethod

@QuarkusTest
class BucketsResourceTest {

    @Test
    fun `Adding a new bucket should return the bucket ID`() {
        val rubricId = RubricsResourceTest.addRubric(TestUtil.generateAddRubricRequest())
        val categoryId = CategoriesResourceTest.addCategory(TestUtil.generateAddCategoryRequest(), rubricId)
        val criterion = CriteriaResourceTest.addCriterion(TestUtil.generateAddCriterionRequest(), categoryId)
        val addBucketRequest = TestUtil.generateAddBucketRequest()

        addBucket(addBucketRequest, criterion.id)
    }

    @Test
    fun `Adding an invalid bucket should return 400`() {
        val rubricId = RubricsResourceTest.addRubric(TestUtil.generateAddRubricRequest())
        val categoryId = CategoriesResourceTest.addCategory(TestUtil.generateAddCategoryRequest(), rubricId)
        val criterion = CriteriaResourceTest.addCriterion(TestUtil.generateAddCriterionRequest(), categoryId)
        val invalidBucketRequest = TestUtil.generateInvalidBucket()

        Given {
            contentType(ContentType.JSON)
            body(invalidBucketRequest)
        } When {
            post(addBucketEndpoint(criterion.id))
        } Then {
            statusCode(StatusCode.BAD_REQUEST)
        }
    }

    @Test
    fun `Getting a bucket by ID should return the correct bucket`() {
        val rubricId = RubricsResourceTest.addRubric(TestUtil.generateAddRubricRequest())
        val categoryId = CategoriesResourceTest.addCategory(TestUtil.generateAddCategoryRequest(), rubricId)
        val criterion = CriteriaResourceTest.addCriterion(TestUtil.generateAddCriterionRequest(), categoryId)
        val addBucketRequest = TestUtil.generateAddBucketRequest()
        val bucketId = addBucket(addBucketRequest, criterion.id)

        val addBucketResponse =
            When {
                get(getBucketByIdEndpoint(bucketId))
            } Then {
                statusCode(StatusCode.OK)
            } Extract {
                body().`as`(AddBucketResponse::class.java)
            }

        addBucketResponse.doCompare(addBucketRequest)
    }

    @Test
    fun `Getting a non-existing bucket should return a 404`() {
        val nonExistentBucketId = BucketId.randomUUID()

        When {
            get(getBucketByIdEndpoint(nonExistentBucketId))
        } Then {
            statusCode(StatusCode.NOT_FOUND)
        }
    }

    @Test
    fun `Updating a bucket should return the updated bucket`() {
        val rubricId = RubricsResourceTest.addRubric(TestUtil.generateAddRubricRequest())
        val categoryId = CategoriesResourceTest.addCategory(TestUtil.generateAddCategoryRequest(), rubricId)
        val criterion = CriteriaResourceTest.addCriterion(TestUtil.generateAddCriterionRequest(), categoryId)
        val addBucketRequest = TestUtil.generateAddBucketRequest()
        val bucketId = addBucket(addBucketRequest, criterion.id)

        val updatedBucketRequest = addBucketRequest.copy(name = "Updated Name")

        val updatedAddBucketResponse =
            Given {
                contentType(ContentType.JSON)
                body(updatedBucketRequest)
            } When {
                put(updateBucketEndpoint(bucketId))
            } Then {
                statusCode(StatusCode.OK)
            } Extract {
                body().`as`(AddBucketResponse::class.java)
            }

        updatedAddBucketResponse.doCompare(updatedBucketRequest)
    }

    @Test
    fun `Updating a non-existing bucket should return a 404`() {
        val nonExistentBucketId = BucketId.randomUUID()
        val updateBucketRequest = TestUtil.generateAddBucketRequest()

        Given {
            contentType(ContentType.JSON)
            body(updateBucketRequest)
        } When {
            put(updateBucketEndpoint(nonExistentBucketId))
        } Then {
            statusCode(StatusCode.NOT_FOUND)
        }
    }

    @Test
    fun `Deleting a bucket should return 204`() {
        val rubricId = RubricsResourceTest.addRubric(TestUtil.generateAddRubricRequest())
        val categoryId = CategoriesResourceTest.addCategory(TestUtil.generateAddCategoryRequest(), rubricId)
        val criterion = CriteriaResourceTest.addCriterion(TestUtil.generateAddCriterionRequest(), categoryId)
        val addBucketRequest = TestUtil.generateAddBucketRequest()
        val bucketId = addBucket(addBucketRequest, criterion.id)

        When {
            delete(deleteBucketEndpoint(bucketId))
        } Then {
            statusCode(StatusCode.NO_CONTENT)
        }
    }

    @Test
    fun `Deleting a non-existing bucket should return 404`() {
        val nonExistentBucketId = BucketId.randomUUID()

        When {
            delete(deleteBucketEndpoint(nonExistentBucketId))
        } Then {
            statusCode(StatusCode.NOT_FOUND)
        }
    }

    /*
     * Helper functions for the resource tests
     */

    companion object {
        fun addBucket(addBucketRequest: AddBucketRequest, criterionId: CriterionId): BucketId {
            val response =
                Given {
                    contentType(ContentType.JSON)
                    body(addBucketRequest)
                } When {
                    post(addBucketEndpoint(criterionId))
                } Then {
                    statusCode(StatusCode.CREATED)
                } Extract {
                    body().`as`(AddBucketResponse::class.java)
                }
            return response.id
        }

        fun addBucketEndpoint(criterionId: CriterionId): String {
            return UriBuilder
                .fromResource(CriteriaResource::class.java)
                .path(CriteriaResource::bucketsSubResource.javaMethod)
                .path(CriteriaResource.BucketsSubResource::createBucket.javaMethod)
                .build(criterionId)
                .toString()
        }

        fun getBucketByIdEndpoint(bucketId: BucketId): String {
            return UriBuilder
                .fromResource(BucketsResource::class.java)
                .path(BucketsResource::getBucket.javaMethod)
                .build(bucketId)
                .toString()
        }

        fun updateBucketEndpoint(bucketId: BucketId): String {
            return UriBuilder
                .fromResource(BucketsResource::class.java)
                .path(BucketsResource::updateBucket.javaMethod)
                .build(bucketId)
                .toString()
        }

        fun deleteBucketEndpoint(bucketId: BucketId): String {
            return UriBuilder
                .fromResource(BucketsResource::class.java)
                .path(BucketsResource::deleteBucket.javaMethod)
                .build(bucketId)
                .toString()
        }
    }

}
