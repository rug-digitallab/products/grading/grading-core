package nl.rug.digitallab.grading

import io.quarkus.test.junit.QuarkusTest
import io.restassured.http.ContentType
import io.restassured.module.kotlin.extensions.Extract
import io.restassured.module.kotlin.extensions.Given
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import jakarta.ws.rs.core.UriBuilder
import nl.rug.digitallab.grading.dtos.requests.AddRubricRequest
import nl.rug.digitallab.grading.dtos.responses.AddAssessmentResponse
import nl.rug.digitallab.grading.dtos.responses.AddRubricResponse
import nl.rug.digitallab.grading.utils.TestUtil
import nl.rug.digitallab.grading.utils.TestUtil.doCompare
import org.jboss.resteasy.reactive.RestResponse.StatusCode
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import kotlin.reflect.jvm.javaMethod

@QuarkusTest
class RubricsResourceTest {

    @Test
    fun `Adding a new rubric should return the rubric ID`() {
        val addRubricRequest = TestUtil.generateAddRubricRequest()
        addRubric(addRubricRequest)
    }

    @Test
    fun `Adding an invalid rubric should return 400`() {
        val invalidRubricRequest = TestUtil.generateInvalidRubric()
        Given {
            contentType(ContentType.JSON)
            body(invalidRubricRequest)
        } When {
            post(addRubricEndpoint())
        } Then {
            statusCode(StatusCode.BAD_REQUEST)
        }
    }

    @Test
    fun `Getting a rubric by ID should return the correct rubric`() {
        val addRubricRequest = TestUtil.generateAddRubricRequest()
        val rubricId = addRubric(addRubricRequest)
        val addRubricResponse =
            When {
                get(getRubricByIdEndpoint(rubricId))
            } Then {
                statusCode(StatusCode.OK)
            } Extract {
                body().`as`(AddRubricResponse::class.java)
            }

        addRubricResponse.doCompare(addRubricRequest)
    }

    @Test
    fun `Getting a rubric by its assignment ID should return the correct rubric`() {
        val addRubricRequest = TestUtil.generateAddRubricRequest()
        val assignmentId = AssignmentId.randomUUID()
        addRubric(addRubricRequest, assignmentId)

        val addRubricResponse =
            When {
                get(getRubricByAssignmentIdEndpoint(assignmentId))
            } Then {
                statusCode(StatusCode.OK)
            } Extract {
                body().`as`(AddRubricResponse::class.java)
            }

        addRubricResponse.doCompare(addRubricRequest)
        assertEquals(addRubricResponse.assignmentId, assignmentId)
    }

    @Test
    fun `Getting a non-existing rubric should return a 404`() {
        val nonExistentRubricId = RubricId.randomUUID()

        When {
            get(getRubricByIdEndpoint(nonExistentRubricId))
        } Then {
            statusCode(StatusCode.NOT_FOUND)
        }
    }

    @Test
    fun `Updating a rubric should return the updated rubric`() {
        val addRubricRequest = TestUtil.generateAddRubricRequest()
        val rubricId = addRubric(addRubricRequest)

        val updatedRubricRequest = addRubricRequest.copy(name = "Updated Name")

        val updatedAddRubricResponse =
            Given {
                contentType(ContentType.JSON)
                body(updatedRubricRequest)
            } When {
                put(updateRubricEndpoint(rubricId))
            } Then {
                statusCode(StatusCode.OK)
            } Extract {
                body().`as`(AddRubricResponse::class.java)
            }

        updatedAddRubricResponse.doCompare(updatedRubricRequest)
    }

    @Test
    fun `Updating a non-existing rubric should return a 404`() {
        val nonExistentRubricId = RubricId.randomUUID()
        val updateRubricRequest = TestUtil.generateAddRubricRequest()

        Given {
            contentType(ContentType.JSON)
            body(updateRubricRequest)
        } When {
            put(updateRubricEndpoint(nonExistentRubricId))
        } Then {
            statusCode(StatusCode.NOT_FOUND)
        }
    }

    @Test
    fun `Deleting a rubric should return 204`() {
        val addRubricRequest = TestUtil.generateAddRubricRequest()
        val rubricId = addRubric(addRubricRequest)

        When {
            delete(deleteRubricEndpoint(rubricId))
        } Then {
            statusCode(StatusCode.NO_CONTENT)
        }
    }

    @Test
    fun `Deleting a non-existing rubric should return 404`() {
        val nonExistentRubricId = RubricId.randomUUID()

        When {
            delete(deleteRubricEndpoint(nonExistentRubricId))
        } Then {
            statusCode(StatusCode.NOT_FOUND)
        }
    }

    @Test
    fun `Starting grading submission should return the started assessment`() {
        val addRubricRequest = TestUtil.generateAddRubricRequest()
        val rubricId = addRubric(addRubricRequest)
        val addedAssessment = startGradingSubmission(rubricId)

        assertEquals(rubricId, addedAssessment.rubricId)
    }

    @Test
    fun `Trying to start grading a non-existing rubric should return a 404`() {
        val rubricId = RubricId.randomUUID()
        Given {
            contentType(ContentType.JSON)
        } When {
            post(startAssessmentEndpoint(rubricId))
        } Then {
            statusCode(StatusCode.NOT_FOUND)
        }
    }

    /*
     * Helper functions for the resource tests
     */

    companion object {
        fun addRubric(addRubricRequest: AddRubricRequest, assignmentId: AssignmentId = AssignmentId.randomUUID()): RubricId {
            val addRubricResponse = Given {
                contentType(ContentType.JSON)
                body(addRubricRequest)
            } When {
                post(addRubricEndpoint(assignmentId))
            } Then {
                statusCode(StatusCode.CREATED)
            } Extract {
                body().`as`(AddRubricResponse::class.java)
            }
            return addRubricResponse.id
        }

        fun addRubricEndpoint(assignmentId: AssignmentId = AssignmentId.randomUUID()): String {
            return UriBuilder
                .fromResource(RubricsResource::class.java)
                .path(RubricsResource::createRubric.javaMethod)
                .build(assignmentId)
                .toString()
        }


        fun getRubricByAssignmentIdEndpoint(assignmentId: AssignmentId): String {
            return UriBuilder
                .fromResource(RubricsResource::class.java)
                .path(RubricsResource::getRubricByAssignment.javaMethod)
                .build(assignmentId)
                .toString()
        }

        fun getRubricByIdEndpoint(rubricId: RubricId): String {
            return UriBuilder
                .fromResource(RubricsResource::class.java)
                .path(RubricsResource::getRubric.javaMethod)
                .build(rubricId)
                .toString()
        }

        fun updateRubricEndpoint(rubricId: RubricId): String {
            return UriBuilder
                .fromResource(RubricsResource::class.java)
                .path(RubricsResource::updateRubric.javaMethod)
                .build(rubricId)
                .toString()
        }

        fun deleteRubricEndpoint(rubricId: RubricId): String {
            return UriBuilder
                .fromResource(RubricsResource::class.java)
                .path(RubricsResource::deleteRubric.javaMethod)
                .build(rubricId)
                .toString()
        }

        fun startGradingSubmission(rubricId: RubricId): AddAssessmentResponse {
            val response =
                Given {
                    contentType(ContentType.JSON)
                } When {
                    post(startAssessmentEndpoint(rubricId))
                } Then {
                    statusCode(StatusCode.CREATED)
                } Extract {
                    body().`as`(AddAssessmentResponse::class.java)
                }
            return response
        }

        fun startAssessmentEndpoint(rubricId: RubricId): String {
            return UriBuilder
                .fromResource(RubricsResource::class.java)
                .path(RubricsResource::assessmentsSubResource.javaMethod)
                .path(RubricsResource.AssessmentsSubResource::startGradingSubmission.javaMethod)
                .build(rubricId)
                .toString()
        }
    }
}
