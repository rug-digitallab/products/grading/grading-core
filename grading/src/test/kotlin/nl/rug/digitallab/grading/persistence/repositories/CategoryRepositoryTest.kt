package nl.rug.digitallab.grading.persistence.repositories

import io.quarkus.test.hibernate.reactive.panache.TransactionalUniAsserter
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import jakarta.inject.Inject
import nl.rug.digitallab.grading.CategoryId
import nl.rug.digitallab.grading.exceptions.CategoryNotFoundException
import nl.rug.digitallab.grading.persistence.entities.Category
import nl.rug.digitallab.grading.utils.ReposTestUtil
import nl.rug.digitallab.grading.utils.ReposTestUtil.doCompare
import nl.rug.digitallab.grading.utils.TestUtil
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.math.BigDecimal

@QuarkusTest
class CategoryRepositoryTest {
    @Inject
    lateinit var rubricRepository: RubricRepository

    @Inject
    lateinit var categoryRepository: CategoryRepository

    @Test
    @RunOnVertxContext
    fun `Finding a category by ID should return the correct category`(asserter: TransactionalUniAsserter) {
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)

        val createUni =
            rubricRepository.create(rubric)
                .flatMap { _ ->
                    categoryRepository.create(category)
                }

        asserter
            .execute<Category> {
                createUni
            }
            .assertThat({
                categoryRepository.findByIdOrThrow(category.id)
            }) { foundCategory ->
                category.doCompare(foundCategory)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Finding a category by ID should throw an exception if the category does not exist`(asserter: TransactionalUniAsserter) {
        val categoryId = CategoryId.randomUUID()
        asserter
            .assertFailedWith({
                categoryRepository.findByIdOrThrow(categoryId)
            }) { e ->
                assertEquals(CategoryNotFoundException::class.java, e.javaClass)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Deleting a category by ID should remove the category`(asserter: TransactionalUniAsserter) {
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)

        val createUni =
            rubricRepository.create(rubric)
                .flatMap { _ ->
                    categoryRepository.create(category)
                }

        asserter
            .execute<Category> {
                createUni
            }
            .execute<Unit> { categoryRepository.deleteWithOrThrow(category.id) }
            .assertFailedWith({
                categoryRepository.findByIdOrThrow(category.id)
            }) { e ->
                assertEquals(CategoryNotFoundException::class.java, e.javaClass)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Updating a category by ID should modify and return the updated category`(asserter: TransactionalUniAsserter) {
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)

        val createUni =
            rubricRepository.create(rubric)
                .flatMap { _ ->
                    categoryRepository.create(category)
                }

        asserter
            .execute<Category> {
                createUni
            }
            .execute<Category> {
                categoryRepository.updateWithOrThrow(category.id) {
                    it.name = "Updated Name"
                    it.description = "Updated Description"
                    it.scalePointsTo = BigDecimal(15)
                    it
                }
            }
            .assertThat({
                categoryRepository.findByIdOrThrow(category.id)
            }) { updatedCategory ->
                assertEquals("Updated Name", updatedCategory.name)
                assertEquals("Updated Description", updatedCategory.description)
                TestUtil.assertBigDecimalEquals(BigDecimal(15), updatedCategory.scalePointsTo)
            }
    }
}
