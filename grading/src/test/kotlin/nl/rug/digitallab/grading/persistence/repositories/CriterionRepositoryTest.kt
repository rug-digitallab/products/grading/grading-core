package nl.rug.digitallab.grading.persistence.repositories

import io.quarkus.test.hibernate.reactive.panache.TransactionalUniAsserter
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import jakarta.inject.Inject
import nl.rug.digitallab.grading.CriterionId
import nl.rug.digitallab.grading.enums.CriterionSource
import nl.rug.digitallab.grading.exceptions.CriterionNotFoundException
import nl.rug.digitallab.grading.persistence.entities.Category
import nl.rug.digitallab.grading.persistence.entities.Criterion
import nl.rug.digitallab.grading.persistence.entities.external.PointsSource
import nl.rug.digitallab.grading.utils.ReposTestUtil
import nl.rug.digitallab.grading.utils.ReposTestUtil.doCompare
import org.hibernate.HibernateException
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.net.URI

@QuarkusTest
class CriterionRepositoryTest {
    @Inject
    lateinit var rubricRepository: RubricRepository

    @Inject
    lateinit var categoryRepository: CategoryRepository

    @Inject
    lateinit var criterionRepository: CriterionRepository

    @Test
    @RunOnVertxContext
    fun `Finding a criterion by ID should return the correct criterion`(asserter: TransactionalUniAsserter) {
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)
        val criterion = ReposTestUtil.generateRandomCriterionEntity(category)

        val createUni =
            rubricRepository.create(rubric)
                .flatMap { _ ->
                    categoryRepository.create(category)
                }.flatMap { _ ->
                    criterionRepository.create(criterion)
                }

        asserter
            .execute<Criterion> {
                createUni
            }
            .assertThat({
                criterionRepository.findByIdOrThrow(criterion.id)
            }) { foundCriterion ->
                criterion.doCompare(foundCriterion)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Finding a criterion by ID should throw an exception if the criterion does not exist`(asserter: TransactionalUniAsserter) {
        val criterionId = CriterionId.randomUUID()
        asserter
            .assertFailedWith({
                criterionRepository.findByIdOrThrow(criterionId)
            }) { e ->
                assertEquals(CriterionNotFoundException::class.java, e.javaClass)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Deleting a criterion by ID should remove the criterion`(asserter: TransactionalUniAsserter) {
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)
        val criterion = ReposTestUtil.generateRandomCriterionEntity(category)

        val createUni =
            rubricRepository.create(rubric)
                .flatMap { _ ->
                    categoryRepository.create(category)
                }.flatMap { _ ->
                    criterionRepository.create(criterion)
                }

        asserter
            .execute<Criterion> {
                createUni
            }
            .execute<Unit> { criterionRepository.deleteWithOrThrow(criterion.id) }
            .assertFailedWith({
                criterionRepository.findByIdOrThrow(criterion.id)
            }) { e ->
                assertEquals(CriterionNotFoundException::class.java, e.javaClass)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Updating a criterion by ID should modify and return the updated criterion`(asserter: TransactionalUniAsserter) {
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)
        val criterion = ReposTestUtil.generateRandomCriterionEntity(category)

        val createUni =
            rubricRepository.create(rubric)
                .flatMap { _ ->
                    categoryRepository.create(category)
                }.flatMap { _ ->
                    criterionRepository.create(criterion)
                }

        asserter
            .execute<Criterion> {
                createUni
            }
            .execute<Criterion> {
                criterionRepository.updateWithOrThrow(criterion.id) {
                    it.name = "Updated Name"
                    it.description = "Updated Description"
                    it
                }
            }
            .assertThat({
                criterionRepository.findByIdOrThrow(criterion.id)
            }) { updatedCriterion ->
                assertEquals("Updated Name", updatedCriterion.name)
                assertEquals("Updated Description", updatedCriterion.description)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Creating a criterion with 2 grading sources should fail validation`(asserter: TransactionalUniAsserter) {
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)

        // Criterion with both manualPoints and constantPoints defined
        val criterion = Criterion(
            category = category,
            name = "Criterion with multiple grading sources",
            description = "Invalid criterion",
            allowOverride = true,
            points = BigDecimal(5.0),
            source = CriterionSource.MANUAL,
            externalPoints = PointsSource(
                externalSource = URI("http://me.example.com"),
                scalePointsTo = BigDecimal(100),
            ),
            buckets = mutableSetOf(),
        )

        val createUni =
            rubricRepository.create(rubric)
                .flatMap { _ ->
                    categoryRepository.create(category)
                }

        asserter
            .execute<Category> {
                createUni
            }
            .assertFailedWith({
                criterionRepository.create(criterion)
            }) { e ->
                assertEquals(HibernateException::class.java, e.javaClass)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Creating a criterion with 3 grading sources should fail validation`(asserter: TransactionalUniAsserter) {
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)

        // Criterion with both manualPoints and constantPoints defined
        val criterion = Criterion(
            category = category,
            name = "Criterion with multiple grading sources",
            description = "Invalid criterion",
            allowOverride = true,
            points = BigDecimal(5.0),
            source = CriterionSource.MANUAL,
            externalPoints = PointsSource(
                URI("http://me.example.com"),
                scalePointsTo = BigDecimal(100)
            ),
            buckets = mutableSetOf()
        )

        val createUni =
            rubricRepository.create(rubric)
                .flatMap { _ ->
                    categoryRepository.create(category)
                }

        asserter
            .execute<Category> {
                createUni
            }
            .assertFailedWith({
                criterionRepository.create(criterion)
            }) { e ->
                assertEquals(HibernateException::class.java, e.javaClass)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Creating a valid criterion should pass validation`(asserter: TransactionalUniAsserter) {
        val rubric = ReposTestUtil.generateRandomRubricEntity()
        val category = ReposTestUtil.generateRandomCategoryEntity(rubric)

        // Criterion with a valid single grading source (manualPoints)
        val criterion = Criterion(
            category = category,
            name = "Valid Criterion",
            description = "Valid criterion with manual points",
            allowOverride = true,
            points = BigDecimal(5.0),
            source = CriterionSource.MANUAL,
            buckets = mutableSetOf()
        )

        val createUni =
            rubricRepository.create(rubric)
                .flatMap { _ ->
                    categoryRepository.create(category)
                }
                .flatMap { _ ->
                    criterionRepository.create(criterion)
                }

        asserter
            .execute<Criterion> {
                createUni
            }
            .assertThat({
                criterionRepository.findByIdOrThrow(criterion.id)
            }) { foundCriterion ->
                criterion.doCompare(foundCriterion)
            }
    }
}
