package nl.rug.digitallab.grading.persistence.repositories

import io.quarkus.test.hibernate.reactive.panache.TransactionalUniAsserter
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import jakarta.inject.Inject
import nl.rug.digitallab.grading.RubricId
import nl.rug.digitallab.grading.exceptions.RubricNotFoundException
import nl.rug.digitallab.grading.persistence.entities.Rubric
import nl.rug.digitallab.grading.utils.ReposTestUtil
import nl.rug.digitallab.grading.utils.ReposTestUtil.doCompare
import nl.rug.digitallab.grading.utils.TestUtil
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.math.BigDecimal

@QuarkusTest
class RubricRepositoryTest {
    @Inject
    private lateinit var rubricRepository: RubricRepository

    @Test
    @RunOnVertxContext
    fun `Finding a rubric by ID should return the correct rubric`(asserter: TransactionalUniAsserter) {
        val rubric = ReposTestUtil.generateRandomRubricEntity()

        asserter
            .execute<Rubric> { rubricRepository.create(rubric) }
            .assertThat({
                rubricRepository.findByIdOrThrow(rubric.id)
            }) { foundRubric ->
                rubric.doCompare(foundRubric)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Finding a rubric by ID should throw an exception if the rubric does not exist`(asserter: TransactionalUniAsserter) {
        val rubricId = RubricId.randomUUID()
        asserter
            .assertFailedWith({
                rubricRepository.findByIdOrThrow(rubricId)
            }) { e ->
                assertEquals(RubricNotFoundException::class.java, e.javaClass)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Deleting a rubric by ID should remove the rubric`(asserter: TransactionalUniAsserter) {
        val rubric = ReposTestUtil.generateRandomRubricEntity()

        asserter
            .execute<Rubric> { rubricRepository.create(rubric) }
            .execute<Unit> { rubricRepository.deleteByIdOrThrow(rubric.id) }
            .assertFailedWith({
                rubricRepository.findByIdOrThrow(rubric.id)
            }) { e ->
                assertEquals(RubricNotFoundException::class.java, e.javaClass)
            }
    }

    @Test
    @RunOnVertxContext
    fun `Updating a rubric by ID should modify and return the updated rubric`(asserter: TransactionalUniAsserter) {
        val rubric = ReposTestUtil.generateRandomRubricEntity()

        asserter
            .execute<Rubric> { rubricRepository.create(rubric) }
            .execute<Rubric> {
                rubricRepository.updateWithOrThrow(rubric.id) {
                    it.name = "Updated Name"
                    it.description = "Updated Description"
                    it.scalePointsTo = BigDecimal(150)
                    it
                }
            }
            .assertThat({
                rubricRepository.findByIdOrThrow(rubric.id)
            }) { updatedRubric ->
                assertEquals("Updated Name", updatedRubric.name)
                assertEquals("Updated Description", updatedRubric.description)
                TestUtil.assertBigDecimalEquals(BigDecimal(150), updatedRubric.scalePointsTo)
            }
    }
}
