package nl.rug.digitallab.grading

import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.vertx.RunOnVertxContext
import io.restassured.module.kotlin.extensions.Extract
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import nl.rug.digitallab.grading.RubricsResourceTest.Companion.getRubricByIdEndpoint
import nl.rug.digitallab.grading.dtos.responses.AddRubricResponse
import nl.rug.digitallab.grading.utils.TestUtil
import nl.rug.digitallab.grading.utils.TestUtil.doCompare
import org.jboss.resteasy.reactive.RestResponse.StatusCode
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

@QuarkusTest
class CompleteRubricTest {

    @Test
    @RunOnVertxContext
    fun `Complete rubric creation and validation`() {
        generateAndCreateRubric()
    }

    companion object {
        fun generateAndCreateRubric() : AddRubricResponse {
            val addRubricRequest = TestUtil.generateAddRubricRequest()
            val addCategoryRequest = TestUtil.generateAddCategoryRequest()
            val addCriterionRequest = TestUtil.generateAddCriterionRequest()
            val addBucketRequest = TestUtil.generateAddBucketRequest()

            val rubricId = RubricsResourceTest.addRubric(addRubricRequest)
            val categoryId = CategoriesResourceTest.addCategory(addCategoryRequest, rubricId)
            val addCriterionResponse = CriteriaResourceTest.addCriterion(addCriterionRequest, categoryId)
            val bucketId = BucketsResourceTest.addBucket(addBucketRequest, addCriterionResponse.id)

            val addRubricResponse =
                When {
                    get(getRubricByIdEndpoint(rubricId))
                } Then {
                    statusCode(StatusCode.OK)
                } Extract {
                    body().`as`(AddRubricResponse::class.java)
                }
            // Validate Rubric
            addRubricResponse.doCompare(addRubricRequest)
            assertEquals(addRubricResponse.categories.size, 1)
            val category = addRubricResponse.categories.first()
            category.doCompare(addCategoryRequest)
            assertEquals(category.criteria.size, 1)
            val criterion = category.criteria.first()
            criterion.doCompare(addCriterionRequest)
            assertEquals(criterion.buckets!!.size, 1)
            val bucket = criterion.buckets!!.first()
            bucket.doCompare(addBucketRequest)

            return addRubricResponse
        }
    }
}
